[//]: # (Readme.md)

# Avet UltraZed-EV VCU TRD Vivado Design Generation Scripts
- This repository contains the latest working copies for the Vivado design module ports of the Xilinx ZCU106 v2018.3 VCU TRD design.
- Each design module port consists of:
	- 2 or more TCL scripts for generating the block design
	- 1 or more design constraints files
	- Optional IP block source folders for custom IP blocks

## Common file organization and naming conventions for each design module port
```
	- ./scripts_uz7ev/<design-module-name>_proj_uz7ev_cc.tcl
		- Top level design generation script for each design module

	- ./scripts_uz7ev/<design-module-name>_bd_uz7ev_cc.tcl
		- Contains design module specific implementation details for each design

	- ./constrs_uz7ev/<design-module-name>.xdc
		- Design constraints file for each design module

	- ./srcs/
		- IP block source folders for custom IP blocks
```

## Other folders/files not contained in the repository
```
	- ./<design-module-name>/
		- Design module working folder for Vivado Project
```

## How to use the TCL scripts to generate a design module
- To create a design module's Vivado project and generate a bitstream file
```
Example: Create a VCU Only UZ7EV Carrier Card Based FPGA Design
	- Set the TRD_HOME variable:
		$ export TRD_HOME=<path_to_bundle>/rdf0428-uz7ev-trd-2018-3/
	- Change the pl directory:
		$ cd $TRD_HOME/pl
	- Execute the design module top-level generation script:
		$ vivado -source scripts_uz7ev/vcu_proj_uz7ev_cc.tcl
	
	
	- Entering any of the above command will launch Vivado� tool, loads the block diagram, and adds the required top file and XDC file to the project
	- In the Flow Navigator pane on the left-hand side under Program and Debug, click Generate Bitstream.
```	
 
