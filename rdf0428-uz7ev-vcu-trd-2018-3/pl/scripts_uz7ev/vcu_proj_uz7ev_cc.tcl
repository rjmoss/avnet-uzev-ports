# global variables
#set ::platform "zcu106"
set ::platform "uz7ev_cc"
#set ::silicon "e"
set ::silicon "i"

# local variables
#set project_dir "vcu_hdmirx"
set project_dir "vcu_${platform}"
set ip_dir "srcs/ip"
#set constrs_dir "constrs"
set constrs_dir "constrs_uz7ev"
#set scripts_dir "scripts"
set scripts_dir "scripts_uz7ev"

# set variable names
#set part "xczu7ev-ffvc1156-2-${::silicon}"
set part "xczu7ev-fbvb900-1-${::silicon}"
#set design_name "vcu_hdmirx"
set design_name "vcu_${platform}"
puts "INFO: Target part selected: '$part'"

# set up project
create_project $design_name $project_dir -part $part -force
#set_property board_part xilinx.com:zcu106:part0:2.3 [current_project]
set_property BOARD_PART em.avnet.com:ultrazed_7ev_cc:part0:1.1 [current_project]

# set up IP repo
set_property ip_repo_paths $ip_dir [current_fileset]
update_ip_catalog -rebuild

# set up bd design
create_bd_design $design_name
#source $scripts_dir/vcu_hdmirx_bd.tcl
source $scripts_dir/vcu_bd_${platform}.tcl

# add hdl sources to project
#make_wrapper -files [get_files ./$project_dir/vcu_hdmirx.srcs/sources_1/bd/vcu_hdmirx/vcu_hdmirx.bd] -top
make_wrapper -files [get_files ./$project_dir/$design_name.srcs/sources_1/bd/$design_name/$design_name.bd] -top

#add_files -norecurse ./$project_dir/vcu_hdmirx.srcs/sources_1/bd/vcu_hdmirx/hdl/vcu_hdmirx_wrapper.v
add_files -norecurse ./$project_dir/$design_name.srcs/sources_1/bd/$design_name/hdl/${design_name}_wrapper.v

#set_property top vcu_hdmirx_wrapper [current_fileset]
set_property top ${design_name}_wrapper [current_fileset]

#add_files -fileset constrs_1 -norecurse $constrs_dir/vcu_hdmirx.xdc
add_files -fileset constrs_1 -norecurse $constrs_dir/${design_name}.xdc

update_compile_order -fileset sources_1
set_property strategy Performance_NetDelay_low [get_runs impl_1]       

validate_bd_design
update_compile_order -fileset sources_1
regenerate_bd_layout
save_bd_design
