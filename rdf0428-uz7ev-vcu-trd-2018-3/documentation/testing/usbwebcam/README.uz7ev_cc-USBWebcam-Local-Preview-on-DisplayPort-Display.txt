########################################################################
# GOAL:
# - Use GStreamer to display webcam video on a DisplayPort Display
# - Review the gstreamer pipeline elements
#   -[WEBCAM] ->
#     |--> [V4L2SRC] -> [QUEUE] -> [KMSSINK(DISPLAY PORT)]
########################################################################
- Step through building the example pipeline above
- During construction of the pipeline, we will use the 'fakesink' element for testing purposes
  - 'fakesink' provides a dummy connection for pads to pass data to (no processing takes place on this data)

########################################################################
# Connect a USB Webcam
# Example: Logitech C930e (1080p, 5.7Mpixel)
########################################################################
- Check the Kernel dmesg log to verify that the USB drivers are loaded
########################################################################
root@vcu_uz7ev_cc:~# dmesg | grep usb

...
[    1.351121] usbcore: registered new interface driver usbfs
[    1.351160] usbcore: registered new interface driver hub
[    1.351202] usbcore: registered new device driver usb
[    7.899036] usbcore: registered new interface driver asix
[    7.899089] usbcore: registered new interface driver ax88179_178a
[    7.899126] usbcore: registered new interface driver cdc_ether
[    7.899160] usbcore: registered new interface driver net1080
[    7.899192] usbcore: registered new interface driver cdc_subset
[    7.899223] usbcore: registered new interface driver zaurus
[    7.899264] usbcore: registered new interface driver cdc_ncm
[    7.900280] usbcore: registered new interface driver uas
[    7.900322] usbcore: registered new interface driver usb-storage
[    7.957501] usbcore: registered new interface driver uvcvideo
[    7.957511] USB Video Class driver (1.1.1)
[    7.957501] usbcore: registered new interface driver uvcvideo
[    7.958560] usbcore: registered new interface driver bcm203x
[    7.958595] usbcore: registered new interface driver bpa10x
[    7.958631] usbcore: registered new interface driver bfusb
[    7.958667] usbcore: registered new interface driver btusb
[    7.958727] usbcore: registered new interface driver ath3k
[    8.060672] usbcore: registered new interface driver usbhid
[    8.060681] usbhid: USB HID core driver
[    8.063155] usbcore: registered new interface driver snd-usb-audio
[   10.324466] usb usb1: New USB device found, idVendor=1d6b, idProduct=0002
[   10.331319] usb usb1: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[   10.338610] usb usb1: Product: xHCI Host Controller
[   10.343539] usb usb1: Manufacturer: Linux 4.14.0-xilinx-v2018.3 xhci-hcd
[   10.350304] usb usb1: SerialNumber: xhci-hcd.0.auto
[   10.376871] usb usb2: New USB device found, idVendor=1d6b, idProduct=0003
[   10.383729] usb usb2: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[   10.391022] usb usb2: Product: xHCI Host Controller
[   10.395944] usb usb2: Manufacturer: Linux 4.14.0-xilinx-v2018.3 xhci-hcd
[   10.402705] usb usb2: SerialNumber: xhci-hcd.0.auto
[   10.702307] usb 1-1: new high-speed USB device number 2 using xhci-hcd
[   10.943922] usb 1-1: New USB device found, idVendor=05e3, idProduct=0610
[   10.950786] usb 1-1: New USB device strings: Mfr=1, Product=2, SerialNumber=0
[   10.961688] usb 1-1: Product: USB2.0 Hub
[   10.969311] usb 1-1: Manufacturer: GenesysLogic
[   11.058353] usb 2-1: new SuperSpeed USB device number 2 using xhci-hcd
[   11.088857] usb 2-1: New USB device found, idVendor=05e3, idProduct=0617
[   11.099250] usb 2-1: New USB device strings: Mfr=1, Product=2, SerialNumber=0
[   11.110023] usb 2-1: Product: USB3.0 Hub
[   11.117532] usb 2-1: Manufacturer: GenesysLogic
[   11.470312] usb 2-1.3: new SuperSpeed USB device number 3 using xhci-hcd
[   11.499040] usb 2-1.3: New USB device found, idVendor=0781, idProduct=5591
[   11.509529] usb 2-1.3: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[   11.520344] usb 2-1.3: Product: Ultra USB 3.0
[   11.528159] usb 2-1.3: Manufacturer: SanDisk
[   11.535837] usb 2-1.3: SerialNumber: 4C530000101226122093
[   11.546014] usb-storage 2-1.3:1.0: USB Mass Storage device detected
[   11.556204] scsi host2: usb-storage 2-1.3:1.0
...

- Connect the USB Webcam (Logitech C930e in this example)
########################################################################
root@vcu_uz7ev_cc:~#

[  157.550236] usb 1-1.2: new high-speed USB device number 3 using xhci-hcd
[  159.972354] usb 1-1.2: New USB device found, idVendor=046d, idProduct=0843
[  159.982652] usb 1-1.2: New USB device strings: Mfr=0, Product=2, SerialNumber=1
[  159.993368] usb 1-1.2: Product: Logitech Webcam C930e
[  160.001835] usb 1-1.2: SerialNumber: 84BF815E
[  160.010586] uvcvideo: Found UVC 1.00 device Logitech Webcam C930e (046d:0843)
[  160.021565] uvcvideo 1-1.2:1.0: Entity type for entity Processing 3 was not initialized!
[  160.033119] uvcvideo 1-1.2:1.0: Entity type for entity Extension 6 was not initialized!
[  160.044567] uvcvideo 1-1.2:1.0: Entity type for entity Extension 8 was not initialized!
[  160.055974] uvcvideo 1-1.2:1.0: Entity type for entity Extension 9 was not initialized!
[  160.067339] uvcvideo 1-1.2:1.0: Entity type for entity Extension 10 was not initialized!
[  160.078779] uvcvideo 1-1.2:1.0: Entity type for entity Extension 11 was not initialized!
[  160.090176] uvcvideo 1-1.2:1.0: Entity type for entity Extension 12 was not initialized!
[  160.101560] uvcvideo 1-1.2:1.0: Entity type for entity Extension 13 was not initialized!
[  160.112898] uvcvideo 1-1.2:1.0: Entity type for entity Camera 1 was not initialized!
[  160.124032] input: Logitech Webcam C930e as /devices/platform/amba/ff9d0000.usb0/fe200000.dwc3/xhci-hcd.0.auto/usb1/1-1/1-1.2/1-1.2:1.0/input0


#########################################################################
# Check to see that the webcam was recognized and registered properly
########################################################################
- Use v4l2-ctl to verify the USB Webcam is registered properly as a video device
########################################################################
root@vcu_uz7ev_cc:~# v4l2-ctl --list-devices

[  170.154407] usb 1-1.2: reset high-speed USB device number 3 using xhci-hcd
Logitech Webcam C930e (usb-xhci-hcd.0.auto-1.2):
        /dev/video0

#########################################################################
- Display supported resolutions and framerates for each format
########################################################################
root@vcu_uz7ev_cc:~# v4l2-ctl --list-formats-ext

ioctl: VIDIOC_ENUM_FMT
        Index       : 0
        Type        : Video Capture
        Pixel Format: 'YUYV'
        Name        : YUYV 4:2:2
...
                Size: Discrete 1280x720
                        Interval: Discrete 0.100s (10.000 fps)
                        Interval: Discrete 0.133s (7.500 fps)
                        Interval: Discrete 0.200s (5.000 fps)
                Size: Discrete 1600x896
                        Interval: Discrete 0.133s (7.500 fps)
                        Interval: Discrete 0.200s (5.000 fps)
                Size: Discrete 1920x1080
                        Interval: Discrete 0.200s (5.000 fps)
                Size: Discrete 2304x1296
                        Interval: Discrete 0.500s (2.000 fps)
                Size: Discrete 2304x1536
                        Interval: Discrete 0.500s (2.000 fps)

        Index       : 1
        Type        : Video Capture
        Pixel Format: 'MJPG' (compressed)
        Name        : Motion-JPEG
...
                Size: Discrete 1280x720
                        Interval: Discrete 0.033s (30.000 fps)
                        Interval: Discrete 0.042s (24.000 fps)
                        Interval: Discrete 0.050s (20.000 fps)
                        Interval: Discrete 0.067s (15.000 fps)
                        Interval: Discrete 0.100s (10.000 fps)
                        Interval: Discrete 0.133s (7.500 fps)
                        Interval: Discrete 0.200s (5.000 fps)
                Size: Discrete 1600x896
                        Interval: Discrete 0.033s (30.000 fps)
                        Interval: Discrete 0.042s (24.000 fps)
                        Interval: Discrete 0.050s (20.000 fps)
                        Interval: Discrete 0.067s (15.000 fps)
                        Interval: Discrete 0.100s (10.000 fps)
                        Interval: Discrete 0.133s (7.500 fps)
                        Interval: Discrete 0.200s (5.000 fps)
                Size: Discrete 1920x1080
                        Interval: Discrete 0.033s (30.000 fps)
                        Interval: Discrete 0.042s (24.000 fps)
                        Interval: Discrete 0.050s (20.000 fps)
                        Interval: Discrete 0.067s (15.000 fps)
                        Interval: Discrete 0.100s (10.000 fps)
                        Interval: Discrete 0.133s (7.500 fps)
                        Interval: Discrete 0.200s (5.000 fps)

########################################################################
# STAGE 1a: Open a window into the USB Webcam
# - use the 'v4l2src' element
# - Let gstreamer auto-negotiate the capabilities (discover the right caps filter)
########################################################################
root@vcu_uz7ev_cc:~# gst-inspect-1.0 v4l2src

Factory Details:
  Rank                     primary (256)
  Long-name                Video (video4linux2) Source
  Klass                    Source/Video
  Description              Reads frames from a Video4Linux2 device
  Author                   Edgard Lima <edgard.lima@gmail.com>, Stefan Kost <ensonic@users.sf.net>

Plugin Details:
  Name                     video4linux2
  Description              elements for Video 4 Linux
...
Pads:
  SRC: 'src'
    Pad Template: 'src'

Element Properties:
...
  device              : Device location
                        flags: readable, writable
                        String. Default: "/dev/video0"
...

- Construct and test the first part of the pipeline 
**Note: This pipeline will run until cancelled manually using <CTRL-C>
########################################################################
root@vcu_uz7ev_cc:~# gst-launch-1.0 -v v4l2src device=/dev/video0 ! fakesink

Setting pipeline to PAUSED ...
Pipeline is live and does not need PREROLL ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
/GstPipeline:pipeline0/GstV4l2Src:v4l2src0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)2304, height=(int)1536, pixel-aspect-1
/GstPipeline:pipeline0/GstFakeSink:fakesink0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)2304, height=(int)1536, pixel-aspe1
^Chandling interrupt.
Interrupt: Stopping pipeline ...
Execution ended after 0:00:02.146648070
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...
root@vcu_uz7ev_cc:~# gst-inspect v4l2src
-sh: gst-inspect: command not found
root@vcu_uz7ev_cc:~# gst-inspect-1.0 v4l2src 

########################################################################
# STAGE 1b: Open a window into the USB Webcam
# - use the 'v4l2src' element
# - Use gstreamer element properties to specify the camera data stream resolution and format
# Note: Looking at the X-RAW formats, you'll notice what might appear to be a mismatch:
# - The camera supports YUYV 4:2:2
# - The v4l2src element does not explicitly support YUYV
# - Look at http://www.fourcc.org/yuv.php:
#   - YUYV is a duplicate name for the YUY2 format
#   - http://www.fourcc.org/yuv-yuy2/
# Note: YUYV is a '4:2:2' format, which is important for a later stage in the pipeline
########################################################################
root@vcu_uz7ev_cc:~# gst-inspect-1.0 v4l2src   

Factory Details:
  Rank                     primary (256)
  Long-name                Video (video4linux2) Source
  Klass                    Source/Video
  Description              Reads frames from a Video4Linux2 device
  Author                   Edgard Lima <edgard.lima@gmail.com>, Stefan Kost <ensonic@users.sf.net>

Plugin Details:
  Name                     video4linux2
  Description              elements for Video 4 Linux
...
Pad Templates:
  SRC template: 'src'
    Availability: Always
    Capabilities:
      image/jpeg
...
      video/x-raw
                 format: { (string)RGB16, (string)BGR, (string)RGB, (string)GRAY8, (string)GRAY16_LE, (string)GRAY16_BE, (string)YVU9, (string)YV12, (string)YUY2, (string)YVYU, (st
ring)UYVY, (string)Y42B, (string)Y41B, (string)YUV9, (string)NV12_64Z32, (string)NV24, (string)NV12_10LE32, (string)NV16_10LE32, (string)NV61, (string)NV16, (string)NV21, (string)N
V12, (string)I420, (string)BGRA, (string)BGRx, (string)ARGB, (string)xRGB, (string)BGR15, (string)RGB15 }
                  width: [ 1, 32768 ]
                 height: [ 1, 32768 ]
              framerate: [ 0/1, 2147483647/1 ]

...
Pads:
  SRC: 'src'
    Pad Template: 'src'

Element Properties:
...
  device              : Device location
                        flags: readable, writable
                        String. Default: "/dev/video0"
...
                           (8388608): SECAM-Lc         - SECAM-Lc
  io-mode             : I/O mode
                        flags: readable, writable
                        Enum "GstV4l2IOMode" Default: 0, "auto"
                           (0): auto             - GST_V4L2_IO_AUTO
                           (1): rw               - GST_V4L2_IO_RW
                           (2): mmap             - GST_V4L2_IO_MMAP
                           (3): userptr          - GST_V4L2_IO_USERPTR
                           (4): dmabuf           - GST_V4L2_IO_DMABUF
                           (5): dmabuf-import    - GST_V4L2_IO_DMABUF_IMPORT
...

- Construct, add and test the next step in the pipeline
**Note: This pipeline will run until cancelled manually using <CTRL-C>
########################################################################
root@vcu_uz7ev_cc:~# gst-launch-1.0 -v v4l2src device=/dev/video0 ! video/x-raw,format=YUY2,width=1280,height=720,framerate=10/1 ! \
fakesink

Setting pipeline to PAUSED ...
Pipeline is live and does not need PREROLL ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
/GstPipeline:pipeline0/GstV4l2Src:v4l2src0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fra1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerat1
/GstPipeline:pipeline0/GstFakeSink:fakesink0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framera1
^Chandling interrupt.
Interrupt: Stopping pipeline ...
Execution ended after 0:00:06.293316223
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...

########################################################################
# STAGE 2a: Send video out to the KMSSINK (Display Output)
# - the input camera data format is YUYV (YUY2) which is 4:2:2
# - use the 'kmssink' element to send decoded video to the display
# - Let gstreamer auto-negotiate display properties
# - This pipeline works on a display set to a different resolution as the source video (i.e. 4K)
# NOTE: You do not have to turn off alpha blending in this case, because the kmssink will setup a display
#       mode change to change resolution when using the fullscreen-overlay=true property
########################################################################
root@vcu_uz7ev_cc:~# gst-inspect-1.0 kmssink

Factory Details:
  Rank                     secondary (128)
  Long-name                KMS video sink
  Klass                    Sink/Video
  Description              Video sink using the Linux kernel mode setting API
  Author                   V�íctor J�áquez <vjaquez@igalia.com>

Plugin Details:
  Name                     kms
  Description              Video sink using the Linux kernel mode setting API
...
Pad Templates:
  SINK template: 'sink'
    Availability: Always
    Capabilities:
      video/x-raw
                 format: { (string)BGRA, (string)BGRx, (string)RGBA, (string)RGBx, (string)RGB, (string)BGR, (string)UYVY, (string)YUY2, (string)YVYU, (string)I420, (string)YV12, (string)Y42B, (str}
                  width: [ 1, 2147483647 ]
                 height: [ 1, 2147483647 ]
              framerate: [ 0/1, 2147483647/1 ]
...
Pads:
  SINK: 'sink'
    Pad Template: 'sink'

Element Properties:
...
  bus-id              : DRM bus ID
                        flags: readable, writable
                        String. Default: null
  connector-id        : DRM connector id
                        flags: readable, writable
                        Integer. Range: -1 - 2147483647 Default: -1 
  plane-id            : DRM plane id
                        flags: readable, writable
                        Integer. Range: -1 - 2147483647 Default: -1 
...
  fullscreen-overlay  : When enabled, the sink sets CRTC size same as input video size
                        flags: readable, writable
                        Boolean. Default: false
...

- Construct, add and test the next step in the pipeline
l- Specify the display device, but let gstreamer auto-negotiate display properties
**Note: This pipeline will not work due to a resolution mismatch
**Note: The command will need to be canceled using <CTRL-C>
########################################################################
root@zcu106_vcu_trd:~# gst-launch-1.0 -v v4l2src device=/dev/video0 ! video/x-raw,format=YUY2,width=1280,height=720,framerate=10/1 ! \
kmssink bus-id="fd4a0000.zynqmp-display" fullscreen-overlay=true

Setting pipeline to PAUSED ...
Pipeline is live and does not need PREROLL ...
/GstPipeline:pipeline0/GstKMSSink:kmssink0: display-width = 3440
/GstPipeline:pipeline0/GstKMSSink:kmssink0: display-height = 1440
Setting pipeline to PLAYING ...
New clock: GstSystemClock
/GstPipeline:pipeline0/GstV4l2Src:v4l2src0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixe
l-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1
, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
[ 1836.399344] PLL: shutdown
[ 1836.404091] PLL: enable
/GstPipeline:pipeline0/GstKMSSink:kmssink0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pix
el-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/
1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
^Chandling interrupt.
Interrupt: Stopping pipeline ...
Execution ended after 0:01:00.714567081
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
[ 1897.136638] PLL: shutdown
[ 1897.141771] PLL: enable
Freeing pipeline ...


########################################################################
# STAGE 2b: Send video out to the KMSSINK (Display Output)
# - the input camera data format is YUYV (YUY2) which is 4:2:2
# - use the 'kmssink' element to send decoded video to the display
# - This pipeline works on a display set to the same resolution as the decoded video (1080p)
# NOTE: If you're using the display to output the serial console, you must first turn off alpha-blending
#       Otherwise you won't see video displayed on the screen
########################################################################
- Turn off alpha blending (if required)
########################################################################
root@vcu_uz7ev_cc:~# modetest -D fd4a0000.zynqmp-display -w 35:g_alpha_en:0

- Construct, add and test the next step in the pipeline
**Note: This pipeline will take a while to complete, as it will parse and playback the entire video clip
########################################################################
root@zcu106_vcu_trd:~# gst-launch-1.0 -v v4l2src device=/dev/video0 ! video/x-raw,format=YUY2,width=1280,height=720,framerate=10/1 ! \
kmssink bus-id="fd4a0000.zynqmp-display"                                                             
Setting pipeline to PAUSED ...
Pipeline is live and does not need PREROLL ...
/GstPipeline:pipeline0/GstKMSSink:kmssink0: display-width = 1280
/GstPipeline:pipeline0/GstKMSSink:kmssink0: display-height = 720
Setting pipeline to PLAYING ...
New clock: GstSystemClock
/GstPipeline:pipeline0/GstV4l2Src:v4l2src0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixe
l-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1
, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstKMSSink:kmssink0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pix
el-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/
1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
^Chandling interrupt.
Interrupt: Stopping pipeline ...
Execution ended after 0:00:08.016918172
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...

- Restore alpha blending (if required)
########################################################################
root@vcu_uz7ev_cc:~# modetest -D fd4a0000.zynqmp-display -w 35:g_alpha_en:1


