########################################################################
# 1.) Check the detailed Configuration for the DisplayPort driver to see alpha-blending support
#      - In the case below, this is connected to a 26" ViewSonic 1920x1200 monitor using DisplayPort
########################################################################
root@vcu_uz7ev_cc:~# modetest -D fd4a0000.zynqmp-display

Encoders:
id      crtc    type    possible crtcs  possible clones
37      36      TMDS    0x00000001      0x00000000

Connectors:
id      encoder status          name            size (mm)       modes   encoders
38      37      connected       DP-1            520x290         25      37
  modes:
        name refresh (Hz) hdisp hss hse htot vdisp vss vse vtot)
  1920x1080 30 1920 2008 2052 2200 1080 1084 1089 1125 74250 flags: phsync, pvsync; type: driver
...
CRTCs:
id      fb      pos     size
36      43      (0,0)   (1920x1080)
  1920x1080 30 1920 2008 2052 2200 1080 1084 1089 1125 74250 flags: phsync, pvsync; type: driver
...
Planes:
id      crtc    fb      CRTC x,y        x,y     gamma size      possible crtcs
34      0       0       0,0             0,0     0               0x00000001
  formats: VYUY UYVY YUYV YVYU YU16 YV16 YU24 YV24 NV16 NV61 BG24 RG24 XB24 XR24 XB30 XR30 YU12 YV12 NV12 NV21 XV15 XV20
...
35      36      43      0,0             0,0     0               0x00000001
  formats: AB24 AR24 RA24 BA24 BG24 RG24 RA15 BA15 RA12 BA12 RG16 BG16
  props:
        6 type:
                flags: immutable enum
                enums: Overlay=0 Primary=1 Cursor=2
                value: 1
        27 alpha:
                flags: range
                values: 0 255
                value: 255
        28 g_alpha_en:
                flags: range
                values: 0 1
                value: 1

Frame buffers:
id      size    pitch

########################################################################
# 2.) Disable Alpha Blending on the Primary Plane
- Primary Plane is #35 (per the configuration above '35 -> props -> type -> value = 1, Primary').
- Use the 'modetest' command (use 'modetest --help to see options')
- Specify a device using '-D'
- Device name = fd4a0000.zynqmp-display
- Change a property for a specific plane using '-w'
  - option format '<plane_id>:<property>:<value>'[,<connector_id>][@crtc_id]:<mode>[-vrefresh>[@<format>]'
  - Plane ID = <plane_id> = 5
  - Property = <property> = g_alpha_en
  - Value = <value> = 0
  - Mode write string = 35:g_alpha_en:0
########################################################################
root@vcu_uz7ev_cc:~# modetest -D fd4a0000.zynqmp-display -w 35:g_alpha_en:0
