########################################################################
# GOAL:
# - Use GStreamer + 'omx264dec' to decode video to a DisplayPort Display
# - Review the gstreamer pipeline elements
#   -[VIDEO FILE INPUT] ->
#     |--> [FILESRC] -> [QTDEMUX.video_0] -> [H264PARSE] -> [OMXH264DEC] -> [QUEUE] -> [KMSSINK(DISPLAY PORT)]
#  **Optional:          [QTDEMUX.audio_0] -> [FAAD] -> [AUDICONVERT] -> [AUDIORESAMPLE] -> [**AUTOAUDIOSINK]
########################################################################
- Step through building the example pipeline above
- During construction of the pipeline, we will use the 'fakesink' element for testing purposes
  - 'fakesink' provides a dummy connection for pads to pass data to (no processing takes place on this data)

########################################################################
# Obtain example media file(s)
# - Xilinx Documentation: PG252 'H.264/H.265 Video Codec Unit v1.2', December 5, 2018
#   - VCU Out of the Box Examples
#     - Download the AVC sample file
# - Download the original clips
#   - Big Buck Bunny Standard Repository (bbb3d.renderfarmig.net/download.html)
#     - 1080p 30fps/60fps
#     - 2160p 30fps
# NOTE: These files are large (97M -> $263M) so this may take time based on your network connection
#       If you do not have enough space on the SD Card partition, you can put these files instead on a USB thumbdrive
########################################################################
root@vcu_uz7ev_cc:~# mkdir -p /media/card/videos
root@vcu_uz7ev_cc:~# cd /media/card/videos

- 1080p Reference clip from the original repository
########################################################################
root@vcu_uz7ev_cc:/media/card/videos# wget http://distribution.bbb3d.renderfarming.net/video/mp4/bbb_sunflower_1080p_30fps_normal.mp4

Connecting to distribution.bbb3d.renderfarming.net (5.186.52.52:80)
bbb_sunflower_1080p_ 100% |***************************************************************************************************|   263M  0:00:00 ETA

########################################################################
# Inspect the video files
# -----------------------
# - Containers are 'Quicktime' MP4 format
#   - Parse (demux) using the 'qtdemux' plugin
#   - Why qtdemux? Quicktime was the basis of the MP4 specification...
# - Video is H.264 Encoded
# - Decode using the VCU 'omxh264dec' plugin
# - Audio is MPEG-4 AAC Encoded
# - Ignore the audio stream for this example
#   - But, this can be decoded with 'faad'
########################################################################
- Inspect the 1080p Reference Clip from the Original Repository
########################################################################
root@vcu_uz7ev_cc:/media/card/videos# gst-discoverer-1.0 bbb_sunflower_1080p_30fps_normal.mp4 

Analyzing file:///media/card/videos/bbb_sunflower_1080p_30fps_normal.mp4
Done discovering file:///media/card/videos/bbb_sunflower_1080p_30fps_normal.mp4
Missing plugins

Topology:
  container: Quicktime
    audio: AC-3 (ATSC A/52)
    audio: MPEG-1 Layer 3 (MP3)
    video: H.264 (High Profile)

Properties:
  Duration: 0:10:34.533333333
  Seekable: yes
  Tags: 
      video codec: H.264 / AVC
      maximum bitrate: 16715296
      bitrate: 2998712
      datetime: 2013-12-16T17:44:39Z
      title: Big Buck Bunny, Sunflower version
      composer: Sacha Goedegebure
      artist: Blender Foundation 2008, Janus Bager Kristensen 2013
      comment: Creative Commons Attribution 3.0 - http://bbb3d.renderfarming.net
      genre: Animation
      QT atom: buffer of 39 bytes
      container format: ISO MP4/M4A


- Do this again in verbose mode to see more information about the underlying video and audio channel formats
########################################################################
root@vcu_uz7ev_cc:/media/card/videos# gst-discoverer-1.0 -v bbb_sunflower_1080p_30fps_normal.mp4                                                               

Analyzing file:///media/card/videos/bbb_sunflower_1080p_30fps_normal.mp4
Done discovering file:///media/card/videos/bbb_sunflower_1080p_30fps_normal.mp4
Missing plugins
 (gstreamer|1.0|gst-discoverer-1.0|MPEG-1 Layer 3 (MP3) decoder|decoder-audio/mpeg, mpegversion=(int)1, mpegaudioversion=(int)1, layer=(int)3)
 (gstreamer|1.0|gst-discoverer-1.0|AC-3 (ATSC A/52) decoder|decoder-audio/x-ac3)

Topology:
  container: video/quicktime, variant=(string)iso
    audio: audio/x-ac3, framed=(boolean)true, rate=(int)48000, channels=(int)6, alignment=(string)frame
      Tags:
        None
      
      Codec:
        audio/x-ac3, framed=(boolean)true, rate=(int)48000, channels=(int)6, alignment=(string)frame
      Additional info:
        None
      Stream ID: (null)
      Language: <unknown>
      Channels: 6
      Sample rate: 48000
      Depth: 0
      Bitrate: 0
      Max bitrate: 0
    audio: audio/mpeg, mpegversion=(int)1, mpegaudioversion=(int)1, layer=(int)3, rate=(int)48000, channels=(int)2, parsed=(boolean)true
      Tags:
        None
      
      Codec:
        audio/mpeg, mpegversion=(int)1, mpegaudioversion=(int)1, layer=(int)3, rate=(int)48000, channels=(int)2, parsed=(boolean)true
      Additional info:
        None
      Stream ID: (null)
      Language: <unknown>
      Channels: 2
      Sample rate: 48000
      Depth: 0
      Bitrate: 0
      Max bitrate: 0
    video: video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, level=(string)4.1, profile=(string)high, width=(int)1920, height=(int)1080, framerate=(fraction)30
/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, chroma-format=(string)4:2:0, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, parsed=(boolean)true
      Tags:
        video codec: H.264 / AVC
        maximum bitrate: 16715296
        bitrate: 2998712
        datetime: 2013-12-16T17:44:39Z
        title: Big Buck Bunny, Sunflower version
        composer: Sacha Goedegebure
        artist: Blender Foundation 2008, Janus Bager Kristensen 2013
        comment: Creative Commons Attribution 3.0 - http://bbb3d.renderfarming.net
        genre: Animation
        QT atom: buffer of 39 bytes
        container format: ISO MP4/M4A
      
      Codec:
        video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, level=(string)4.1, profile=(string)high, width=(int)1920, height=(int)1080, framerate=(fraction)30/1,
 pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, chroma-format=(string)4:2:0, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, parsed=(boolean)true
      Additional info:
        None
      Stream ID: 90be03beb868dd676eb64b5b7b89225e1df38a3a66ac93c6d9d6eec97e6e093a/001
      Width: 1920
      Height: 1080
      Depth: 24
      Frame rate: 30/1
      Pixel aspect ratio: 1/1
      Interlaced: false
      Bitrate: 2998712
      Max bitrate: 16715296

Properties:
  Duration: 0:10:34.533333333
  Seekable: yes
  Tags: 
      video codec: H.264 / AVC
      maximum bitrate: 16715296
      bitrate: 2998712
      datetime: 2013-12-16T17:44:39Z
      title: Big Buck Bunny, Sunflower version
      composer: Sacha Goedegebure
      artist: Blender Foundation 2008, Janus Bager Kristensen 2013
      comment: Creative Commons Attribution 3.0 - http://bbb3d.renderfarming.net
      genre: Animation
      QT atom: buffer of 39 bytes
      container format: ISO MP4/M4A

########################################################################
# STAGE 1: Bring the video file into the pipeline
# - use the 'filesrc' element
########################################################################
root@vcu_uz7ev_cc:~# cd ~
root@vcu_uz7ev_cc:~# gst-inspect-1.0 filesrc

Factory Details:
  Rank                     primary (256)
  Long-name                File Source
  Klass                    Source/File
  Description              Read from arbitrary point in a file
...
Plugin Details:
  Name                     coreelements
  Description              GStreamer core elements
...
Pad Templates:
  SRC template: 'src'
    Availability: Always
    Capabilities:
      ANY
...
URI handling capabilities:
  Element can act as source.
  Supported URI protocols:
    file

Pads:
  SRC: 'src'
    Pad Template: 'src'

Element Properties:
...
  location            : Location of the file to read
                        flags: readable, writable, changeable only in NULL or READY state
                        String. Default: null

- Construct and test the first part of the pipeline 
########################################################################
root@vcu_uz7ev_cc:~# gst-launch-1.0 filesrc location=/media/card/videos/bbb_sunflower_1080p_30fps_normal.mp4 ! \
fakesink           

Setting pipeline to PAUSED ...
Pipeline is PREROLLING ...
Pipeline is PREROLLED ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
Got EOS from element "pipeline0".
Execution ended after 0:00:00.294631988
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...

########################################################################
# STAGE 2: Parse the video container format to split out video and audio components
# - use the 'qtdemux' element
# - name the instance 'videodemux'
# - access the video stream  using 'videodemux.video_0'
########################################################################
root@vcu_uz7ev_cc:~# gst-inspect-1.0 qtdemux

Factory Details:
  Rank                     primary (256)
  Long-name                QuickTime demuxer
  Klass                    Codec/Demuxer
  Description              Demultiplex a QuickTime file into audio and video streams
...
Plugin Details:
  Name                     isomp4
  Description              ISO base media file format support (mp4, 3gpp, qt, mj2)
...
Pad Templates:
  SINK template: 'sink'
    Availability: Always
    Capabilities:
      video/quicktime
      video/mj2
      audio/x-m4a
      application/x-3gp

  SRC template: 'video_%u'
    Availability: Sometimes
    Capabilities:
      ANY

  SRC template: 'audio_%u'
    Availability: Sometimes
    Capabilities:
      ANY

  SRC template: 'subtitle_%u'
    Availability: Sometimes
    Capabilities:
      ANY
...
Pads:
  SINK: 'sink'
    Pad Template: 'sink'

Element Properties:
  name                : The name of the object
                        flags: readable, writable
                        String. Default: "qtdemux0"
...

- Construct, add and test the next step in the pipeline
########################################################################
root@vcu_uz7ev_cc:~# gst-launch-1.0 -v filesrc location=/media/card/videos/bbb_sunflower_1080p_30fps_normal.mp4 ! \
qtdemux name=videodemux videodemux.video_0 ! \
fakesink           

Setting pipeline to PAUSED ...
Pipeline is PREROLLING ...
/GstPipeline:pipeline0/GstFakeSink:fakesink0.GstPad:sink: caps = video/x-h264, stream-format=(string)avc, alignment=(string)au, level=(string)4.1, profile=(string)high, codec_data=
(buffer)01640029ffe1001b67640029acca501e0089f970110000030001000003003c8f18319601000568e93b2c8b, width=(int)1920, height=(int)1080, framerate=(fraction)30/1, pixel-aspect-ratio=(fra
ction)1/1
Pipeline is PREROLLED ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
Got EOS from element "pipeline0".
Execution ended after 0:00:00.950800072
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...

########################################################################
# STAGE 3: Parse the H.264 stream
# - use the 'h264parse' element to convert stream formats and byte alignment
#   - video source file:
#   - stream format: 'avc'
#   - alignment: 'au'
# - omxh264dec accepts the following:
#   - stream format: 'byte-stream'
#   - alignment: 'au'
########################################################################
root@vcu_uz7ev_cc:~# gst-inspect-1.0 h264parse

Factory Details:
  Rank                     primary + 1 (257)
  Long-name                H.264 parser
  Klass                    Codec/Parser/Converter/Video
  Description              Parses H.264 streams
 ...
Plugin Details:
  Name                     videoparsersbad
  Description              videoparsers
...
Pad Templates:
  SRC template: 'src'
    Availability: Always
    Capabilities:
      video/x-h264
                 parsed: true
          stream-format: { (string)avc, (string)avc3, (string)byte-stream }
              alignment: { (string)au, (string)nal }

  SINK template: 'sink'
    Availability: Always
    Capabilities:
      video/x-h264
...
Element Properties:
  name                : The name of the object
                        flags: readable, writable
                        String. Default: "h264parse0"
  parent              : The parent of the object
                        flags: readable, writable
                        Object of type "GstObject"
  disable-passthrough : Force processing (disables passthrough)
                        flags: readable, writable
                        Boolean. Default: false
  config-interval     : Send SPS and PPS Insertion Interval in seconds (sprop parameter sets will be multiplexed in the data stream when detected.) (0 = disabled, -1 = send with ev
ery IDR frame)
                        flags: readable, writable
                        Integer. Range: -1 - 3600 Default: 0 

- Construct, add and test the next step in the pipeline
########################################################################
root@vcu_uz7ev_cc:~# gst-launch-1.0 -v filesrc location=/media/card/videos/bbb_sunflower_1080p_30fps_normal.mp4 ! qtdemux name=videodemux videodemux.video_0 ! h264parse ! fakesink           

Setting pipeline to PAUSED ...
Pipeline is PREROLLING ...
/GstPipeline:pipeline0/GstH264Parse:h264parse0.GstPad:src: caps = video/x-h264, stream-format=(string)avc, alignment=(string)au, level=(string)4.1, profile=(string)high, codec_data
=(buffer)01640029ffe1001b67640029acca501e0089f970110000030001000003003c8f18319601000568e93b2c8b, width=(int)1920, height=(int)1080, framerate=(fraction)30/1, pixel-aspect-ratio=(fr
action)1/1, interlace-mode=(string)progressive, chroma-format=(string)4:2:0, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, parsed=(boolean)true
/GstPipeline:pipeline0/GstFakeSink:fakesink0.GstPad:sink: caps = video/x-h264, stream-format=(string)avc, alignment=(string)au, level=(string)4.1, profile=(string)high, codec_data=
(buffer)01640029ffe1001b67640029acca501e0089f970110000030001000003003c8f18319601000568e93b2c8b, width=(int)1920, height=(int)1080, framerate=(fraction)30/1, pixel-aspect-ratio=(fra
ction)1/1, interlace-mode=(string)progressive, chroma-format=(string)4:2:0, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, parsed=(boolean)true
/GstPipeline:pipeline0/GstH264Parse:h264parse0.GstPad:sink: caps = video/x-h264, stream-format=(string)avc, alignment=(string)au, level=(string)4.1, profile=(string)high, codec_dat
a=(buffer)01640029ffe1001b67640029acca501e0089f970110000030001000003003c8f18319601000568e93b2c8b, width=(int)1920, height=(int)1080, framerate=(fraction)30/1, pixel-aspect-ratio=(f
raction)1/1
Pipeline is PREROLLED ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
Got EOS from element "pipeline0".
Execution ended after 0:00:01.590423481
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...

########################################################################
# STAGE 4a: Decode the video stream using the vcu
# - use the 'omxh264dec' element to decode the h.264 bytestream
#   - This plugin leverages the VCU decode engine
# - Let gstreamer auto-negotiate the capabilities (discover the right caps filter)
########################################################################
root@vcu_uz7ev_cc:~# gst-inspect-1.0 omxh264dec 

Factory Details:
  Rank                     primary + 1 (257)
  Long-name                OpenMAX H.264 Video Decoder
  Klass                    Codec/Decoder/Video
  Description              Decode H.264 video streams
...
Plugin Details:
  Name                     omx
  Description              GStreamer OpenMAX Plug-ins
...
Pad Templates:
  SINK template: 'sink'
    Availability: Always
    Capabilities:
      video/x-h264
              alignment: au
          stream-format: byte-stream
                  width: [ 1, 2147483647 ]
                 height: [ 1, 2147483647 ]

  SRC template: 'src'
    Availability: Always
    Capabilities:
      video/x-raw(memory:GLMemory)
                 format: RGBA
                  width: [ 1, 2147483647 ]
                 height: [ 1, 2147483647 ]
              framerate: [ 0/1, 2147483647/1 ]
      video/x-raw
                  width: [ 1, 2147483647 ]
                 height: [ 1, 2147483647 ]
              framerate: [ 0/1, 2147483647/1 ]
...
Pads:
  SINK: 'sink'
    Pad Template: 'sink'
  SRC: 'src'
    Pad Template: 'src'

Element Properties:
  name                : The name of the object
                        flags: readable, writable
                        String. Default: "omxh264dec-omxh264dec0"
...
  internal-entropy-buffers: Number of internal buffers used by the decoder to smooth out entropy decoding performance. Increasing it may improve tht
                        flags: readable, writable, changeable only in NULL or READY state
                        Unsigned Integer. Range: 2 - 16 Default: 5 
  latency-mode        : Decoder latency mode
                        flags: readable, writable
                        Enum "GstOMXVideoDecLatencyMode" Default: -1, "default"
                           (0): normal           - Normal mode
                           (1): reduced-latency  - Low ref dpb mode(reduced-latency)
                           (2): low-latency      - Low latency mode
                           (-1): default          - Component Default

- Construct, add and test the next step in the pipeline
**Note: This pipeline will take a while to complete, as it will parse and playback the entire video clip (to fakesink) 
########################################################################
root@vcu_uz7ev_cc:~# gst-launch-1.0 -v filesrc location=/media/card/videos/bbb_sunflower_1080p_30fps_normal.mp4 ! \
qtdemux name=videodemux videodemux.video_0 ! \
h264parse ! \
omxh264dec latency-mode=0 ! \
fakesink

Setting pipeline to PAUSED ...
Pipeline is PREROLLING ...
/GstPipeline:pipeline0/GstH264Parse:h264parse0.GstPad:sink: caps = video/x-h264, stream-format=(string)avc, alignment=(string)au, level=(string)4.1, profile=(string)high, codec_dat
a=(buffer)01640029ffe1001b67640029acca501e0089f970110000030001000003003c8f18319601000568e93b2c8b, width=(int)1920, height=(int)1080, framerate=(fraction)30/1, pixel-aspect-ratio=(f
raction)1/1
/GstPipeline:pipeline0/GstH264Parse:h264parse0.GstPad:src: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, level=(string)4.1, profile=(string)high, wi
dth=(int)1920, height=(int)1080, framerate=(fraction)30/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, chroma-format=(string)4:2:0, bit-depth-luma=(uint)8
, bit-depth-chroma=(uint)8, parsed=(boolean)true
Redistribute latency...
/GstPipeline:pipeline0/GstOMXH264Dec-omxh264dec:omxh264dec-omxh264dec0.GstPad:sink: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, level=(string)4.1,
 profile=(string)high, width=(int)1920, height=(int)1080, framerate=(fraction)30/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, chroma-format=(string)4:2:
0, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, parsed=(boolean)true
/GstPipeline:pipeline0/GstOMXH264Dec-omxh264dec:omxh264dec-omxh264dec0.GstPad:src: caps = video/x-raw(memory:GLMemory), format=(string)RGBA, width=(int)1920, height=(int)1080, inte
rlace-mode=(string)progressive, pixel-aspect-ratio=(fraction)1/1, colorimetry=(string)sRGB, framerate=(fraction)30/1
/GstPipeline:pipeline0/GstFakeSink:fakesink0.GstPad:sink: caps = video/x-raw(memory:GLMemory), format=(string)RGBA, width=(int)1920, height=(int)1080, interlace-mode=(string)progre
ssive, pixel-aspect-ratio=(fraction)1/1, colorimetry=(string)sRGB, framerate=(fraction)30/1
/GstPipeline:pipeline0/GstOMXH264Dec-omxh264dec:omxh264dec-omxh264dec0.GstPad:src: caps = video/x-raw, format=(string)NV12, width=(int)1920, height=(int)1080, interlace-mode=(strin
g)progressive, pixel-aspect-ratio=(fraction)1/1, chroma-site=(string)mpeg2, colorimetry=(string)bt709, framerate=(fraction)30/1
/GstPipeline:pipeline0/GstFakeSink:fakesink0.GstPad:sink: caps = video/x-raw, format=(string)NV12, width=(int)1920, height=(int)1080, interlace-mode=(string)progressive, pixel-aspe
ct-ratio=(fraction)1/1, chroma-site=(string)mpeg2, colorimetry=(string)bt709, framerate=(fraction)30/1
Pipeline is PREROLLED ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
Got EOS from element "pipeline0".
Execution ended after 0:07:21.039406673
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...

########################################################################
# STAGE 4b: Decode the video stream using the vcu
# - use the 'omxh264dec' element to decode the h.264 bytestream
#   - This plugin leverages the VCU decode engine
# - buffer data using the 'queue' element
# - Use gstreamer caps filter to specify the resolution/framerate allowed from the omxh264dec SRC pad
#   - use the 'video/x-raw' capabilities from the 'src' SRC template to specify the resolution and framerate
########################################################################
root@vcu_uz7ev_cc:~# gst-inspect-1.0 omxh264dec 

...
  SRC template: 'src'
    Availability: Always
    Capabilities:
      video/x-raw(memory:GLMemory)
                 format: RGBA
                  width: [ 1, 2147483647 ]
                 height: [ 1, 2147483647 ]
              framerate: [ 0/1, 2147483647/1 ]
      video/x-raw
                  width: [ 1, 2147483647 ]
                 height: [ 1, 2147483647 ]
              framerate: [ 0/1, 2147483647/1 ]
...

- Inspect the queue element
########################################################################
root@vcu_uz7ev_cc:~# gst-inspect-1.0 queue

Factory Details:
  Rank                     none (0)
  Long-name                Queue
  Klass                    Generic
  Description              Simple data queue
  Author                   Erik Walthinsen <omega@cse.ogi.edu>

Plugin Details:
  Name                     coreelements
  Description              GStreamer core elements
...
Pad Templates:
  SINK template: 'sink'
    Availability: Always
    Capabilities:
      ANY

  SRC template: 'src'
    Availability: Always
    Capabilities:
      ANY
...
Pads:
  SINK: 'sink'
    Pad Template: 'sink'
  SRC: 'src'
    Pad Template: 'src'

Element Properties:
...
  max-size-buffers    : Max. number of buffers in the queue (0=disable)
                        flags: readable, writable, changeable in NULL, READY, PAUSED or PLAYING state
                        Unsigned Integer. Range: 0 - 4294967295 Default: 200 
  max-size-bytes      : Max. amount of data in the queue (bytes, 0=disable)
                        flags: readable, writable, changeable in NULL, READY, PAUSED or PLAYING state
                        Unsigned Integer. Range: 0 - 4294967295 Default: 10485760 
...
  flush-on-eos        : Discard all data in the queue when an EOS event is received
                        flags: readable, writable, changeable in NULL, READY, PAUSED or PLAYING state
                        Boolean. Default: false
...

- Construct, add and test the resolution to the previous step in the pipeline
**Note: This pipeline will take a while to complete, as it will parse and playback the entire video clip (to fakesink) 
########################################################################
root@vcu_uz7ev_cc:~# gst-launch-1.0 -v filesrc location=/media/card/videos/bbb_sunflower_1080p_30fps_normal.mp4 ! \
qtdemux name=videodemux videodemux.video_0 ! \
h264parse ! \
omxh264dec latency-mode=0 ! video/x-raw,format=NV12,width=1920,height=1080,framerate=30/1 ! \
queue ! \
fakesink

Setting pipeline to PAUSED ...
Pipeline is PREROLLING ...
/GstPipeline:pipeline0/GstH264Parse:h264parse0.GstPad:sink: caps = video/x-h264, stream-format=(string)avc, alignment=(string)au, level=(string)4.1, profile=(string)high, codec_dat
a=(buffer)01640029ffe1001b67640029acca501e0089f970110000030001000003003c8f18319601000568e93b2c8b, width=(int)1920, height=(int)1080, framerate=(fraction)30/1, pixel-aspect-ratio=(f
raction)1/1
/GstPipeline:pipeline0/GstH264Parse:h264parse0.GstPad:src: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, level=(string)4.1, profile=(string)high, wi
dth=(int)1920, height=(int)1080, framerate=(fraction)30/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, chroma-format=(string)4:2:0, bit-depth-luma=(uint)8
, bit-depth-chroma=(uint)8, parsed=(boolean)true
Redistribute latency...
/GstPipeline:pipeline0/GstOMXH264Dec-omxh264dec:omxh264dec-omxh264dec0.GstPad:sink: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, level=(string)4.1,
 profile=(string)high, width=(int)1920, height=(int)1080, framerate=(fraction)30/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, chroma-format=(string)4:2:
0, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, parsed=(boolean)true
/GstPipeline:pipeline0/GstOMXH264Dec-omxh264dec:omxh264dec-omxh264dec0.GstPad:src: caps = video/x-raw(memory:GLMemory), format=(string)RGBA, width=(int)1920, height=(int)1080, inte
rlace-mode=(string)progressive, pixel-aspect-ratio=(fraction)1/1, colorimetry=(string)sRGB, framerate=(fraction)30/1
/GstPipeline:pipeline0/GstOMXH264Dec-omxh264dec:omxh264dec-omxh264dec0.GstPad:src: caps = video/x-raw, format=(string)NV12, width=(int)1920, height=(int)1080, interlace-mode=(strin
g)progressive, pixel-aspect-ratio=(fraction)1/1, chroma-site=(string)mpeg2, colorimetry=(string)bt709, framerate=(fraction)30/1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)NV12, width=(int)1920, height=(int)1080, interlace-mode=(string)progressive, pixel-a
spect-ratio=(fraction)1/1, chroma-site=(string)mpeg2, colorimetry=(string)bt709, framerate=(fraction)30/1
/GstPipeline:pipeline0/GstQueue:queue0.GstPad:sink: caps = video/x-raw, format=(string)NV12, width=(int)1920, height=(int)1080, interlace-mode=(string)progressive, pixel-aspect-rat
io=(fraction)1/1, chroma-site=(string)mpeg2, colorimetry=(string)bt709, framerate=(fraction)30/1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)NV12, width=(int)1920, height=(int)1080, interlace-mode=(string)progressive, pixel-
aspect-ratio=(fraction)1/1, chroma-site=(string)mpeg2, colorimetry=(string)bt709, framerate=(fraction)30/1
/GstPipeline:pipeline0/GstQueue:queue0.GstPad:src: caps = video/x-raw, format=(string)NV12, width=(int)1920, height=(int)1080, interlace-mode=(string)progressive, pixel-aspect-rati
o=(fraction)1/1, chroma-site=(string)mpeg2, colorimetry=(string)bt709, framerate=(fraction)30/1
/GstPipeline:pipeline0/GstFakeSink:fakesink0.GstPad:sink: caps = video/x-raw, format=(string)NV12, width=(int)1920, height=(int)1080, interlace-mode=(string)progressive, pixel-aspe
ct-ratio=(fraction)1/1, chroma-site=(string)mpeg2, colorimetry=(string)bt709, framerate=(fraction)30/1
Pipeline is PREROLLED ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
Got EOS from element "pipeline0".
Execution ended after 0:07:21.192087689
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...


########################################################################
# Inspect the DisplayPort Interface (ZynqMP)
# - DisplayPort has a DRM device bus-id = 'fd4a0000.zynqmp-display'
# - Use 'modetest' to query device configuration
# - See working with DisplayPort displays reference for more information
########################################################################
root@vcu_uz7ev_cc:~# modetest -D fd4a0000.zynqmp-display

...
Connectors:
id      encoder status          name            size (mm)       modes   encoders
38      37      connected       DP-1            520x290         37      37
  modes:
        name refresh (Hz) hdisp hss hse htot vdisp vss vse vtot)
  1920x1080 60 1920 2008 2052 2200 1080 1084 1089 1125 148500 flags: phsync, pvsync; type: preferred, driver
...
CRTCs:
id      fb      pos     size
36      44      (0,0)   (1920x1080)
  1920x1080 60 1920 2008 2052 2200 1080 1084 1089 1125 148500 flags: phsync, pvsync; type: preferred, driver
  props:
        29 output_color:
                flags: enum
                enums: rgb=0 ycrcb444=1 ycrcb422=2 yonly=3
                value: 0
        30 bg_c0:
                flags: range
                values: 0 4095
                value: 0
        31 bg_c1:
                flags: range
                values: 0 4095
                value: 0
        32 bg_c2:
                flags: range
                values: 0 4095
                value: 0

Planes:
id      crtc    fb      CRTC x,y        x,y     gamma size      possible crtcs
34      0       0       0,0             0,0     0               0x00000001
  formats: VYUY UYVY YUYV YVYU YU16 YV16 YU24 YV24 NV16 NV61 BG24 RG24 XB24 XR24 XB30 XR30 YU12 YV12 NV12 NV21 XV15 XV20
  props:
        6 type:
                flags: immutable enum
                enums: Overlay=0 Primary=1 Cursor=2
                value: 0
        33 tpg:
                flags: range
                values: 0 1
                value: 0
35      36      44      0,0             0,0     0               0x00000001
  formats: AB24 AR24 RA24 BA24 BG24 RG24 RA15 BA15 RA12 BA12 RG16 BG16
  props:
        6 type:
                flags: immutable enum
                enums: Overlay=0 Primary=1 Cursor=2
                value: 1
        27 alpha:
                flags: range
                values: 0 255
                value: 255
        28 g_alpha_en:
                flags: range
                values: 0 1
                value: 0
...

- You can also query this information from the debugfs entry
########################################################################
root@vcu_uz7ev_cc:~# cat /sys/kernel/debug/dri/0/state

...
plane[35]: plane-1
        crtc=crtc-0
        fb=44
                format=RG16 little-endian (0x36314752)
                        modifier=0x0
                size=1920x2160
                layers:
                        pitch[0]=3840
                        offset[0]=0
        crtc-pos=1920x1080+0+0
        src-pos=1920.000000x1080.000000+0.000000+0.000000
        rotation=1
crtc[36]: crtc-0
        enable=1
        active=1
        planes_changed=1
        mode_changed=0
        active_changed=0
        connectors_changed=0
        color_mgmt_changed=0
        plane_mask=2
        connector_mask=1
        encoder_mask=1
        mode: 0:"1920x1080" 60 148500 1920 2008 2052 2200 1080 1084 1089 1125 0x48 0x5
connector[38]: DP-1
        crtc=crtc-0

########################################################################
# STAGE 5a: Send video out to the KMSSINK (Display Output)
# - use the 'kmssink' element to send decoded video to the display
# - Let gstreamer auto-negotiate display properties
# - This pipeline works on a display set to the same resolution as the decoded video (1080p)
# NOTE: If you're using the display to output the serial console, you must first turn off alpha-blending
#       Otherwise you won't see video displayed on the screen
########################################################################
root@vcu_uz7ev_cc:~# gst-inspect-1.0 kmssink

Factory Details:
  Rank                     secondary (128)
  Long-name                KMS video sink
  Klass                    Sink/Video
  Description              Video sink using the Linux kernel mode setting API
  Author                   V�íctor J�áquez <vjaquez@igalia.com>

Plugin Details:
  Name                     kms
  Description              Video sink using the Linux kernel mode setting API
...
Pad Templates:
  SINK template: 'sink'
    Availability: Always
    Capabilities:
      video/x-raw
                 format: { (string)BGRA, (string)BGRx, (string)RGBA, (string)RGBx, (string)RGB, (string)BGR, (string)UYVY, (string)YUY2, (string)YVYU, (string)I420, (string)YV12, (string)Y42B, (str}
                  width: [ 1, 2147483647 ]
                 height: [ 1, 2147483647 ]
              framerate: [ 0/1, 2147483647/1 ]
...
Pads:
  SINK: 'sink'
    Pad Template: 'sink'

Element Properties:
...
  bus-id              : DRM bus ID
                        flags: readable, writable
                        String. Default: null
  connector-id        : DRM connector id
                        flags: readable, writable
                        Integer. Range: -1 - 2147483647 Default: -1 
  plane-id            : DRM plane id
                        flags: readable, writable
                        Integer. Range: -1 - 2147483647 Default: -1 
...
  fullscreen-overlay  : When enabled, the sink sets CRTC size same as input video size
                        flags: readable, writable
                        Boolean. Default: false
...

- Turn off alpha blending (if required)
########################################################################
root@vcu_uz7ev_cc:~# modetest -D fd4a0000.zynqmp-display -w 35:g_alpha_en:0

- Construct, add and test the next step in the pipeline
**Note: This pipeline will take a while to complete, as it will parse and playback the entire video clip
########################################################################
root@vcu_uz7ev_cc:~# gst-launch-1.0 -v filesrc location=/media/card/videos/bbb_sunflower_1080p_30fps_normal.mp4 ! \
qtdemux name=videodemux videodemux.video_0 ! \
h264parse ! \
omxh264dec latency-mode=0 ! video/x-raw,format=NV12,width=1920,height=1080,framerate=30/1 ! \
queue ! \
kmssink bus-id=fd4a0000.zynqmp-display                                                             

Setting pipeline to PAUSED ...
Pipeline is PREROLLING ...
/GstPipeline:pipeline0/GstKMSSink:kmssink0: display-width = 1920
/GstPipeline:pipeline0/GstKMSSink:kmssink0: display-height = 1080
/GstPipeline:pipeline0/GstH264Parse:h264parse0.GstPad:sink: caps = video/x-h264, stream-format=(string)avc, alignment=(string)au, level=(string)4.1, profile=(string)high, codec_dat
a=(buffer)01640029ffe1001b67640029acca501e0089f970110000030001000003003c8f18319601000568e93b2c8b, width=(int)1920, height=(int)1080, framerate=(fraction)30/1, pixel-aspect-ratio=(f
raction)1/1
/GstPipeline:pipeline0/GstH264Parse:h264parse0.GstPad:src: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, level=(string)4.1, profile=(string)high, wi
dth=(int)1920, height=(int)1080, framerate=(fraction)30/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, chroma-format=(string)4:2:0, bit-depth-luma=(uint)8
, bit-depth-chroma=(uint)8, parsed=(boolean)true
Redistribute latency...
/GstPipeline:pipeline0/GstOMXH264Dec-omxh264dec:omxh264dec-omxh264dec0.GstPad:sink: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, level=(string)4.1,
 profile=(string)high, width=(int)1920, height=(int)1080, framerate=(fraction)30/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, chroma-format=(string)4:2:
0, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, parsed=(boolean)true
/GstPipeline:pipeline0/GstOMXH264Dec-omxh264dec:omxh264dec-omxh264dec0.GstPad:src: caps = video/x-raw(memory:GLMemory), format=(string)RGBA, width=(int)1920, height=(int)1080, inte
rlace-mode=(string)progressive, pixel-aspect-ratio=(fraction)1/1, colorimetry=(string)sRGB, framerate=(fraction)30/1
/GstPipeline:pipeline0/GstOMXH264Dec-omxh264dec:omxh264dec-omxh264dec0.GstPad:src: caps = video/x-raw, format=(string)NV12, width=(int)1920, height=(int)1080, interlace-mode=(strin
g)progressive, pixel-aspect-ratio=(fraction)1/1, chroma-site=(string)mpeg2, colorimetry=(string)bt709, framerate=(fraction)30/1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)NV12, width=(int)1920, height=(int)1080, interlace-mode=(string)progressive, pixel-a
spect-ratio=(fraction)1/1, chroma-site=(string)mpeg2, colorimetry=(string)bt709, framerate=(fraction)30/1
/GstPipeline:pipeline0/GstQueue:queue0.GstPad:src: caps = video/x-raw, format=(string)NV12, width=(int)1920, height=(int)1080, interlace-mode=(string)progressive, pixel-aspect-rati
o=(fraction)1/1, chroma-site=(string)mpeg2, colorimetry=(string)bt709, framerate=(fraction)30/1
/GstPipeline:pipeline0/GstKMSSink:kmssink0.GstPad:sink: caps = video/x-raw, format=(string)NV12, width=(int)1920, height=(int)1080, interlace-mode=(string)progressive, pixel-aspect
-ratio=(fraction)1/1, chroma-site=(string)mpeg2, colorimetry=(string)bt709, framerate=(fraction)30/1
/GstPipeline:pipeline0/GstQueue:queue0.GstPad:sink: caps = video/x-raw, format=(string)NV12, width=(int)1920, height=(int)1080, interlace-mode=(string)progressive, pixel-aspect-rat
io=(fraction)1/1, chroma-site=(string)mpeg2, colorimetry=(string)bt709, framerate=(fraction)30/1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)NV12, width=(int)1920, height=(int)1080, interlace-mode=(string)progressive, pixel-
aspect-ratio=(fraction)1/1, chroma-site=(string)mpeg2, colorimetry=(string)bt709, framerate=(fraction)30/1
Pipeline is PREROLLED ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
Got EOS from element "pipeline0".
Execution ended after 0:07:21.192087689
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...

- Restore alpha blending (if required)
########################################################################
root@vcu_uz7ev_cc:~# modetest -D fd4a0000.zynqmp-display -w 35:g_alpha_en:1

########################################################################
# STAGE 5b: Send video out to the KMSSINK (Display Output)
# - use the 'kmssink' element to send decoded video to the display
# - Let gstreamer auto-negotiate display properties
# - This pipeline works on a display set to a different resolution as the source video (i.e. 4K)
# NOTE: You do not have to turn off alpha blending in this case, because the kmssink will setup a display
#       mode change to change resolution when using the fullscreen-overlay=true property
########################################################################
**Note: This pipeline will take a while to complete, as it will parse and playback (to fakesink) the entire video clip
root@vcu_uz7ev_cc:~# gst-launch-1.0 -v filesrc location=/media/card/videos/bbb_sunflower_1080p_30fps_normal.mp4 ! \
qtdemux name=videodemux videodemux.video_0 ! \
h264parse ! \
omxh264dec latency-mode=0 ! video/x-raw,format=NV12,width=1920,height=1080,framerate=30/1 ! \
queue ! \
kmssink bus-id=fd4a0000.zynqmp-display fullscreen-overlay=true

Setting pipeline to PAUSED ...
Pipeline is PREROLLING ...
/GstPipeline:pipeline0/GstKMSSink:kmssink0: display-width = 3440
/GstPipeline:pipeline0/GstKMSSink:kmssink0: display-height = 1440
/GstPipeline:pipeline0/GstH264Parse:h264parse0.GstPad:sink: caps = video/x-h264, stream-format=(string)avc, alignment=(string)au, level=(string)4.1, profile=(string)high, codec_data=(buffer)01640029ffe1001b67640029acca501e0089f970110000030001000003003c8f18319601000568e93b2c8b, width=(int)1920, height=(int)1080, framerate=(fraction)30/1, pixel-aspect-ratio=(fraction)1/1
/GstPipeline:pipeline0/GstH264Parse:h264parse0.GstPad:src: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, level=(string)4.1, profile=(string)high, width=(int)1920, height=(int)1080, framerate=(fraction)30/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, chroma-format=(string)4:2:0, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, parsed=(boolean)true
Redistribute latency...
/GstPipeline:pipeline0/GstOMXH264Dec-omxh264dec:omxh264dec-omxh264dec0.GstPad:sink: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, level=(string)4.1, profile=(string)high, width=(int)1920, height=(int)1080, framerate=(fraction)30/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, chroma-format=(string)4:2:0, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, parsed=(boolean)true
/GstPipeline:pipeline0/GstOMXH264Dec-omxh264dec:omxh264dec-omxh264dec0.GstPad:src: caps = video/x-raw(memo[  344.862961] PLL: shutdownry:GLMemory), format=(string)RGBA, width=(int)1920, height=(int)[  344.871064] PLL: enable
1080, interlace-mode=(string)progressive, pixel-aspect-ratio=(fraction)1/1, colorimetry=(string)sRGB, framerate=(fraction)30/1
/GstPipeline:pipeline0/GstOMXH264Dec-omxh264dec:omxh264dec-omxh264dec0.GstPad:src: caps = video/x-raw, format=(string)NV12, width=(int)1920, height=(int)1080, interlace-mode=(string)progressive, pixel-aspect-ratio=(fraction)1/1, chroma-site=(string)mpeg2, colorimetry=(string)bt709, framerate=(fraction)30/1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)NV12, width=(int)1920, height=(int)1080, interlace-mode=(string)progressive, pixel-aspect-ratio=(fraction)1/1, chroma-site=(string)mpeg2, colorimetry=(string)bt709, framerate=(fraction)30/1
/GstPipeline:pipeline0/GstQueue:queue0.GstPad:sink: caps = video/x-raw, format=(string)NV12, width=(int)1920, height=(int)1080, interlace-mode=(string)progressive, pixel-aspect-ratio=(fraction)1/1, chroma-site=(string)mpeg2, colorimetry=(string)bt709, framerate=(fraction)30/1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)NV12, width=(int)1920, height=(int)1080, interlace-mode=(string)progressive, pixel-aspect-ratio=(fraction)1/1, chroma-site=(string)mpeg2, colorimetry=(string)bt709, framerate=(fraction)30/1
/GstPipeline:pipeline0/GstQueue:queue0.GstPad:src: caps = video/x-raw, format=(string)NV12, width=(int)1920, height=(int)1080, interlace-mode=(string)progressive, pixel-aspect-ratio=(fraction)1/1, chroma-site=(string)mpeg2, colorimetry=(string)bt709, framerate=(fraction)30/1
/GstPipeline:pipeline0/GstKMSSink:kmssink0.GstPad:sink: caps = video/x-raw, format=(string)NV12, width=(int)1920, height=(int)1080, interlace-mode=(string)progressive, pixel-aspect-ratio=(fraction)1/1, chroma-site=(string)mpeg2, colorimetry=(string)bt709, framerate=(fraction)30/1
Pipeline is PREROLLED ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
^Chandling interrupt.
Interrupt: Stopping pipeline ...
Execution ended after 0:00:04.943900017
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
[  350.168058] PLL: shutdown
[  350.176622] PLL: enable
Freeing pipeline ...
