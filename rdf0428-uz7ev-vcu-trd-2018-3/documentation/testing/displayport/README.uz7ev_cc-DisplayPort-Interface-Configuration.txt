########################################################################
# 1.) Figure out what devices are used for the display (/dev/dri/xxx)
#     - We see the /dev/dri/card0 is assigned to zynqmp-display and is an amba device
#       - This is part of the PS, and the zynqmp-dpsub compatibility indiciates this is associated with DISPLAYPORT
########################################################################
root@vcu_uz7ev_cc:~# drmdevice

device[0]
        available_nodes 0001
        nodes
                nodes[0] /dev/dri/card0
        bustype 0002
        businfo
                platform
                        fullname        /amba/zynqmp-display@fd4a0000
        deviceinfo
                platform
                        compatible
                                xlnx,zynqmp-dpsub-1.7

Opening device 0 node /dev/dri/card0
device[0]
        available_nodes 0001
        nodes
                nodes[0] /dev/dri/card0
        bustype 0002
        businfo
                platform
                        fullname        /amba/zynqmp-display@fd4a0000
        deviceinfo
                platform
                        compatible
                                xlnx,zynqmp-dpsub-1.7

########################################################################
# 2.) Find out more from the DEBUGFS entries for DRI
########################################################################
root@vcu_uz7ev_cc:~# find /sys/kernel/debug/dri -type f

/sys/kernel/debug/dri/0/DP-1/edid_override
/sys/kernel/debug/dri/0/DP-1/force
/sys/kernel/debug/dri/0/state
/sys/kernel/debug/dri/0/gem_names
/sys/kernel/debug/dri/0/clients
/sys/kernel/debug/dri/0/name
root@vcu_uz7ev_cc:~# cat /sys/kernel/debug/dri/0/state
plane[34]: plane-0
        crtc=(null)
        fb=0
        crtc-pos=0x0+0+0
        src-pos=0.000000x0.000000+0.000000+0.000000
        rotation=1
plane[35]: plane-1
        crtc=(null)
        fb=0
        crtc-pos=0x0+0+0
        src-pos=0.000000x0.000000+0.000000+0.000000
        rotation=1
crtc[36]: crtc-0
        enable=0
        active=0
        planes_changed=0
        mode_changed=0
        active_changed=0
        connectors_changed=0
        color_mgmt_changed=0
        plane_mask=0
        connector_mask=0
        encoder_mask=0
        mode: 0:"" 0 0 0 0 0 0 0 0 0 0 0x0 0x0
connector[38]: DP-1
        crtc=(null)
root@vcu_uz7ev_cc:~# [  375.650568] PLL: enable
[  375.862351] Console: switching to colour frame buffer device 240x67
[  375.904483] zynqmp-display fd4a0000.zynqmp-display: fb0:  frame buffer device

########################################################################
# 3.) Check out the Display Port Device (dri/0)
#      - In the case below, DisplayPort is disconnected so you'll see that reflected in the debugfs state
########################################################################
root@vcu_uz7ev_cc:~# cat /sys/kernel/debug/dri/0/state

plane[34]: plane-0
        crtc=(null)
        fb=0
        crtc-pos=0x0+0+0
        src-pos=0.000000x0.000000+0.000000+0.000000
        rotation=1
plane[35]: plane-1
        crtc=crtc-0
        fb=43
                format=RG16 little-endian (0x36314752)
                        modifier=0x0
                size=1920x2160
                layers:
                        pitch[0]=3840
                        offset[0]=0
        crtc-pos=1920x1080+0+0
        src-pos=1920.000000x1080.000000+0.000000+0.000000
        rotation=1
crtc[36]: crtc-0
        enable=1
        active=1
        planes_changed=1
        mode_changed=0
        active_changed=0
        connectors_changed=0
        color_mgmt_changed=0
        plane_mask=2
        connector_mask=1
        encoder_mask=1
        mode: 0:"1920x1080" 30 74250 1920 2008 2052 2200 1080 1084 1089 1125 0x40 0x5
connector[38]: DP-1
        crtc=crtc-0

########################################################################
# 4.) Check out the DisplayPort "device name"
########################################################################
root@vcu_uz7ev_cc:~# ls -al /sys/class/drm/card0/device

lrwxrwxrwx 1 root root 0 Jun 14 16:00 /sys/class/drm/card0/device -> ../../../fd4a0000.zynqmp-display

root@vcu_uz7ev_cc:~# cat /sys/kernel/debug/dri/0/name

xlnx dev=fd4a0000.zynqmp-display unique=fd4a0000.zynqmp-display

########################################################################
# 5.) Check what modes are supported by the DisplayPort connection
#      - In the case below, this is connected to a 26" ViewSonic 1920x1200 monitor using DisplayPort
########################################################################
root@vcu_uz7ev_cc:~# cat /sys/class/drm/card0-DP-1/modes

1920x1080
1920x1080
1920x1080
1920x1080
1920x1080
1280x720
1280x720
1280x720
1024x768
1024x768
1024x768
832x624
800x600
800x600
800x600
800x600
720x576
720x480
720x480
640x480
640x480
640x480
640x480
640x480
720x400

########################################################################
# 6.) Check the detailed Configuration for the DisplayPort driver
#      - In the case below, this is connected to a 26" ViewSonic 1920x1200 monitor using DisplayPort
########################################################################
root@vcu_uz7ev_cc:~# modetest -D fd4a0000.zynqmp-display

Encoders:
id      crtc    type    possible crtcs  possible clones
37      36      TMDS    0x00000001      0x00000000

Connectors:
id      encoder status          name            size (mm)       modes   encoders
38      37      connected       DP-1            520x290         25      37
  modes:
        name refresh (Hz) hdisp hss hse htot vdisp vss vse vtot)
  1920x1080 30 1920 2008 2052 2200 1080 1084 1089 1125 74250 flags: phsync, pvsync; type: driver
  1920x1080 30 1920 2008 2052 2200 1080 1084 1089 1125 74176 flags: phsync, pvsync; type: driver
  1920x1080 25 1920 2448 2492 2640 1080 1084 1089 1125 74250 flags: phsync, pvsync; type: driver
  1920x1080 24 1920 2558 2602 2750 1080 1084 1089 1125 74250 flags: phsync, pvsync; type: driver
  1920x1080 24 1920 2558 2602 2750 1080 1084 1089 1125 74176 flags: phsync, pvsync; type: driver
  1280x720 60 1280 1390 1430 1650 720 725 730 750 74250 flags: phsync, pvsync; type: driver
  1280x720 60 1280 1390 1430 1650 720 725 730 750 74176 flags: phsync, pvsync; type: driver
  1280x720 50 1280 1720 1760 1980 720 725 730 750 74250 flags: phsync, pvsync; type: driver
  1024x768 75 1024 1040 1136 1312 768 769 772 800 78750 flags: phsync, pvsync; type: driver
  1024x768 70 1024 1048 1184 1328 768 771 777 806 75000 flags: nhsync, nvsync; type: driver
  1024x768 60 1024 1048 1184 1344 768 771 777 806 65000 flags: nhsync, nvsync; type: driver
  832x624 75 832 864 928 1152 624 625 628 667 57284 flags: nhsync, nvsync; type: driver
  800x600 75 800 816 896 1056 600 601 604 625 49500 flags: phsync, pvsync; type: driver
  800x600 72 800 856 976 1040 600 637 643 666 50000 flags: phsync, pvsync; type: driver
  800x600 60 800 840 968 1056 600 601 605 628 40000 flags: phsync, pvsync; type: driver
  800x600 56 800 824 896 1024 600 601 603 625 36000 flags: phsync, pvsync; type: driver
  720x576 50 720 732 796 864 576 581 586 625 27000 flags: nhsync, nvsync; type: driver
  720x480 60 720 736 798 858 480 489 495 525 27027 flags: nhsync, nvsync; type: driver
  720x480 60 720 736 798 858 480 489 495 525 27000 flags: nhsync, nvsync; type: driver
  640x480 75 640 656 720 840 480 481 484 500 31500 flags: nhsync, nvsync; type: driver
  640x480 73 640 664 704 832 480 489 492 520 31500 flags: nhsync, nvsync; type: driver
  640x480 67 640 704 768 864 480 483 486 525 30240 flags: nhsync, nvsync; type: driver
  640x480 60 640 656 752 800 480 490 492 525 25200 flags: nhsync, nvsync; type: driver
  640x480 60 640 656 752 800 480 490 492 525 25175 flags: nhsync, nvsync; type: driver
  720x400 70 720 738 846 900 400 412 414 449 28320 flags: nhsync, pvsync; type: driver
  props:
        1 EDID:
                flags: immutable blob
                blobs:

                value:
                        00ffffffffffff005a63317901010101
                        241b0104c5341d78320bd5a25750a128
                        0f5054bfef80b300a940a9c095009040
                        8180814081c0023a801871382d40582c
                        450009252100001e000000ff00554536
                        3137333634313138320a000000fd0032
                        4b185211000a202020202020000000fc
                        00564132343532205345524945530182
                        02031ff152900504030207061f202122
                        14131211161501230907038301000002
                        3a801871382d40582c45000925210000
                        1e011d8018711c1620582c2500092521
                        00009e011d007251d01e206e28550009
                        252100001e8c0ad08a20e02d10103e96
                        00092521000018023a80d072382d4010
                        2c458009252100001e0000000000003d
        2 DPMS:
                flags: enum
                enums: On=0 Standby=1 Suspend=2 Off=3
                value: 0
        5 link-status:
                flags: enum
                enums: Good=0 Bad=1
                value: 0
        39 sync:
                flags: range
                values: 0 1
                value: 0
        40 bpc:
                flags: enum
                enums: 6BPC=6 8BPC=8 10BPC=10 12BPC=12
                value: 8

CRTCs:
id      fb      pos     size
36      43      (0,0)   (1920x1080)
  1920x1080 30 1920 2008 2052 2200 1080 1084 1089 1125 74250 flags: phsync, pvsync; type: driver
  props:
        29 output_color:
                flags: enum
                enums: rgb=0 ycrcb444=1 ycrcb422=2 yonly=3
                value: 0
        30 bg_c0:
                flags: range
                values: 0 4095
                value: 0
        31 bg_c1:
                flags: range
                values: 0 4095
                value: 0
        32 bg_c2:
                flags: range
                values: 0 4095
                value: 0

Planes:
id      crtc    fb      CRTC x,y        x,y     gamma size      possible crtcs
34      0       0       0,0             0,0     0               0x00000001
  formats: VYUY UYVY YUYV YVYU YU16 YV16 YU24 YV24 NV16 NV61 BG24 RG24 XB24 XR24 XB30 XR30 YU12 YV12 NV12 NV21 XV15 XV20
  props:
        6 type:
                flags: immutable enum
                enums: Overlay=0 Primary=1 Cursor=2
                value: 0
        33 tpg:
                flags: range
                values: 0 1
                value: 0
35      36      43      0,0             0,0     0               0x00000001
  formats: AB24 AR24 RA24 BA24 BG24 RG24 RA15 BA15 RA12 BA12 RG16 BG16
  props:
        6 type:
                flags: immutable enum
                enums: Overlay=0 Primary=1 Cursor=2
                value: 1
        27 alpha:
                flags: range
                values: 0 255
                value: 255
        28 g_alpha_en:
                flags: range
                values: 0 1
                value: 1

Frame buffers:
id      size    pitch

########################################################################
# 7.) Test a resolution change on the DP Connection
- Resolution is currently at 1920x1200 or 3840x2160 (per the configuration above).
- Use the 'modetest' command (use 'modetest --help to see options')
- Specify a device using '-D'
  - Device name from #5 above (fd4a0000.zynqmp-display)
- Specify a supported display mode using '-s'
  - option format '<connector_id>[,<connector_id>][@crtc_id]:<mode>[-vrefresh>[@<format>]'
  - Connector ID from #4 above = <connector_id> = 38 (connector[38]:DP-1)
  - CRTC ID from #4 above = @crtc_id = 36 (crtc[36]: crtc-0)
  - Mode = supported mode from #6 above = <mode> = 1280x720
  - Vertical Refresh rate = vrefresh = 60 (crtc[37]: mode 0: "height x width" vrefresh ...)
  - Format from #4 above = <format> = AR24 (plane[35]: plane-6: fb=60: format=AR24 little endian...)
  - Mode string = 38@36:1280x720-60@AR24
########################################################################
- Check current resolution
########################################################################
root@vcu_uz7ev_cc:~# cat /sys/kernel/debug/dri/0/state

plane[34]: plane-0
        crtc=(null)
        fb=0
        crtc-pos=0x0+0+0
        src-pos=0.000000x0.000000+0.000000+0.000000
        rotation=1
plane[35]: plane-1
        crtc=crtc-0
        fb=43
                format=RG16 little-endian (0x36314752)
                        modifier=0x0
                size=1920x2160
                layers:
                        pitch[0]=3840
                        offset[0]=0
        crtc-pos=1920x1080+0+0
        src-pos=1920.000000x1080.000000+0.000000+0.000000
        rotation=1
crtc[36]: crtc-0
        enable=1
        active=1
        planes_changed=1
        mode_changed=0
        active_changed=0
        connectors_changed=0
        color_mgmt_changed=0
        plane_mask=2
        connector_mask=1
        encoder_mask=1
        mode: 0:"1920x1080" 30 74250 1920 2008 2052 2200 1080 1084 1089 1125 0x40 0x5
connector[38]: DP-1
        crtc=crtc-0

- Run modetest in the background to change the resolution
########################################################################
root@vcu_uz7ev_cc:~# modetest -D fd4a0000.zynqmp-display -s 38@36:1280x720-60@AR24 &

[1] 2466
setting mode 1280x720-60Hz@AR24 on connectors 38, crtc 36
[  508.811756] PLL: shutdown
[  508.818900] PLL: enable

root@vcu_uz7ev_cc:~# 

- Check current resolution
########################################################################
root@vcu_uz7ev_cc:~# cat /sys/kernel/debug/dri/0/state

plane[34]: plane-0
...
plane[35]: plane-1
        crtc=crtc-0
        fb=44
                format=AR24 little-endian (0x34325241)
                        modifier=0x0
                size=1280x720
 ...
 crtc[36]: crtc-0
        enable=1
        active=1
 ...
        mode: 0:"1280x720" 60 74250 1280 1390 1430 1650 720 725 730 750 0x40 0x5
connector[38]: DP-1
        crtc=crtc-0

- Resume modetest command and exit with <CTRL-C>
########################################################################
root@vcu_uz7ev_cc:~# fg
modetest -M xlnx -D fd4a0000.zynqmp-display -s 38@36:1280x720-60@AR24
^C[  533.647401] PLL: shutdown
[  533.651082] PLL: enable

- Verify original resolution was restored.
########################################################################
root@vcu_uz7ev_cc:~# cat /sys/kernel/debug/dri/0/state

plane[34]: plane-0
...
plane[35]: plane-1
        crtc=crtc-0
        fb=43
                format=RG16 little-endian (0x36314752)
                        modifier=0x0
                size=1920x2160
...
crtc[36]: crtc-0
        enable=1
        active=1
...
        mode: 0:"1920x1080" 30 74250 1920 2008 2052 2200 1080 1084 1089 1125 0x40 0x5
connector[38]: DP-1
        crtc=crtc-0