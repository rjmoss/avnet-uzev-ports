########################################################################
# GOAL:
# - Use GStreamer to Stream webcam video to a host and display webcam video on local DisplayPort Display 
# - Review the gstreamer pipeline elements
#   -[WEBCAM] ->
#     |--> [V4L2SRC] -> [TEE] -> [QUEUE] -> [VIDEOCONVERT] -> [OMXH264ENC] -> [QUEUE] -> [RTP] -> [UDPSINK]
#                         |----> [QUEUE] -> [KMSSINK(DISPLAY PORT)]
# VLC Media Player:
#   -[HOST] <-- [SDP FILE]
########################################################################
- Step through building the example pipeline above
- During construction of the pipeline, we will use the 'fakesink' element for testing purposes
  - 'fakesink' provides a dummy connection for pads to pass data to (no processing takes place on this data)

########################################################################
# Connect a USB Webcam
# Example: Logitech C930e (1080p, 5.7Mpixel)
########################################################################
- Plug in USB Webcam and check the Kernel dmesg log
########################################################################
root@zcu106_vcu_trd:~# dmesg

[  157.550236] usb 1-1.2: new high-speed USB device number 3 using xhci-hcd
[  159.972354] usb 1-1.2: New USB device found, idVendor=046d, idProduct=0843
[  159.982652] usb 1-1.2: New USB device strings: Mfr=0, Product=2, SerialNumber=1
[  159.993368] usb 1-1.2: Product: Logitech Webcam C930e
[  160.001835] usb 1-1.2: SerialNumber: 84BF815E
[  160.010586] uvcvideo: Found UVC 1.00 device Logitech Webcam C930e (046d:0843)
[  160.021565] uvcvideo 1-1.2:1.0: Entity type for entity Processing 3 was not initialized!
[  160.033119] uvcvideo 1-1.2:1.0: Entity type for entity Extension 6 was not initialized!
[  160.044567] uvcvideo 1-1.2:1.0: Entity type for entity Extension 8 was not initialized!
[  160.055974] uvcvideo 1-1.2:1.0: Entity type for entity Extension 9 was not initialized!
[  160.067339] uvcvideo 1-1.2:1.0: Entity type for entity Extension 10 was not initialized!
[  160.078779] uvcvideo 1-1.2:1.0: Entity type for entity Extension 11 was not initialized!
[  160.090176] uvcvideo 1-1.2:1.0: Entity type for entity Extension 12 was not initialized!
[  160.101560] uvcvideo 1-1.2:1.0: Entity type for entity Extension 13 was not initialized!
[  160.112898] uvcvideo 1-1.2:1.0: Entity type for entity Camera 1 was not initialized!
[  160.124032] input: Logitech Webcam C930e as /devices/platform/amba/ff9d0000.usb0/fe200000.dwc3/xhci-hcd.0.auto/usb1/1-1/1-1.2/1-1.2:1.0/input0


#########################################################################
# Check to see that the webcam was recognized and registered properly
########################################################################
- Use v4l2-ctl to verify the USB Webcam is registered properly as a video device
########################################################################
root@vcu_uz7ev_cc:~# v4l2-ctl --list-devices

[  329.652209] usb 1-1.2: reset high-speed USB device number 3 using xhci-hcd
Logitech Webcam C930e (usb-xhci-hcd.0.auto-1.2):
        /dev/video0

#########################################################################
- Display supported resolutions and framerates for each format
########################################################################
root@vcu_uz7ev_cc:~# v4l2-ctl -d /dev/video0 --list-formats-ext

ioctl: VIDIOC_ENUM_FMT
        Index       : 0
        Type        : Video Capture
        Pixel Format: 'YUYV'
        Name        : YUYV 4:2:2
...
                Size: Discrete 1280x720
                        Interval: Discrete 0.100s (10.000 fps)
                        Interval: Discrete 0.133s (7.500 fps)
                        Interval: Discrete 0.200s (5.000 fps)
                Size: Discrete 1600x896
                        Interval: Discrete 0.133s (7.500 fps)
                        Interval: Discrete 0.200s (5.000 fps)
                Size: Discrete 1920x1080
                        Interval: Discrete 0.200s (5.000 fps)
                Size: Discrete 2304x1296
                        Interval: Discrete 0.500s (2.000 fps)
                Size: Discrete 2304x1536
                        Interval: Discrete 0.500s (2.000 fps)

        Index       : 1
        Type        : Video Capture
        Pixel Format: 'MJPG' (compressed)
        Name        : Motion-JPEG
...
                Size: Discrete 1280x720
                        Interval: Discrete 0.033s (30.000 fps)
                        Interval: Discrete 0.042s (24.000 fps)
                        Interval: Discrete 0.050s (20.000 fps)
                        Interval: Discrete 0.067s (15.000 fps)
                        Interval: Discrete 0.100s (10.000 fps)
                        Interval: Discrete 0.133s (7.500 fps)
                        Interval: Discrete 0.200s (5.000 fps)
                Size: Discrete 1600x896
                        Interval: Discrete 0.033s (30.000 fps)
                        Interval: Discrete 0.042s (24.000 fps)
                        Interval: Discrete 0.050s (20.000 fps)
                        Interval: Discrete 0.067s (15.000 fps)
                        Interval: Discrete 0.100s (10.000 fps)
                        Interval: Discrete 0.133s (7.500 fps)
                        Interval: Discrete 0.200s (5.000 fps)
                Size: Discrete 1920x1080
                        Interval: Discrete 0.033s (30.000 fps)
                        Interval: Discrete 0.042s (24.000 fps)
                        Interval: Discrete 0.050s (20.000 fps)
                        Interval: Discrete 0.067s (15.000 fps)
                        Interval: Discrete 0.100s (10.000 fps)
                        Interval: Discrete 0.133s (7.500 fps)
                        Interval: Discrete 0.200s (5.000 fps)

########################################################################
# STAGE 1a: Open a window into the USB Webcam
# - use the 'v4l2src' element
# - Let gstreamer auto-negotiate the capabilities (discover the right caps filter)
########################################################################
root@vcu_uz7ev_cc:~# gst-inspect-1.0 v4l2src

Factory Details:
  Rank                     primary (256)
  Long-name                Video (video4linux2) Source
  Klass                    Source/Video
  Description              Reads frames from a Video4Linux2 device
  Author                   Edgard Lima <edgard.lima@gmail.com>, Stefan Kost <ensonic@users.sf.net>

Plugin Details:
  Name                     video4linux2
  Description              elements for Video 4 Linux
...
Pads:
  SRC: 'src'
    Pad Template: 'src'

Element Properties:
...
  device              : Device location
                        flags: readable, writable
                        String. Default: "/dev/video0"
...

- Construct and test the first part of the pipeline 
**Note: This pipeline will run until cancelled manually using <CTRL-C>
########################################################################
root@vcu_uz7ev_cc:~# gst-launch-1.0 -v v4l2src device=/dev/video0 ! fakesink

Setting pipeline to PAUSED ...
Pipeline is live and does not need PREROLL ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
/GstPipeline:pipeline0/GstV4l2Src:v4l2src0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)2304, height=(int)1536, pixel-aspect-1
/GstPipeline:pipeline0/GstFakeSink:fakesink0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)2304, height=(int)1536, pixel-aspe1
^Chandling interrupt.
Interrupt: Stopping pipeline ...
Execution ended after 0:00:04.598849926
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...

########################################################################
# STAGE 1b: Open a window into the USB Webcam
# - use the 'v4l2src' element
# - Use gstreamer element properties to specify the camera data stream resolution and format
# Note: Looking at the X-RAW formats, you'll notice what might appear to be a mismatch:
# - The camera supports YUYV 4:2:2
# - The v4l2src element does not explicitly support YUYV
# - Look at http://www.fourcc.org/yuv.php:
#   - YUYV is a duplicate name for the YUY2 format
#   - http://www.fourcc.org/yuv-yuy2/
# Note: YUYV is a '4:2:2' format, which is important for a later stage in the pipeline
########################################################################
root@vcu_uz7ev_cc:~# gst-inspect-1.0 v4l2src

Factory Details:
  Rank                     primary (256)
  Long-name                Video (video4linux2) Source
  Klass                    Source/Video
  Description              Reads frames from a Video4Linux2 device
  Author                   Edgard Lima <edgard.lima@gmail.com>, Stefan Kost <ensonic@users.sf.net>

Plugin Details:
  Name                     video4linux2
  Description              elements for Video 4 Linux
...
Pad Templates:
  SRC template: 'src'
    Availability: Always
    Capabilities:
      image/jpeg
...
      video/x-raw
                 format: { (string)RGB16, (string)BGR, (string)RGB, (string)GRAY8, (string)GRAY16_LE, (string)GRAY16_BE, (string)YVU9, (string)YV12, (string)YUY2, (string)YVYU, (st
ring)UYVY, (string)Y42B, (string)Y41B, (string)YUV9, (string)NV12_64Z32, (string)NV24, (string)NV12_10LE32, (string)NV16_10LE32, (string)NV61, (string)NV16, (string)NV21, (string)N
V12, (string)I420, (string)BGRA, (string)BGRx, (string)ARGB, (string)xRGB, (string)BGR15, (string)RGB15 }
                  width: [ 1, 32768 ]
                 height: [ 1, 32768 ]
              framerate: [ 0/1, 2147483647/1 ]

...
Pads:
  SRC: 'src'
    Pad Template: 'src'

Element Properties:
...
  device              : Device location
                        flags: readable, writable
                        String. Default: "/dev/video0"
...
                           (8388608): SECAM-Lc         - SECAM-Lc
  io-mode             : I/O mode
                        flags: readable, writable
                        Enum "GstV4l2IOMode" Default: 0, "auto"
                           (0): auto             - GST_V4L2_IO_AUTO
                           (1): rw               - GST_V4L2_IO_RW
                           (2): mmap             - GST_V4L2_IO_MMAP
                           (3): userptr          - GST_V4L2_IO_USERPTR
                           (4): dmabuf           - GST_V4L2_IO_DMABUF
                           (5): dmabuf-import    - GST_V4L2_IO_DMABUF_IMPORT
...

- Construct, add and test the next step in the pipeline
**Note: This pipeline will run until cancelled manually using <CTRL-C>
########################################################################
root@vcu_uz7ev_cc:~# gst-launch-1.0 -v v4l2src device=/dev/video0 ! video/x-raw,format=YUY2,width=1280,height=720,framerate=10/1 ! \
fakesink

Setting pipeline to PAUSED ...
Pipeline is live and does not need PREROLL ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
/GstPipeline:pipeline0/GstV4l2Src:v4l2src0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fra1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerat1
/GstPipeline:pipeline0/GstFakeSink:fakesink0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framera1
^Chandling interrupt.
Interrupt: Stopping pipeline ...
Execution ended after 0:00:03.847423010
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...

########################################################################
# STAGE 2: Create a copy of the element data to share with a second element pad
# - use the 'tee' element
# - the original copy will be used for the RTP stream pipeline
# - the second copy will be used for a display-port preview of the webcam
########################################################################
root@vcu_uz7ev_cc:~# gst-inspect-1.0 tee

Factory Details:
  Rank                     none (0)
  Long-name                Tee pipe fitting
  Klass                    Generic
  Description              1-to-N pipe fitting
  Author                   Erik Walthinsen <omega@cse.ogi.edu>, Wim Taymans <wim@fluendo.com>

Plugin Details:
  Name                     coreelements
  Description              GStreamer core elements
...
Pad Templates:
  SINK template: 'sink'
    Availability: Always
    Capabilities:
      ANY

  SRC template: 'src_%u'
    Availability: On request
    Capabilities:
      ANY
...
Pads:
  SINK: 'sink'
    Pad Template: 'sink'

Element Properties:
  name                : The name of the object
                        flags: readable, writable
                        String. Default: "tee0"
...

- Construct, add and test the next step in the pipeline
**Note: with tee, must terminate both data paths
########################################################################
root@vcu_uz7ev_cc:~# gst-launch-1.0 -v v4l2src device=/dev/video0 ! video/x-raw,format=YUY2,width=1280,height=720,framerate=10/1 ! \
tee name=tee_preview ! fakesink \
tee_preview. ! fakesink

Setting pipeline to PAUSED ...
Pipeline is live and does not need PREROLL ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
/GstPipeline:pipeline0/GstV4l2Src:v4l2src0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fra1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerat1
/GstPipeline:pipeline0/GstTee:tee_preview.GstTeePad:src_0: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=1
/GstPipeline:pipeline0/GstFakeSink:fakesink0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(1
/GstPipeline:pipeline0/GstTee:tee_preview.GstTeePad:src_1: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=1
/GstPipeline:pipeline0/GstFakeSink:fakesink1.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(1
/GstPipeline:pipeline0/GstTee:tee_preview.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fra1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framera1
^Chandling interrupt.
Interrupt: Stopping pipeline ...
Execution ended after 0:00:04.860905916
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...


########################################################################
# STAGE 3-1: Buffer data after the tee
# - buffer data using the 'queue' element (after encoding)
#######################################################################
root@vcu_uz7ev_cc:~# gst-inspect-1.0 queue

Factory Details:
  Rank                     none (0)
  Long-name                Queue
  Klass                    Generic
  Description              Simple data queue
  Author                   Erik Walthinsen <omega@cse.ogi.edu>

Plugin Details:
  Name                     coreelements
  Description              GStreamer core elements
...
Pad Templates:
  SINK template: 'sink'
    Availability: Always
    Capabilities:
      ANY

  SRC template: 'src'
    Availability: Always
    Capabilities:
      ANY
...
Pads:
  SINK: 'sink'
    Pad Template: 'sink'
  SRC: 'src'
    Pad Template: 'src'

Element Properties:
...
  max-size-buffers    : Max. number of buffers in the queue (0=disable)
                        flags: readable, writable, changeable in NULL, READY, PAUSED or PLAYING state
                        Unsigned Integer. Range: 0 - 4294967295 Default: 200 
  max-size-bytes      : Max. amount of data in the queue (bytes, 0=disable)
                        flags: readable, writable, changeable in NULL, READY, PAUSED or PLAYING state
                        Unsigned Integer. Range: 0 - 4294967295 Default: 10485760 
...
  flush-on-eos        : Discard all data in the queue when an EOS event is received
                        flags: readable, writable, changeable in NULL, READY, PAUSED or PLAYING state
                        Boolean. Default: false
...


- Construct, add and test the next step in the pipeline
**Note: This pipeline will run until cancelled manually using <CTRL-C>
########################################################################
root@vcu_uz7ev_cc:~# gst-launch-1.0 -v v4l2src device=/dev/video0 ! video/x-raw,format=YUY2,width=1280,height=720,framerate=10/1 ! \
tee name=tee_preview ! queue ! fakesink \
tee_preview. ! fakesink

Setting pipeline to PAUSED ...
Pipeline is live and does not need PREROLL ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
/GstPipeline:pipeline0/GstV4l2Src:v4l2src0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstTee:tee_preview.GstTeePad:src_0: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstQueue:queue0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstQueue:queue0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstTee:tee_preview.GstTeePad:src_1: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstTee:tee_preview.GstTeePad:src_1: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstFakeSink:fakesink1.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstTee:tee_preview.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
^Chandling interrupt.
Interrupt: Stopping pipeline ...
Execution ended after 0:00:03.417102706
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...

########################################################################
# STAGE 4-1: Convert format to a supported VCU colorspace
# - use the 'videoconvert' element
# - the input camera data format is YUYV (YUY2) which is 4:2:2
# See PG252 for information on supported encoder data formats
# - It's completely "unclear", but here are the formats supported by the h264 encoder
# - NV12 8-bit/10-bit YUV 4:2:0
# - NV16 8-bit/10-bit YUV 4:2:2
########################################################################
root@vcu_uz7ev_cc:~# gst-inspect-1.0 videoconvert

Factory Details:
  Rank                     none (0)
  Long-name                Colorspace converter
  Klass                    Filter/Converter/Video
  Description              Converts video from one colorspace to another
  Author                   GStreamer maintainers <gstreamer-devel@lists.freedesktop.org>

Plugin Details:
  Name                     videoconvert
  Description              Colorspace conversion
...
Pad Templates:
  SINK template: 'sink'
    Availability: Always
    Capabilities:
      video/x-raw
                 format: { (string)I420, (string)YV12, (string)YUY2, (string)UYVY, (string)AYUV, (string)RGBx, (string)BGRx, (string)xRGB, (string)xBGR, (string)RGBA, (string)BGRA,
 (string)ARGB, (string)ABGR, (string)RGB, (string)BGR, (string)Y41B, (string)Y42B, (string)YVYU, (string)Y444, (string)v210, (string)v216, (string)NV12, (string)NV21, (string)GRAY8
, (string)GRAY16_BE, (string)GRAY16_LE, (string)v308, (string)RGB16, (string)BGR16, (string)RGB15, (string)BGR15, (string)UYVP, (string)A420, (string)RGB8P, (string)YUV9, (string)Y
VU9, (string)IYU1, (string)ARGB64, (string)AYUV64, (string)r210, (string)I420_10BE, (string)I420_10LE, (string)I422_10BE, (string)I422_10LE, (string)Y444_10BE, (string)Y444_10LE, (
string)GBR, (string)GBR_10BE, (string)GBR_10LE, (string)NV16, (string)NV24, (string)NV12_64Z32, (string)A420_10BE, (string)A420_10LE, (string)A422_10BE, (string)A422_10LE, (string)
A444_10BE, (string)A444_10LE, (string)NV61, (string)P010_10BE, (string)P010_10LE, (string)IYU2, (string)VYUY, (string)GBRA, (string)GBRA_10BE, (string)GBRA_10LE, (string)GBR_12BE, 
(string)GBR_12LE, (string)GBRA_12BE, (string)GBRA_12LE, (string)I420_12BE, (string)I420_12LE, (string)I422_12BE, (string)I422_12LE, (string)Y444_12BE, (string)Y444_12LE, (string)GR
AY10_LE32, (string)NV12_10LE32, (string)NV16_10LE32 }
                  width: [ 1, 2147483647 ]
                 height: [ 1, 2147483647 ]
              framerate: [ 0/1, 2147483647/1 ]
...
  SRC template: 'src'
    Availability: Always
    Capabilities:
      video/x-raw
                 format: { (string)I420, (string)YV12, (string)YUY2, (string)UYVY, (string)AYUV, (string)RGBx, (string)BGRx, (string)xRGB, (string)xBGR, (string)RGBA, (string)BGRA,
 (string)ARGB, (string)ABGR, (string)RGB, (string)BGR, (string)Y41B, (string)Y42B, (string)YVYU, (string)Y444, (string)v210, (string)v216, (string)NV12, (string)NV21, (string)GRAY8
, (string)GRAY16_BE, (string)GRAY16_LE, (string)v308, (string)RGB16, (string)BGR16, (string)RGB15, (string)BGR15, (string)UYVP, (string)A420, (string)RGB8P, (string)YUV9, (string)Y
VU9, (string)IYU1, (string)ARGB64, (string)AYUV64, (string)r210, (string)I420_10BE, (string)I420_10LE, (string)I422_10BE, (string)I422_10LE, (string)Y444_10BE, (string)Y444_10LE, (
string)GBR, (string)GBR_10BE, (string)GBR_10LE, (string)NV16, (string)NV24, (string)NV12_64Z32, (string)A420_10BE, (string)A420_10LE, (string)A422_10BE, (string)A422_10LE, (string)
A444_10BE, (string)A444_10LE, (string)NV61, (string)P010_10BE, (string)P010_10LE, (string)IYU2, (string)VYUY, (string)GBRA, (string)GBRA_10BE, (string)GBRA_10LE, (string)GBR_12BE, 
(string)GBR_12LE, (string)GBRA_12BE, (string)GBRA_12LE, (string)I420_12BE, (string)I420_12LE, (string)I422_12BE, (string)I422_12LE, (string)Y444_12BE, (string)Y444_12LE, (string)GR
AY10_LE32, (string)NV12_10LE32, (string)NV16_10LE32 }
                  width: [ 1, 2147483647 ]
                 height: [ 1, 2147483647 ]
              framerate: [ 0/1, 2147483647/1 ]
...
Pads:
  SINK: 'sink'
    Pad Template: 'sink'
  SRC: 'src'
    Pad Template: 'src'

Element Properties:
...

- Construct, add and test the next step in the pipeline
**Note: This pipeline will run until cancelled manually using <CTRL-C>
########################################################################
root@vcu_uz7ev_cc:~# gst-launch-1.0 -v v4l2src device=/dev/video0 ! video/x-raw,format=YUY2,width=1280,height=720,framerate=10/1 ! \
tee name=tee_preview ! queue ! \                               
videoconvert ! video/x-raw,format=NV16,width=1280,height=720,framerate=10/1 ! fakesink \
tee_preview. ! fakesink

Setting pipeline to PAUSED ...
Pipeline is live and does not need PREROLL ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
/GstPipeline:pipeline0/GstV4l2Src:v4l2src0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstTee:tee_preview.GstTeePad:src_0: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstQueue:queue0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstTee:tee_preview.GstTeePad:src_1: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstQueue:queue0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstFakeSink:fakesink1.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstTee:tee_preview.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstVideoConvert:videoconvert0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
/GstPipeline:pipeline0/GstCapsFilter:capsfilter1.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
/GstPipeline:pipeline0/GstFakeSink:fakesink0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
/GstPipeline:pipeline0/GstCapsFilter:capsfilter1.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
/GstPipeline:pipeline0/GstVideoConvert:videoconvert0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, format=(string)YUY2, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
^Chandling interrupt.
Interrupt: Stopping pipeline ...
Execution ended after 0:00:04.091970296
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...

########################################################################
# STAGE 5-1: Route the data to the VCU encoder
# - use the 'omxh264enc' element
#   - This plugin leverages the VCU encoder engine
# See PG252 for information on supported encoder data formats
# - It's completely "unclear", but here are the formats supported by the h264 encoder
# - NV12 8-bit/10-bit YUV 4:2:0
# - NV16 8-bit/10-bit YUV 4:2:2
########################################################################
root@vcu_uz7ev_cc:~# gst-inspect-1.0 omxh264enc

Factory Details:
  Rank                     primary + 1 (257)
  Long-name                OpenMAX H.264 Video Encoder
  Klass                    Codec/Encoder/Video
  Description              Encode H.264 video streams
  Author                   Sebastian Dr�öge <sebastian.droege@collabora.co.uk>

Plugin Details:
  Name                     omx
  Description              GStreamer OpenMAX Plug-ins
...
Pad Templates:
  SINK template: 'sink'
    Availability: Always
    Capabilities:
      video/x-raw
                  width: [ 1, 2147483647 ]
                 height: [ 1, 2147483647 ]
              framerate: [ 0/1, 2147483647/1 ]
         interlace-mode: { (string)interleaved, (string)mixed, (string)alternate }
            field-order: { (string)top-field-first, (string)bottom-field-first }

  SRC template: 'src'
    Availability: Always
    Capabilities:
      video/x-h264
                  width: [ 16, 4096 ]
                 height: [ 16, 4096 ]
...
Pads:
  SINK: 'sink'
    Pad Template: 'sink'
  SRC: 'src'
    Pad Template: 'src'

Element Properties:
...
  qos                 : Handle Quality-of-Service events from downstream
                        flags: readable, writable
                        Boolean. Default: false
  control-rate        : Bitrate control method
                        flags: readable, writable, changeable only in NULL or READY state
                        Enum "GstOMXVideoEncControlRate" Default: -1, "default"
                           (0): disable          - Disable
                           (1): variable         - Variable
                           (2): constant         - Constant
                           (3): variable-skip-frames - Variable Skip Frames
                           (4): constant-skip-frames - Constant Skip Frames
                           (2130706433): low-latency      - Low Latency
                           (-1): default          - Component Default
  target-bitrate      : Target bitrate in Kbps (0xffffffff=component default)
                        flags: readable, writable, changeable in NULL, READY, PAUSED or PLAYING state
                        Unsigned Integer. Range: 0 - 4294967295 Default: 4294967295 
 ...
  qp-mode             : QP control mode used by the VCU encoder
                        flags: readable, writable, changeable only in NULL or READY state
                        Enum "GstOMXVideoEncQpMode" Default: -1, "default"
                           (0): uniform          - Use the same QP for all coding units of the frame
                           (2): auto             - Let the VCU encoder change the QP for each coding unit according to its content
                           (1): roi              - Adjust QP according to the regions of interest defined on each frame. Must be set to handle ROI .
                           (-1): default          - Component Default
...
  gop-mode            : Group Of Pictures mode
                        flags: readable, writable, changeable only in NULL or READY state
                        Enum "GstOMXVideoEncGopMode" Default: 0, "basic"
                           (0): basic            - Basic GOP settings
                           (1): pyramidal        - Advanced GOP pattern with hierarchical B-frames
                           (3): low-delay-p      - Single I-frame followed by P-frames only
                           (4): low-delay-b      - Single I-frame followed by B-frames only
                           (2): adaptive         - Advanced GOP pattern with adaptive B-frames
...
  latency-mode        : Encoder latency mode
                        flags: readable, writable
                        Enum "GstOMXVideoEncLatencyMode" Default: 0, "normal"
                           (0): normal           - Normal mode
                           (1): low-latency      - Low latency mode
                           (-1): default          - Component Default
  default-roi-quality : The default quality level to apply to each Region of Interest
                        flags: readable, writable
                        Enum "GstOMXVideoEncRoiQuality" Default: 0, "high"
                           (0): high             - Delta QP of -5
                           (1): medium           - Delta QP of 0
                           (2): low              - Delta QP of +5
                           (3): dont-care        - Maximum delta QP value
...

- Construct, add and test the next step in the pipeline
**Note: This pipeline will run until cancelled manually using <CTRL-C>
########################################################################
root@vcu_uz7ev_cc:~# gst-launch-1.0 -v v4l2src device=/dev/video0 ! \
video/x-raw,format=YUY2,width=1280,height=720,framerate=10/1 ! \
tee name=tee_preview ! queue ! \
videoconvert ! video/x-raw,format=NV16,width=1280,height=720,framerate=10/1 ! \
omxh264enc ! fakesink \
tee_preview. ! fakesink

Setting pipeline to PAUSED ...
ERROR: Pipeline doesn't want to pause.
ERROR: from element /GstPipeline:pipeline0/GstV4l2Src:v4l2src0: Cannot identify device '/dev/video9'.
Additional debug info:
../../../git/sys/v4l2/v4l2_calls.c(614): gst_v4l2_open (): /GstPipeline:pipeline0/GstV4l2Src:v4l2src0:
system error: No such file or directory
Setting pipeline to NULL ...
Freeing pipeline ...
root@vcu_uz7ev_cc:~# gst-launch-1.0 -v v4l2src device=/dev/video0 ! \
> video/x-raw,format=YUY2,width=1280,height=720,framerate=10/1 ! \
> tee name=tee_preview ! queue ! \
> videoconvert ! video/x-raw,format=NV16,width=1280,height=720,framerate=10/1 ! \
> omxh264enc ! fakesink \
> tee_preview. ! fakesink
Setting pipeline to PAUSED ...
Pipeline is live and does not need PREROLL ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
/GstPipeline:pipeline0/GstV4l2Src:v4l2src0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstTee:tee_preview.GstTeePad:src_0: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstQueue:queue0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstQueue:queue0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstTee:tee_preview.GstTeePad:src_1: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstFakeSink:fakesink1.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstTee:tee_preview.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstVideoConvert:videoconvert0.GstPad:src: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
/GstPipeline:pipeline0/GstCapsFilter:capsfilter1.GstPad:src: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
Redistribute latency...
/GstPipeline:pipeline0/GstOMXH264Enc-omxh264enc:omxh264enc-omxh264enc0.GstPad:sink: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
/GstPipeline:pipeline0/GstCapsFilter:capsfilter1.GstPad:sink: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
/GstPipeline:pipeline0/GstVideoConvert:videoconvert0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
!! Warning : Adapting profile to support bitdepth and chroma mode
!! The specified Level is too low and will be adjusted !!
/GstPipeline:pipeline0/GstOMXH264Enc-omxh264enc:omxh264enc-omxh264enc0.GstPad:src: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, profile=(string)high-4:2:2, level=(string)3.1, chroma-format=(string)4:2:2, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, width=(int)1280, height=(int)720, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)10/1, interlace-mode=(string)progressive, colorimetry=(string)bt709, chroma-site=(string)mpeg2
/GstPipeline:pipeline0/GstFakeSink:fakesink0.GstPad:sink: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, profile=(string)high-4:2:2, level=(string)3.1, chroma-format=(string)4:2:2, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, width=(int)1280, height=(int)720, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)10/1, interlace-mode=(string)progressive, colorimetry=(string)bt709, chroma-site=(string)mpeg2
^Chandling interrupt.
Interrupt: Stopping pipeline ...
Execution ended after 0:00:04.047967366
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...

########################################################################
# STAGE 6-1: Buffer data after the encoder
# - buffer data using the 'queue' element (after encoding)
########################################################################
root@vcu_uz7ev_cc:~# gst-inspect-1.0 queue

Factory Details:
  Rank                     none (0)
  Long-name                Queue
  Klass                    Generic
  Description              Simple data queue
  Author                   Erik Walthinsen <omega@cse.ogi.edu>

Plugin Details:
  Name                     coreelements
  Description              GStreamer core elements
...
Pad Templates:
  SINK template: 'sink'
    Availability: Always
    Capabilities:
      ANY

  SRC template: 'src'
    Availability: Always
    Capabilities:
      ANY
...
Pads:
  SINK: 'sink'
    Pad Template: 'sink'
  SRC: 'src'
    Pad Template: 'src'

Element Properties:
...
  max-size-buffers    : Max. number of buffers in the queue (0=disable)
                        flags: readable, writable, changeable in NULL, READY, PAUSED or PLAYING state
                        Unsigned Integer. Range: 0 - 4294967295 Default: 200 
  max-size-bytes      : Max. amount of data in the queue (bytes, 0=disable)
                        flags: readable, writable, changeable in NULL, READY, PAUSED or PLAYING state
                        Unsigned Integer. Range: 0 - 4294967295 Default: 10485760 
...
  flush-on-eos        : Discard all data in the queue when an EOS event is received
                        flags: readable, writable, changeable in NULL, READY, PAUSED or PLAYING state
                        Boolean. Default: false
...


- Construct, add and test the next step in the pipeline
**Note: This pipeline will run until cancelled manually using <CTRL-C>
########################################################################
root@vcu_uz7ev_cc:~# gst-launch-1.0 -v v4l2src device=/dev/video0 ! \
video/x-raw,format=YUY2,width=1280,height=720,framerate=10/1 ! \
tee name=tee_preview ! queue ! \
videoconvert ! video/x-raw,format=NV16,width=1280,height=720,framerate=10/1 ! \
omxh264enc ! queue ! fakesink \
tee_preview. ! fakesink

Setting pipeline to PAUSED ...
Pipeline is live and does not need PREROLL ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
/GstPipeline:pipeline0/GstV4l2Src:v4l2src0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstTee:tee_preview.GstTeePad:src_0: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstQueue:queue0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstQueue:queue0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstTee:tee_preview.GstTeePad:src_1: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstFakeSink:fakesink1.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstTee:tee_preview.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstVideoConvert:videoconvert0.GstPad:src: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
/GstPipeline:pipeline0/GstCapsFilter:capsfilter1.GstPad:src: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
Redistribute latency...
/GstPipeline:pipeline0/GstOMXH264Enc-omxh264enc:omxh264enc-omxh264enc0.GstPad:sink: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
/GstPipeline:pipeline0/GstCapsFilter:capsfilter1.GstPad:sink: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
/GstPipeline:pipeline0/GstVideoConvert:videoconvert0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
!! Warning : Adapting profile to support bitdepth and chroma mode
!! The specified Level is too low and will be adjusted !!
/GstPipeline:pipeline0/GstOMXH264Enc-omxh264enc:omxh264enc-omxh264enc0.GstPad:src: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, profile=(string)high-4:2:2, level=(string)3.1, chroma-format=(string)4:2:2, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, width=(int)1280, height=(int)720, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)10/1, interlace-mode=(string)progressive, colorimetry=(string)bt709, chroma-site=(string)mpeg2
/GstPipeline:pipeline0/GstQueue:queue1.GstPad:sink: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, profile=(string)high-4:2:2, level=(string)3.1, chroma-format=(string)4:2:2, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, width=(int)1280, height=(int)720, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)10/1, interlace-mode=(string)progressive, colorimetry=(string)bt709, chroma-site=(string)mpeg2
/GstPipeline:pipeline0/GstQueue:queue1.GstPad:sink: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, profile=(string)high-4:2:2, level=(string)3.1, chroma-format=(string)4:2:2, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, width=(int)1280, height=(int)720, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)10/1, interlace-mode=(string)progressive, colorimetry=(string)bt709, chroma-site=(string)mpeg2
/GstPipeline:pipeline0/GstFakeSink:fakesink0.GstPad:sink: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, profile=(string)high-4:2:2, level=(string)3.1, chroma-format=(string)4:2:2, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, width=(int)1280, height=(int)720, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)10/1, interlace-mode=(string)progressive, colorimetry=(string)bt709, chroma-site=(string)mpeg2
^Chandling interrupt.
Interrupt: Stopping pipeline ...
Execution ended after 0:00:03.595994655
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...


########################################################################
# STAGE 7-1: Setup the RTP Stream
# - use the 'rtph264pay' element to setup the H.264 payload
# - Let gstreamer auto-negotiate properties
########################################################################
root@vcu_uz7ev_cc:~# gst-inspect-1.0 rtph264pay

Factory Details:
  Rank                     secondary (128)
  Long-name                RTP H264 payloader
  Klass                    Codec/Payloader/Network/RTP
  Description              Payload-encode H264 video into RTP packets (RFC 3984)
  Author                   Laurent Glayal <spglegle@yahoo.fr>

Plugin Details:
  Name                     rtp
  Description              Real-time protocol plugins
...
Pad Templates:
  SRC template: 'src'
    Availability: Always
    Capabilities:
      application/x-rtp
                  media: video
                payload: [ 96, 127 ]
             clock-rate: 90000
          encoding-name: H264

  SINK template: 'sink'
    Availability: Always
    Capabilities:
      video/x-h264
          stream-format: avc
              alignment: au
      video/x-h264
          stream-format: byte-stream
              alignment: { (string)nal, (string)au }
...
Pads:
  SRC: 'src'
    Pad Template: 'src'
  SINK: 'sink'
    Pad Template: 'sink'

Element Properties:
...
  config-interval     : Send SPS and PPS Insertion Interval in seconds (sprop parameter sets will be multiplexed in the data stream when detected.) (0 = disabled, -1 = send with every IDR fr
ame)
                        flags: readable, writable
                        Integer. Range: -1 - 3600 Default: 0 


- Construct, add and test the next step in the pipeline
**Note: This pipeline will run until cancelled manually using <CTRL-C>
########################################################################
root@vcu_uz7ev_cc:~# gst-launch-1.0 -v v4l2src device=/dev/video0 ! \
video/x-raw,format=YUY2,width=1280,height=720,framerate=10/1 ! \
tee name=tee_preview ! queue ! \
videoconvert ! video/x-raw,format=NV16,width=1280,height=720,framerate=10/1 ! \
omxh264enc ! queue ! \
rtph264pay config-interval=10 ! fakesink \
tee_preview. ! fakesink 

Setting pipeline to PAUSED ...
Pipeline is live and does not need PREROLL ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
/GstPipeline:pipeline0/GstV4l2Src:v4l2src0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstTee:tee_preview.GstTeePad:src_0: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstQueue:queue0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstQueue:queue0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstTee:tee_preview.GstTeePad:src_1: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstFakeSink:fakesink1.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstTee:tee_preview.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstVideoConvert:videoconvert0.GstPad:src: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
/GstPipeline:pipeline0/GstCapsFilter:capsfilter1.GstPad:src: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
Redistribute latency...
/GstPipeline:pipeline0/GstOMXH264Enc-omxh264enc:omxh264enc-omxh264enc0.GstPad:sink: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
/GstPipeline:pipeline0/GstCapsFilter:capsfilter1.GstPad:sink: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
/GstPipeline:pipeline0/GstVideoConvert:videoconvert0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
!! Warning : Adapting profile to support bitdepth and chroma mode
!! The specified Level is too low and will be adjusted !!
/GstPipeline:pipeline0/GstOMXH264Enc-omxh264enc:omxh264enc-omxh264enc0.GstPad:src: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, profile=(string)high-4:2:2, level=(string)3.1, chroma-format=(string)4:2:2, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, width=(int)1280, height=(int)720, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)10/1, interlace-mode=(string)progressive, colorimetry=(string)bt709, chroma-site=(string)mpeg2
/GstPipeline:pipeline0/GstQueue:queue1.GstPad:src: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, profile=(string)high-4:2:2, level=(string)3.1, chroma-format=(string)4:2:2, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, width=(int)1280, height=(int)720, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)10/1, interlace-mode=(string)progressive, colorimetry=(string)bt709, chroma-site=(string)mpeg2
/GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0.GstPad:sink: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, profile=(string)high-4:2:2, level=(string)3.1, chroma-format=(string)4:2:2, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, width=(int)1280, height=(int)720, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)10/1, interlace-mode=(string)progressive, colorimetry=(string)bt709, chroma-site=(string)mpeg2
/GstPipeline:pipeline0/GstQueue:queue1.GstPad:sink: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, profile=(string)high-4:2:2, level=(string)3.1, chroma-format=(string)4:2:2, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, width=(int)1280, height=(int)720, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)10/1, interlace-mode=(string)progressive, colorimetry=(string)bt709, chroma-site=(string)mpeg2
/GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0.GstPad:src: caps = application/x-rtp, media=(string)video, payload=(int)96, clock-rate=(int)90000, encoding-name=(string)H264, ssrc=(uint)245907231, timestamp-offset=(uint)1633743960, seqnum-offset=(uint)3213, a-framerate=(string)10
/GstPipeline:pipeline0/GstFakeSink:fakesink0.GstPad:sink: caps = application/x-rtp, media=(string)video, payload=(int)96, clock-rate=(int)90000, encoding-name=(string)H264, ssrc=(uint)245907231, timestamp-offset=(uint)1633743960, seqnum-offset=(uint)3213, a-framerate=(string)10
/GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0.GstPad:src: caps = application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, packetization-mode=(string)1, sprop-parameter-sets=(string)"J3oAH70AzkAUAW7AUoKCgqAAAAMAIAAAAwKWAB8Au///gUAA\,KOtjywA\=", payload=(int)96, seqnum-offset=(uint)3213, timestamp-offset=(uint)1633743960, ssrc=(uint)245907231, a-framerate=(string)10
/GstPipeline:pipeline0/GstFakeSink:fakesink0.GstPad:sink: caps = application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, packetization-mode=(string)1, sprop-parameter-sets=(string)"J3oAH70AzkAUAW7AUoKCgqAAAAMAIAAAAwKWAB8Au///gUAA\,KOtjywA\=", payload=(int)96, seqnum-offset=(uint)3213, timestamp-offset=(uint)1633743960, ssrc=(uint)245907231, a-framerate=(string)10
/GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0: timestamp = 1633817232
/GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0: seqnum = 3213
^Chandling interrupt.
Interrupt: Stopping pipeline ...
Execution ended after 0:00:04.261469321
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...


########################################################################
# STAGE 8-1: Setup the Network Socket
# - use the 'udpsink' element to setup a udp socket to transfer the rtp stream
# - Let gstreamer auto-negotiate properties
# - specify the IP Address of the destination for the video
# - specify the network port used
########################################################################
root@vcu_uz7ev_cc:~# gst-inspect-1.0 udpsink

Factory Details:
  Rank                     none (0)
  Long-name                UDP packet sender
  Klass                    Sink/Network
  Description              Send data over the network via UDP
  Author                   Wim Taymans <wim@fluendo.com>

Plugin Details:
  Name                     udp
  Description              transfer data via UDP
 ...
 Pad Templates:
  SINK template: 'sink'
    Availability: Always
    Capabilities:
      ANY
...
Pads:
  SINK: 'sink'
    Pad Template: 'sink'

Element Properties:
...
  host                : The host/IP/Multicast group to send the packets to
                        flags: readable, writable
                        String. Default: "localhost"
  port                : The port to send the packets to
                        flags: readable, writable
                        Integer. Range: 0 - 65535 Default: 5004 
...

- Construct, add and test the next step in the pipeline
**Note: This pipeline will run until cancelled manually using <CTRL-C>
########################################################################
root@vcu_uz7ev_cc:~# gst-launch-1.0 -v v4l2src device=/dev/video0 ! \
video/x-raw,format=YUY2,width=1280,height=720,framerate=10/1 ! \
tee name=tee_preview ! queue ! \
videoconvert ! video/x-raw,format=NV16,width=1280,height=720,framerate=10/1 ! \
omxh264enc ! queue ! \
rtph264pay config-interval=10 ! \
udpsink host=192.168.1.103 port=5000 \
tee_preview. ! fakesink

Setting pipeline to PAUSED ...
Pipeline is live and does not need PREROLL ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
/GstPipeline:pipeline0/GstV4l2Src:v4l2src0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstTee:tee_preview.GstTeePad:src_0: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstQueue:queue0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstQueue:queue0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstTee:tee_preview.GstTeePad:src_1: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstFakeSink:fakesink0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstTee:tee_preview.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstVideoConvert:videoconvert0.GstPad:src: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
/GstPipeline:pipeline0/GstCapsFilter:capsfilter1.GstPad:src: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
Redistribute latency...
/GstPipeline:pipeline0/GstOMXH264Enc-omxh264enc:omxh264enc-omxh264enc0.GstPad:sink: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
/GstPipeline:pipeline0/GstCapsFilter:capsfilter1.GstPad:sink: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
/GstPipeline:pipeline0/GstVideoConvert:videoconvert0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
!! Warning : Adapting profile to support bitdepth and chroma mode
!! The specified Level is too low and will be adjusted !!
/GstPipeline:pipeline0/GstOMXH264Enc-omxh264enc:omxh264enc-omxh264enc0.GstPad:src: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, profile=(string)high-4:2:2, level=(string)3.1, chroma-format=(string)4:2:2, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, width=(int)1280, height=(int)720, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)10/1, interlace-mode=(string)progressive, colorimetry=(string)bt709, chroma-site=(string)mpeg2
/GstPipeline:pipeline0/GstQueue:queue1.GstPad:sink: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, profile=(string)high-4:2:2, level=(string)3.1, chroma-format=(string)4:2:2, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, width=(int)1280, height=(int)720, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)10/1, interlace-mode=(string)progressive, colorimetry=(string)bt709, chroma-site=(string)mpeg2
/GstPipeline:pipeline0/GstQueue:queue1.GstPad:sink: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, profile=(string)high-4:2:2, level=(string)3.1, chroma-format=(string)4:2:2, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, width=(int)1280, height=(int)720, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)10/1, interlace-mode=(string)progressive, colorimetry=(string)bt709, chroma-site=(string)mpeg2
/GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0.GstPad:sink: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, profile=(string)high-4:2:2, level=(string)3.1, chroma-format=(string)4:2:2, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, width=(int)1280, height=(int)720, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)10/1, interlace-mode=(string)progressive, colorimetry=(string)bt709, chroma-site=(string)mpeg2
/GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0.GstPad:src: caps = application/x-rtp, media=(string)video, payload=(int)96, clock-rate=(int)90000, encoding-name=(string)H264, ssrc=(uint)3727775354, timestamp-offset=(uint)772168228, seqnum-offset=(uint)4825, a-framerate=(string)10
/GstPipeline:pipeline0/GstUDPSink:udpsink0.GstPad:sink: caps = application/x-rtp, media=(string)video, payload=(int)96, clock-rate=(int)90000, encoding-name=(string)H264, ssrc=(uint)3727775354, timestamp-offset=(uint)772168228, seqnum-offset=(uint)4825, a-framerate=(string)10
/GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0.GstPad:src: caps = application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, packetization-mode=(string)1, sprop-parameter-sets=(string)"J3oAH70AzkAUAW7AUoKCgqAAAAMAIAAAAwKWAB8Au///gUAA\,KOtjywA\=", payload=(int)96, seqnum-offset=(uint)4825, timestamp-offset=(uint)772168228, ssrc=(uint)3727775354, a-framerate=(string)10
/GstPipeline:pipeline0/GstUDPSink:udpsink0.GstPad:sink: caps = application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, packetization-mode=(string)1, sprop-parameter-sets=(string)"J3oAH70AzkAUAW7AUoKCgqAAAAMAIAAAAwKWAB8Au///gUAA\,KOtjywA\=", payload=(int)96, seqnum-offset=(uint)4825, timestamp-offset=(uint)772168228, ssrc=(uint)3727775354, a-framerate=(string)10
/GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0: timestamp = 772229476
/GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0: seqnum = 4825
^Chandling interrupt.
Interrupt: Stopping pipeline ...
Execution ended after 0:00:05.249397422
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...


########################################################################
# STAGE 3-2: Buffer data after the tee (on the second pipeline path)
# - buffer data using the 'queue' element (after encoding)
#######################################################################
root@vcu_uz7ev_cc:~# gst-inspect-1.0 queue

Factory Details:
  Rank                     none (0)
  Long-name                Queue
  Klass                    Generic
  Description              Simple data queue
  Author                   Erik Walthinsen <omega@cse.ogi.edu>

Plugin Details:
  Name                     coreelements
  Description              GStreamer core elements
...
Pads:
  SINK: 'sink'
    Pad Template: 'sink'
  SRC: 'src'
    Pad Template: 'src'
...

- Construct, add and test the next step in the pipeline
**Note: This pipeline will run until cancelled manually using <CTRL-C>
########################################################################
root@vcu_uz7ev_cc:~# gst-launch-1.0 -v v4l2src device=/dev/video0 ! \
video/x-raw,format=YUY2,width=1280,height=720,framerate=10/1 ! \
tee name=tee_preview ! queue ! \
videoconvert ! video/x-raw,format=NV16,width=1280,height=720,framerate=10/1 ! \
omxh264enc ! queue ! \
rtph264pay config-interval=10 ! \
udpsink host=192.168.1.103 port=5000 \
tee_preview. ! queue ! fakesink

Setting pipeline to PAUSED ...
Pipeline is live and does not need PREROLL ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
/GstPipeline:pipeline0/GstV4l2Src:v4l2src0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstTee:tee_preview.GstTeePad:src_0: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstQueue:queue0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstQueue:queue0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstTee:tee_preview.GstTeePad:src_1: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstQueue:queue2.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstFakeSink:fakesink0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstQueue:queue2.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstTee:tee_preview.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstVideoConvert:videoconvert0.GstPad:src: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
/GstPipeline:pipeline0/GstCapsFilter:capsfilter1.GstPad:src: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
Redistribute latency...
/GstPipeline:pipeline0/GstOMXH264Enc-omxh264enc:omxh264enc-omxh264enc0.GstPad:sink: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
/GstPipeline:pipeline0/GstCapsFilter:capsfilter1.GstPad:sink: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
/GstPipeline:pipeline0/GstVideoConvert:videoconvert0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
!! Warning : Adapting profile to support bitdepth and chroma mode
!! The specified Level is too low and will be adjusted !!
/GstPipeline:pipeline0/GstOMXH264Enc-omxh264enc:omxh264enc-omxh264enc0.GstPad:src: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, profile=(string)high-4:2:2, level=(string)3.1, chroma-format=(string)4:2:2, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, width=(int)1280, height=(int)720, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)10/1, interlace-mode=(string)progressive, colorimetry=(string)bt709, chroma-site=(string)mpeg2
/GstPipeline:pipeline0/GstQueue:queue1.GstPad:sink: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, profile=(string)high-4:2:2, level=(string)3.1, chroma-format=(string)4:2:2, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, width=(int)1280, height=(int)720, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)10/1, interlace-mode=(string)progressive, colorimetry=(string)bt709, chroma-site=(string)mpeg2
/GstPipeline:pipeline0/GstQueue:queue1.GstPad:sink: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, profile=(string)high-4:2:2, level=(string)3.1, chroma-format=(string)4:2:2, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, width=(int)1280, height=(int)720, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)10/1, interlace-mode=(string)progressive, colorimetry=(string)bt709, chroma-site=(string)mpeg2
/GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0.GstPad:sink: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, profile=(string)high-4:2:2, level=(string)3.1, chroma-format=(string)4:2:2, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, width=(int)1280, height=(int)720, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)10/1, interlace-mode=(string)progressive, colorimetry=(string)bt709, chroma-site=(string)mpeg2
/GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0.GstPad:src: caps = application/x-rtp, media=(string)video, payload=(int)96, clock-rate=(int)90000, encoding-name=(string)H264, ssrc=(uint)1531453067, timestamp-offset=(uint)1375765087, seqnum-offset=(uint)24271, a-framerate=(string)10
/GstPipeline:pipeline0/GstUDPSink:udpsink0.GstPad:sink: caps = application/x-rtp, media=(string)video, payload=(int)96, clock-rate=(int)90000, encoding-name=(string)H264, ssrc=(uint)1531453067, timestamp-offset=(uint)1375765087, seqnum-offset=(uint)24271, a-framerate=(string)10
/GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0.GstPad:src: caps = application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, packetization-mode=(string)1, sprop-parameter-sets=(string)"J3oAH70AzkAUAW7AUoKCgqAAAAMAIAAAAwKWAB8Au///gUAA\,KOtjywA\=", payload=(int)96, seqnum-offset=(uint)24271, timestamp-offset=(uint)1375765087, ssrc=(uint)1531453067, a-framerate=(string)10
/GstPipeline:pipeline0/GstUDPSink:udpsink0.GstPad:sink: caps = application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, packetization-mode=(string)1, sprop-parameter-sets=(string)"J3oAH70AzkAUAW7AUoKCgqAAAAMAIAAAAwKWAB8Au///gUAA\,KOtjywA\=", payload=(int)96, seqnum-offset=(uint)24271, timestamp-offset=(uint)1375765087, ssrc=(uint)1531453067, a-framerate=(string)10
/GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0: timestamp = 1375833141
/GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0: seqnum = 24271
^Chandling interrupt.
Interrupt: Stopping pipeline ...
Execution ended after 0:00:05.947516405
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...

########################################################################
# STAGE 4-2: Send video out to the KMSSINK (Display Output)
# - the input camera data format is YUYV (YUY2) which is 4:2:2
# - use the 'kmssink' element to send decoded video to the display
# - Use gstreamer element properties to allow fullscreen-overlay
########################################################################
root@vcu_uz7ev_cc:~# gst-inspect-1.0 kmssink

Factory Details:
  Rank                     secondary (128)
  Long-name                KMS video sink
  Klass                    Sink/Video
  Description              Video sink using the Linux kernel mode setting API
  Author                   V�íctor J�áquez <vjaquez@igalia.com>

Plugin Details:
  Name                     kms
  Description              Video sink using the Linux kernel mode setting API
...
Pad Templates:
  SINK template: 'sink'
    Availability: Always
    Capabilities:
      video/x-raw
                 format: { (string)BGRA, (string)BGRx, (string)RGBA, (string)RGBx, (string)RGB, (string)BGR, (string)UYVY, (string)YUY2, (string)YVYU, (string)I420, (string)YV12, (string)Y42B, (str}
                  width: [ 1, 2147483647 ]
                 height: [ 1, 2147483647 ]
              framerate: [ 0/1, 2147483647/1 ]
...
Pads:
  SINK: 'sink'
    Pad Template: 'sink'

Element Properties:
...
  bus-id              : DRM bus ID
                        flags: readable, writable
                        String. Default: null
  connector-id        : DRM connector id
                        flags: readable, writable
                        Integer. Range: -1 - 2147483647 Default: -1 
  plane-id            : DRM plane id
                        flags: readable, writable
                        Integer. Range: -1 - 2147483647 Default: -1 
...
  fullscreen-overlay  : When enabled, the sink sets CRTC size same as input video size
                        flags: readable, writable
                        Boolean. Default: false
...

- Construct, add and test the next step in the pipeline
**Note: The command will need to be canceled using <CTRL-C>
########################################################################
root@vcu_uz7ev_cc:~# gst-launch-1.0 -v v4l2src device=/dev/video0 ! \
video/x-raw,format=YUY2,width=1280,height=720,framerate=10/1 ! \
tee name=tee_preview ! queue ! \
videoconvert ! video/x-raw,format=NV16,width=1280,height=720,framerate=10/1 ! \
omxh264enc ! queue ! \
rtph264pay config-interval=10 ! \
udpsink host=192.168.1.103 port=5000 \
tee_preview. ! queue ! \
kmssink bus-id="fd4a0000.zynqmp-display" fullscreen-overlay=true

Setting pipeline to PAUSED ...
Pipeline is live and does not need PREROLL ...
/GstPipeline:pipeline0/GstKMSSink:kmssink0: display-width = 3440
/GstPipeline:pipeline0/GstKMSSink:kmssink0: display-height = 1440
Setting pipeline to PLAYING ...
New clock: GstSystemClock
/GstPipeline:pipeline0/GstV4l2Src:v4l2src0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstCapsFilter:capsfilter0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
[ 5291.248007] PLL: shutdown
[ 5291.253789] PLL: enable
/GstPipeline:pipeline0/GstTee:tee_preview.GstTeePad:src_0: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstQueue:queue0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstTee:tee_preview.GstTeePad:src_1: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstQueue:queue0.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstQueue:queue2.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstQueue:queue2.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstQueue:queue2.GstPad:src: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstVideoConvert:videoconvert0.GstPad:src: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
/GstPipeline:pipeline0/GstCapsFilter:capsfilter1.GstPad:src: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
Redistribute latency...
/GstPipeline:pipeline0/GstOMXH264Enc-omxh264enc:omxh264enc-omxh264enc0.GstPad:sink: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
/GstPipeline:pipeline0/GstCapsFilter:capsfilter1.GstPad:sink: caps = video/x-raw, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, format=(string)NV16
/GstPipeline:pipeline0/GstVideoConvert:videoconvert0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
/GstPipeline:pipeline0/GstKMSSink:kmssink0.GstPad:sink: caps = video/x-raw, format=(string)YUY2, width=(int)1280, height=(int)720, framerate=(fraction)10/1, pixel-aspect-ratio=(fraction)1/1, interlace-mode=(string)progressive, colorimetry=(string)2:4:7:1
!! Warning : Adapting profile to support bitdepth and chroma mode
!! The specified Level is too low and will be adjusted !!
/GstPipeline:pipeline0/GstOMXH264Enc-omxh264enc:omxh264enc-omxh264enc0.GstPad:src: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, profile=(string)high-4:2:2, level=(string)3.1, chroma-format=(string)4:2:2, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, width=(int)1280, height=(int)720, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)10/1, interlace-mode=(string)progressive, colorimetry=(string)bt709, chroma-site=(string)mpeg2
/GstPipeline:pipeline0/GstQueue:queue1.GstPad:sink: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, profile=(string)high-4:2:2, level=(string)3.1, chroma-format=(string)4:2:2, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, width=(int)1280, height=(int)720, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)10/1, interlace-mode=(string)progressive, colorimetry=(string)bt709, chroma-site=(string)mpeg2
/GstPipeline:pipeline0/GstQueue:queue1.GstPad:sink: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, profile=(string)high-4:2:2, level=(string)3.1, chroma-format=(string)4:2:2, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, width=(int)1280, height=(int)720, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)10/1, interlace-mode=(string)progressive, colorimetry=(string)bt709, chroma-site=(string)mpeg2
/GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0.GstPad:sink: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, profile=(string)high-4:2:2, level=(string)3.1, chroma-format=(string)4:2:2, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, width=(int)1280, height=(int)720, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)10/1, interlace-mode=(string)progressive, colorimetry=(string)bt709, chroma-site=(string)mpeg2
/GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0.GstPad:src: caps = application/x-rtp, media=(string)video, payload=(int)96, clock-rate=(int)90000, encoding-name=(string)H264, ssrc=(uint)1340308335, timestamp-offset=(uint)687074929, seqnum-offset=(uint)13719, a-framerate=(string)10
/GstPipeline:pipeline0/GstUDPSink:udpsink0.GstPad:sink: caps = application/x-rtp, media=(string)video, payload=(int)96, clock-rate=(int)90000, encoding-name=(string)H264, ssrc=(uint)1256232410, timestamp-offset=(uint)1623613073, seqnum-offset=(uint)21243, a-framerate=(string)10
/GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0.GstPad:src: caps = application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, packetization-mode=(string)1, sprop-parameter-sets=(string)"J3oAH70AzkAUAW7AUoKCgqAAAAMAIAAAAwKWAB8Au///gUAA\,KOtjywA\=", payload=(int)96, seqnum-offset=(uint)13719, timestamp-offset=(uint)687074929, ssrc=(uint)1340308335, a-framerate=(string)10
/GstPipeline:pipeline0/GstUDPSink:udpsink0.GstPad:sink: caps = application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, packetization-mode=(string)1, sprop-parameter-sets=(string)"J3oAH70AzkAUAW7AUoKCgqAAAAMAIAAAAwKWAB8Au///gUAA\,KOtjywA\=", payload=(int)96, seqnum-offset=(uint)21243, timestamp-offset=(uint)1623613073, ssrc=(uint)1256232410, a-framerate=(string)10
/GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0: timestamp = 1623699310
/GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0: seqnum = 21243
^Chandling interrupt.
Interrupt: Stopping pipeline ...
Execution ended after 0:00:54.985678282
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
[ 5346.382408] PLL: shutdown
[ 5346.409141] PLL: enable
Freeing pipeline ...


########################################################################
# Optimized encoder settings per Xilinx TRD Wiki Example
########################################################################
root@vcu_uz7ev_cc:~# gst-launch-1.0 -v v4l2src device=/dev/video0 ! \
video/x-raw,format=YUY2,width=1280,height=720,framerate=10/1 ! \
tee name=tee_preview ! queue ! \
videoconvert ! video/x-raw,format=NV16,width=1280,height=720,framerate=10/1 ! \
omxh264enc qp-mode=auto gop-mode=basic gop-length=60 b-frames=0 target-bitrate=20000 num-slices=8 control-rate=constant prefetch-buffer=true low-bandwidth=false filler-data=true latency-mode=normal ! queue ! \
rtph264pay config-interval=10 ! \
udpsink host=192.168.1.103 port=5000 \
tee_preview. ! queue ! \
kmssink bus-id="fd4a0000.zynqmp-display" fullscreen-overlay=true

########################################################################
########################################################################
########################################################################
# Review the Session Description Protocol (SDP) format:
# - RFC4566 [https://tools.ietf.org/html/rfc4566.pdf]
# - Review Section 5 for format information
########################################################################
Session description
 v=  (protocol version)
 o=  (originator and session identifier)
 s=  (session name)
 i=* (session information)
 u=* (URI of description)
 e=* (email address)
 p=* (phone number)
 c=* (connection information -- not required if included in
      all media)
 b=* (zero or more bandwidth information lines)
 One or more time descriptions ("t=" and "r=" lines; see below)
 z=* (time zone adjustments)
 k=* (encryption key)
 a=* (zero or more session attribute lines)
 Zero or more media descriptions

Time description
 t=  (time the session is active)
 r=* (zero or more repeat times)

Media description, if present
 m=  (media name and transport address)
 i=* (media title)
 c=* (connection information -- optional if included at
      session level)
 b=* (zero or more bandwidth information lines)
 k=* (encryption key)
 a=* (zero or more media attribute lines)

########################################################################
# CREATE an SDP FILE for VLC Playback over the network
# Create an SDP file to receive thsi video stream in VLC
# - use the video stage information for the RTP stream from GStreamer to construct the SDP file
########################################################################
...
/GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0.GstPad:sink: caps = video/x-h264, stream-format=(string)byte-stream, alignment=(string)au, profile=(string)high-4:2:2, level=(string)3.1, chroma-format=(string)4:2:2, bit-depth-luma=(uint)8, bit-depth-chroma=(uint)8, width=(int)1280, height=(int)720, pixel-aspect-ratio=(fraction)1/1, framerate=(fraction)10/1, interlace-mode=(string)progressive, colorimetry=(string)bt709, chroma-site=(string)mpeg2
...
/GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0.GstPad:src: caps = application/x-rtp, media=(string)video, payload=(int)96, clock-rate=(int)90000, encoding-name=(string)H264, ssrc=(uint)1340308335, timestamp-offset=(uint)687074929, seqnum-offset=(uint)13719, a-framerate=(string)10
...
/GstPipeline:pipeline0/GstRtpH264Pay:rtph264pay0.GstPad:src: caps = application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, packetization-mode=(string)1, sprop-parameter-sets=(string)"J3oAH70AzkAUAW7AUoKCgqAAAAMAIAAAAwKWAB8Au///gUAA\,KOtjywA\=", payload=(int)96, seqnum-offset=(uint)13719, timestamp-offset=(uint)687074929, ssrc=(uint)1340308335, a-framerate=(string)10


########################################################################
- Set the Version
  - Syntax: v=0
########################################################################
v=0

########################################################################
- Set the Origin
  - Syntax: o=<username> <sess-id> <sess-version> <nettype> <addrtype> <unicast-address>
########################################################################
o=- 3762381622 3353214112 IN IP4 192.168.1.109

- <username>
  '-' for no login supported/required

- <sess-id>, <sess-version>
  Get an NTP timestamp to be used as session id and version (must be unique)
  $ ntptime
  ntp_gettime() returns code 0 (OK)
    time e0416336.c7ddfca0  Sat, Mar 23 2019 22:00:22.780, (.780731217),
    maximum error 75156 us, estimated error 3810 us, TAI offset 37
  ntp_adjtime() returns code 0 (OK)
    modes 0x0 (),
    offset 835.856 us, frequency 30.972 ppm, interval 1 s,
    maximum error 75156 us, estimated error 3810 us,
    status 0x2001 (PLL,NANO),
    time constant 6, precision 0.001 us, tolerance 500 ppm,

  Convert 'time e0416336.c7ddfca0' from hex to decimal format
    - e0416336 = 3762381622
    - c7ddfca0 = 3353214112

- <nettype>
  IN = Internet

- <addrtype>
  IP4 = IPv4 Address Format

- <unicast-address>
  Get IP Address on Streaming Host
  root@zcu106_vcu_trd:~# ifconfig
  eth0      Link encap:Ethernet  HWaddr 00:0A:35:00:22:01  
            inet addr:192.168.1.109  Bcast:192.168.1.255  Mask:255.255.255.0
            inet6 addr: fe80::20a:35ff:fe00:2201%4882584/64 Scope:Link
            inet6 addr: 2600:1700:34b0:6f20:20a:35ff:fe00:2201%4882584/64 Scope:Global
            UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
            RX packets:562374 errors:0 dropped:85971 overruns:0 frame:0
            TX packets:27749 errors:0 dropped:0 overruns:0 carrier:0
            collisions:0 txqueuelen:1000 
            RX bytes:31532183 (30.0 MiB)  TX bytes:10296412 (9.8 MiB)
            Interrupt:32 

  lo        Link encap:Local Loopback  
            inet addr:127.0.0.1  Mask:255.0.0.0
            inet6 addr: ::1%4882584/128 Scope:Host
            UP LOOPBACK RUNNING  MTU:65536  Metric:1
            RX packets:0 errors:0 dropped:0 overruns:0 frame:0
            TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
            collisions:0 txqueuelen:1000 
            RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)

########################################################################
- Set the Session Name
  - Syntax: s=Session Name
- Hint: Create a descriptive string to name the session
########################################################################
s=USB Webcam OMXH264ENC RTP Stream 720p10

########################################################################
- Set the Connection Information
  - Syntax: c=<nettype> <addrtype> <connection-address>
########################################################################
c=IN IP4 192.168.1.109

- <nettype>
  IN = Internet

- <addrtype>
  IP4 = IPv4 Address Format

########################################################################
- Set the Connection Information
  - Syntax: c=<nettype> <addrtype> <connection-address>
########################################################################
c=IN IP4 192.168.1.109

- <nettype>
  IN = Internet

- <addrtype>
  IP4 = IPv4 Address Format

########################################################################
- Set Media Description
  - Syntax: m=<media> <port> <proto> <fmt> ...
########################################################################
From the GSTREAMER Commnad Line Verbose Output
...
/GstPipeline:pipeline0/GstUDPSink:udpsink0.GstPad:sink: caps = application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, packetization-mode=(string)1, sprop-
parameter-sets=(string)"J3oAH70AzkAUAW7AUoKCgqAAAAMAIAAAAwKWAB8Au///gUAA\,KOtjywA\=", payload=(int)96, seqnum-offset=(uint)24267, timestamp-offset=(uint)3550796446, ssrc=(uint)330783705, a-framerate=(string)10
...
########################################################################
m=video 5000 RTP/AVP 96

- <media> = video
  - Gstreamer Log: 'media=(string)video'
- <port> = 5000
- <proto> = RTP/AVP
  - Gstreamer Log: 'application/x-rtp'
- <fmt> = 96
  - Gstreamer Log: 'payload=(int)96'

########################################################################
- Set Media Attributes
  - Syntax: a=<attribute> or a=<attribute>:<value>
- RTP requires an 'rtpmap' attribute
  - Syntax: a=rtpmap:<payload type> <encoding name>/<clock rate> [/<encoding
         parameters>]
- Payload Format 96 requires additional attributes
  - Generic Syntax:  
    - a=fmtp:<format> <format specific parameters>
  - Specific Syntax: 
    - a=fmtp:<payload> media=<media>; clock-rate=<clock-rate>; encoding-name=<encoding-name>; packetization-mode=<packetization-mode>; sprop-parameter-sets=<sprop-parameter-sets>
    - This is derived from Gstreamer Command Output
########################################################################
a=rtpmap:96 H264/90000
a=fmpt:96 media=video; clock-rate=90000; encoding-name=H264; packetization-mode=1; sprop-parameter-sets=J3oAH70AzkAUAW7AUoKCgqAAAAMAIAAAAwKWAB8Au///gUAA\,KOtjywA\=

- <payload type> = 96
  - Gstreamer Log: 'payload=(int)96'
- <encoding name> = H264
  - GStreamer Log: 'encoding-name=(string)H264'
- <clock rate> = 90000
  - Gstreamer Log: 'clock-rate=(int)90000'

- <sprop-parameter-sets> = "J3oAH70AzkAUAW7AUoKCgqAAAAMAIAAAAwKWAB8Au///gUAA\,KOtjywA\="
  - Gstreamer Log: 'sprop-parameter-sets=(string)"J3oAH70AzkAUAW7AUoKCgqAAAAMAIAAAAwKWAB8Au///gUAA\,KOtjywA\="'

########################################################################
FINAL EXAMPLE SDP FILE
- Put the contents below in a file with an '.SDP' extension
- Play this file in VLC media player on the destination machine
########################################################################
v=0
o=- 3762381622 3353214112 IN IP4 192.168.1.109
s=USB Webcam OMXH264ENC RTP Stream 720p10
c=IN IP4 192.168.1.109
m=video 5000 RTP/AVP 96
a=rtpmap:96 H264/90000
a=fmpt:96 media=video; clock-rate=90000; encoding-name=H264; packetization-mode=1; sprop-parameter-sets=J3oAH70AzkAUAW7AUoKCgqAAAAMAIAAAAwKWAB8Au///gUAA\,KOtjywA\=; timestamp-offset=3550796446; ssrc=330783705; a-framerate=10

