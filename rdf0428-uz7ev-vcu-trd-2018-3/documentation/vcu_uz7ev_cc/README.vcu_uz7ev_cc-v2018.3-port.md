[//]: # (Readme.vcu_uz7ev_cc-v2018.3-port.md)
 
# Pre-requisites
- Example commands below assume working from a user's home folder

## Download the v2018.3 VCU TRD Bundle from Xilinx
v2018.3 VCU TRD Bundle:
- https://www.xilinx.com/member/forms/download/design-license-xef.html?filename=rdf0428-zcu106-vcu-trd-2018-3.zip

- place in the __*downloads*__ folder

## Download the UltraZed-EV BSP Files for the Carrier Card (we will use to replicate the PS configuration)
Top Level Design Documents: 
- http://zedboard.org/support/design/22581/166

v2018.2 Carrier Card Reference Design: 
- http://zedboard.org/sites/default/files/design/uz7ev_evcc_2018_2.zip

- place in the __*downloads*__ folder

## Download and Install the Avnet Board Definitions from GitHub
Avnet Board Definition Repository Instructions:
- http://zedboard.org/sites/default/files/documentations/Installing-Board-Definition-Files_v1_0.pdf

Avnet Github Repository:
- https://github.com/Avnet/bdf.git

```bash

$ pushd downloads/
$ git clone https://github.com/Avnet/bdf.git Avnet/bdf
$ popd
```

### Copy the UZ7EV_CC board definition to the Vivado Board Files Directory
```bash

$ cp -R downloads/Avnet/bdf/ultrazed_7ev* /opt/Xilinx/Vivado/2018.3/data/boards/board_files/
```

# Launch the UZ_PETALINUX reference design for the UltraZed-EV

## Extract the UltraZed-EV Release Archive to access the reference __*uz_petalinux*__ Vivado Project
- Extract the project from the BSP archive to the temporary working folder
```bash
:~/avnet-uzev-ports$ cd temp
:~/avnet-uzev-ports/temp$ unzip ../downloads/uz7ev_evcc_2018_2.zip -d uz7ev_evcc_2018_2
:~/avnet-uzev-ports/temp/uz7ev_evcc_2018_2$ cd uz7ev_evcc_2018_2
:~/avnet-uzev-ports/temp/uz7ev_evcc_2018_2$ tar -zxvf uz7ev_evcc_2018_2.bsp
```

## Save a TCL script based on the v2018.2 design in the BSP
- When you first go to open the project, you will be prompted by Vivado to either:
	- Automatically upgrade to the current version
	- Open the project in read-only mode

- Open the project in Vivado
	- Location: __*/avnet-uzev-ports/temp/uz7ev_evcc_2018_2/uz7ev_evcc_2018_2/hardware/UZ7EV_ECC/uz_petalinux.xpr*__

- For the first pass, select the second option
	- __*Open the project in read-only mode*__

## Export the __*uz_petalinux*__ example design module to a TCL script for reference
- Open the Block Design
	- Flow -> Open Block Design -> uz_petalinux.bd

- Open a TCL console
	- Window -> Tcl Console

- Export the TCL script
	__*write_bd_tcl –no_ip_version –exclude_layout ~/avnet-uzev-ports/_artifacts/rdf0428-uz7ev-vcu-trd-2018-3/uz_petalinux_v2018.2.reference.tcl*__

- Close the Vivado Project

## Upgrade the __*uz_petalinux*__ example design
- Since this is a v2018.2 release, it needs to be updated to match the IP parameters for the v2018.3 release
- When you re-open the project, you will be prompted by Vivado to either:
	- Automatically upgrade to the current version
	- Open the project in read-only mode

- Open the project in Vivado
	- Location: __*/avnet-uzev-ports/temp/uz7ev_evcc_2018_2/uz7ev_evcc_2018_2/hardware/UZ7EV_ECC/uz_petalinux.xpr*__

- For the first pass, select the second option
	- __*Automatically upgrade to the current version*__

- Vivado will ask if you want to Report IP Status
	- Select __*Report IP Status*__
	- See: ![Vivado Report IP Status Results]("_artifacts/rdf0428-uz7ev-vcu-trd-2018-3/vivado_uz_petalinux_upgrade-Report-IP-Status.png")

- In the __*Report IP Status*__ window
	- Select all of the IP blocks that need upgraded
	- Click the __*Upgrade Selected*__ button

## Export the upgraded __*uz_petalinux*__ example design module to a TCL script for reference
- Open the Block Design
	- Flow -> Open Block Design -> uz_petalinux.bd

- Open a TCL console
	- Window -> Tcl Console

- Export the TCL script
	__*write_bd_tcl –no_ip_version –exclude_layout ~/avnet-uzev-ports/_artifacts/rdf0428-uz7ev-vcu-trd-2018-3/uz_petalinux_v2018.3.reference.tcl*__

## Generate the modified block design with only the upgraded UltraZed-EV base design.
- Complete Synthesis, Implementation and Generate a new Bitstream using
- We will test this at a later stage

## Export the design so it can be used in Petalinux to build a set of working software images.
- File -> Export -> Hardware
- Check the __*Include Bitstream*__ option, Leave __*Export To*__ set to __*<Local to Project>*__

- Close the Vivado Project

## Exit Vivado

# Launch the ZCU106 reference design module for __*vcu_hdmirx*__

## Set the TRD_HOME environment variable
```bash
:~/avnet-uzev-ports$ cd temp/rdf0428-zcu106-vcu-trd-2018-3
:~/avnet-uzev-ports/temp/rdf0428-zcu106-vcu-trd-2018-3$ export TRD_HOME=$(pwd)
:~/avnet-uzev-ports/temp/rdf0428-zcu106-vcu-trd-2018-3$ cd pl
:~/avnet-uzev-ports/temp/rdf0428-zcu106-vcu-trd-2018-3/pl$ vivado -source scripts/vcu_hdmirx_proj.tcl

****** Vivado v2018.3 (64-bit)
  **** SW Build 2405991 on Thu Dec  6 23:36:41 MST 2018
  **** IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
    ** Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.

start_gui
```

## Export the ZCU106 example design module to a TCL script for reference
- Open the Block Design
	- Flow -> Open Block Design -> vcu_hdmirx.bd

- Open a TCL console
	- Window -> Tcl Console

- Export the TCL script
	__*write_bd_tcl –no_ip_version –exclude_layout ~/avnet-uzev-ports/_artifacts/rdf0428-uz7ev-vcu-trd-2018-3/vcu_hdmirx.reference.tcl*__
	
## Create an archive of the project

- Create the archive
	- File -> Project -> Archiev...

- Settings:
	- check all boxes, leave the rest default


# Simplify the ZCU106 reference design module for __*vcu_hdmirx*__
- Strip out the HDMI logic, leaving only the VCU.

## Remove the HDMI-RX components from the block design, leaving only the VCU instantiation.
- Open the Block Design
	- Flow -> Open Block Design -> vcu_hdmirx.bd

- Remove the following blocks and interface pins
```
	(Blocks)
	- hdmi_input
	- vid_phy_controller
	- gt_refclk_buf
	- rx_hdmi_hb_0
	- tx_hdmi_hb_0
	- interrupts1
	- axi_interconnect_4 (for the vid_phy_controller)

	(Interface Pins)
	- HDMI_CTRL_IIC
	- RX_DET
	- SI5319_LOL
	- TX_REFCLK_P
	- TX_REFCLK_N
	- HDMI_RX_CLK_P
	- HDMI_RX_CLK_N
	- DRU_CLK
	- HDMI_RX_DAT_N[2:0]
	- HDMI_RX_DAT_P[2:0]
	- LED7
	- RX_HPD[0:0]
	- RX_DDC
	- LED6
	- HDMI_TX_CLK_P
	- HDMI_TX_CLK_N
	- RX_REFCLK_P
	- RX_REFCLK_N
	- HDMI_TX_DAT_N[2:0]
	- HDMI_TX_DAT_P[2:0]
	- TX_EN
```

- Drop down into the mpsoc_ss block and remove the following blocks and interface pins
```
	(Blocks)
	- hdmi_ctrl_iic
	(Interface Pins)
	- IIC
	- iic2intr_irpt
```

- Open the Design Constraints File
	- Window -> Sources
	- Expand Constraints, constrs_1 and open the vcu_hdmirx.xdc file

- Remove all constraints except for the differential input clock, the LED port and the reset pin
```
#####
## Constraints for ZCU106
## Version 1.0
#####


#####
## Pins
#####
set_property PACKAGE_PIN AH12 [get_ports {si570_user_clk_p}]
set_property IOSTANDARD LVDS [get_ports {si570_user_clk_p}]

...

set_property IOSTANDARD LVCMOS12 [get_ports LED*]

set_property PACKAGE_PIN H8 [get_ports {SI5319_RST[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {SI5319_RST[0]}]
```


## Test the modified block design with only the VCU instantiation.
- Complete Synthesis, Implementation and Generate a new Bitstream

## Export the design so it can be used in Petalinux to build a set of working software images.
- File -> Export -> Hardware
- Check the __*Include Bitstream*__ option, Leave __*Export To*__ set to __*<Local to Project>*__

## Export the ZCU106 example design without HDMI to a TCL script for reference
- Open the Block Design
	- Flow -> Open Block Design -> vcu_hdmirx.bd

- Open a TCL console
	- Window -> Tcl Console

- Export the TCL script
	__*write_bd_tcl –no_ip_version –exclude_layout ~/avnet-uzev-ports/_artifacts/rdf0428-uz7ev-vcu-trd-2018-3/vcu_hdmirx_nohdmi.reference.tcl*__

## Exit Vivado

# Build Linux targeteted to the modified design module for __*vcu_hdmirx_nohdmi*__
- Reference https://www.engineerin.avnet.com/docs/DOC-1667.pdf 
- Strip out the HDMI logic and device tree entries, leaving only the VCU subsystem.

## Create a targeted Petalinux Build for the new Vivado Design

### Create a new Petalinux project using the VCU TRD BSP
```bash
:~/avnet-uzev-ports/temp$ petalinux-create -t project -n vcu_hdmirx_nohdmi -s ~/avnet-uzev-ports/temp/rdf0428-zcu106-vcu-trd-2018-3/apu/vcu_petalinux_bsp/xilinx-vcu-trd-zcu106-v2018.3-final.bsp 
INFO: Create project: vcu_hdmirx_nohdmi
INFO: New project successfully created in ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi
```

### Use the modified Vivado design to configure the Petalinux project
```bash
:~/avnet-uzev-ports/temp$ cd vcu_hdmirx_nohdmi
:~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi$ petalinux-config --get-hw-description=~/avnet-uzev-ports/temp/rdf0428-zcu106-vcu-trd-2018-3/pl/vcu_hdmirx.sdk --oldconfig
INFO: Getting hardware description...
INFO: Rename vcu_hdmirx_wrapper.hdf to system.hdf
[INFO] generating Kconfig for project
[INFO] oldconfig project
[INFO] sourcing bitbake
[INFO] generating plnxtool conf
[INFO] generating meta-plnx-generated layer
[INFO] generating machine configuration
[INFO] generating bbappends for project . This may take time ! 
[INFO] generating u-boot configuration files
[INFO] generating kernel configuration files
[INFO] generating kconfig for Rootfs
[INFO] oldconfig rootfs
[INFO] generating petalinux-user-image.bb
```

### Execute the build, which is expected to fail due to extra device tree entries
```bash
:~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi$ petalinux-build
[INFO] building project
[INFO] sourcing bitbake
INFO: bitbake petalinux-user-image
Parsing recipes: 100% |#################################################################################################################################################################| Time: 0:01:09
Parsing of 2572 .bb files complete (0 cached, 2572 parsed). 3464 targets, 137 skipped, 0 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |##############################################################################################################################################################| Time: 0:00:28
Checking sstate mirror object availability: 100% |######################################################################################################################################| Time: 0:00:08
NOTE: Executing SetScene Tasks
NOTE: Executing RunQueue Tasks
ERROR: device-tree-xilinx+gitAUTOINC+b7466bbeee-r0 do_compile: Function failed: do_compile (log file is located at ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work/plnx_zynqmp-xilinx-linux/device-tree/xilinx+gitAUTOINC+b7466bbeee-r0/temp/log.do_compile.3126)
ERROR: Logfile of failure stored in: ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work/plnx_zynqmp-xilinx-linux/device-tree/xilinx+gitAUTOINC+b7466bbeee-r0/temp/log.do_compile.3126
Log data follows:
| DEBUG: Executing shell function do_compile
| Error: ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work/plnx_zynqmp-xilinx-linux/device-tree/xilinx+gitAUTOINC+b7466bbeee-r0/li-imx274mipi-fmc.dtsi:12.1-14 Label or path sensor_iic_0 not found
| Error: ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work/plnx_zynqmp-xilinx-linux/device-tree/xilinx+gitAUTOINC+b7466bbeee-r0/hdmi-misc.dtsi:11.1-24 Label or path mpsoc_ss_hdmi_ctrl_iic not found
| Error: ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work/plnx_zynqmp-xilinx-linux/device-tree/xilinx+gitAUTOINC+b7466bbeee-r0/design-fixes.dtsi:44.1-20 Label or path vid_phy_controller not found
| Error: ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work/plnx_zynqmp-xilinx-linux/device-tree/xilinx+gitAUTOINC+b7466bbeee-r0/design-fixes.dtsi:57.1-24 Label or path mpsoc_ss_hdmi_ctrl_iic not found
| Error: ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work/plnx_zynqmp-xilinx-linux/device-tree/xilinx+gitAUTOINC+b7466bbeee-r0/design-fixes.dtsi:63.1-19 Label or path tpg_input_v_tpg_1 not found
| Error: ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work/plnx_zynqmp-xilinx-linux/device-tree/xilinx+gitAUTOINC+b7466bbeee-r0/design-fixes.dtsi:66.1-11 Label or path tpg_port0 not found
| Error: ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work/plnx_zynqmp-xilinx-linux/device-tree/xilinx+gitAUTOINC+b7466bbeee-r0/design-fixes.dtsi:72.1-27 Label or path hdmi_input_v_hdmi_rx_ss_0 not found
| Error: ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work/plnx_zynqmp-xilinx-linux/device-tree/xilinx+gitAUTOINC+b7466bbeee-r0/design-fixes.dtsi:77.1-24 Label or path hdmi_input_v_proc_ss_0 not found
| Error: ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work/plnx_zynqmp-xilinx-linux/device-tree/xilinx+gitAUTOINC+b7466bbeee-r0/design-fixes.dtsi:85.1-12 Label or path axi_intc_0 not found
| Error: ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work/plnx_zynqmp-xilinx-linux/device-tree/xilinx+gitAUTOINC+b7466bbeee-r0/design-fixes.dtsi:92.1-28 Label or path hdmi_output_v_hdmi_tx_ss_0 not found
| Error: ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work/plnx_zynqmp-xilinx-linux/device-tree/xilinx+gitAUTOINC+b7466bbeee-r0/design-fixes.dtsi:97.1-21 Label or path hdmi_output_v_mix_0 not found
| Error: ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work/plnx_zynqmp-xilinx-linux/device-tree/xilinx+gitAUTOINC+b7466bbeee-r0/design-fixes.dtsi:114.1-15 Label or path xx_mix_master not found
| Error: ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work/plnx_zynqmp-xilinx-linux/device-tree/xilinx+gitAUTOINC+b7466bbeee-r0/design-fixes.dtsi:123.1-14 Label or path sensor_iic_0 not found
| Error: ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work/plnx_zynqmp-xilinx-linux/device-tree/xilinx+gitAUTOINC+b7466bbeee-r0/design-fixes.dtsi:126.1-31 Label or path mipi_csi2_rx_v_proc_ss_scaler not found
| Error: ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work/plnx_zynqmp-xilinx-linux/device-tree/xilinx+gitAUTOINC+b7466bbeee-r0/design-fixes.dtsi:130.1-28 Label or path mipi_csi2_rx_v_proc_ss_csc not found
| Error: ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work/plnx_zynqmp-xilinx-linux/device-tree/xilinx+gitAUTOINC+b7466bbeee-r0/design-fixes.dtsi:134.1-39 Label or path mipi_csi2_rx_mipi_csi2_rx_subsystem_0 not found
| Error: ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work/plnx_zynqmp-xilinx-linux/device-tree/xilinx+gitAUTOINC+b7466bbeee-r0/design-fixes.dtsi:138.1-10 Label or path csiss_in not found
| FATAL ERROR: Syntax error parsing input tree
| WARNING: ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work/plnx_zynqmp-xilinx-linux/device-tree/xilinx+gitAUTOINC+b7466bbeee-r0/temp/run.do_compile.3126:1 exit 1 from 'dtc -I dts -O dtb -R 8 -p 0x1000 -b 0 -i ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/../components/plnx_workspace/device-tree/device-tree -i ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work-shared/plnx-zynqmp/kernel-source/include -i ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work-shared/plnx-zynqmp/kernel-source/arch/arm64/boot/dts/xilinx -i ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work-shared/plnx-zynqmp/kernel-source/include -i ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work/plnx_zynqmp-xilinx-linux/device-tree/xilinx+gitAUTOINC+b7466bbeee-r0 -o ${DTS_NAME}.dtb `basename ${DTS_FILE}`.pp'
| ERROR: Function failed: do_compile (log file is located at ~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/build/tmp/work/plnx_zynqmp-xilinx-linux/device-tree/xilinx+gitAUTOINC+b7466bbeee-r0/temp/log.do_compile.3126)
ERROR: Task (/opt/Xilinx/petalinux/v2018.3/components/yocto/source/aarch64/layers/meta-xilinx/meta-xilinx-bsp/recipes-bsp/device-tree/device-tree.bb:do_compile) failed with exit code '1'
NOTE: Tasks Summary: Attempted 4765 tasks of which 1649 didn't need to be rerun and 1 failed.

Summary: 1 task failed:
  /opt/Xilinx/petalinux/v2018.3/components/yocto/source/aarch64/layers/meta-xilinx/meta-xilinx-bsp/recipes-bsp/device-tree/device-tree.bb:do_compile
Summary: There was 1 ERROR message shown, returning a non-zero exit code.
ERROR: Failed to build project
```

### Missing device tree entries are:
- sensor_iic_0
- hdmi_ctrl_iic
- vid_phy_controller
- mpsoc_ss_hdmi_ctrl_iic
- tpg_input_v_tpg_1
- tpg_port0
- hdmi_input_v_hdmi_rx_ss_0
- hdmi_input_v_proc_ss_0
- axi_intc_0
- hdmi_output_v_hdmi_tx_ss_0
- hdmi_output_v_mix_0
- xx_mix_master
- mipi_csi2_rx_v_proc_ss_scaler
- mipi_csi2_rx_v_proc_ss_csc
- mipi_csi2_rx_mipi_csi2_rx_subsystem_0
- csiss_in

### Locate the first unused device tree entry
- First, find where the unused entries are defined in the device tree
- Start with __*sensor_iic_0*__
```bash
:~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi$ grep "sensor_iic_0" ./project-spec/ -r
	./project-spec/meta-user/recipes-bsp/device-tree/files/li-imx274mipi-fmc.dtsi:&sensor_iic_0 {
	./project-spec/meta-user/recipes-bsp/device-tree/files/design-fixes.dtsi:&sensor_iic_0 {
```

- Check the file __*design-fixes.dtsi*__
- Note: __*sensor_iic_0*__ is contained in a conditional statement based on the definition of __*CONFIG_CSI*__
```bash
:~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi$ cat project-spec/meta-user/recipes-bsp/device-tree/files/design-fixes.dtsi 
#ifdef CONFIG_HDMI_RX_MULTI_STREAM
#include "vcap-hdmi_2.dtsi"
#include "vcap-hdmi_3.dtsi"
#include "vcap-hdmi_4.dtsi"
#include "vcap-hdmi_5.dtsi"
#include "vcap-hdmi_6.dtsi"
#include "vcap-hdmi_7.dtsi"
#endif

...

#if defined(CONFIG_CSI)
&sensor_iic_0 {
        clocks = <&vid_s_axi_clk>;
};
&mipi_csi2_rx_v_proc_ss_scaler {
        compatible = "xlnx,v-vpss-scaler";
};

&mipi_csi2_rx_v_proc_ss_csc {
        compatible = "xlnx,v-vpss-csc";
};

&mipi_csi2_rx_mipi_csi2_rx_subsystem_0 {
        compatible = "xlnx,mipi-csi2-rx-subsystem-2.0";
};

&csiss_in {
        data-lanes = <1 2 3 4>;
        remote-endpoint = <&sensor_out>;
};
#endif
...

```

- Check to see where the file __*design-fixes.dtsi*__ is included in the device tree
```bash
:~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi$ grep "design-fixes.dtsi" ./project-spec/ -r
...
./project-spec/meta-user/recipes-bsp/device-tree/files/vcu_hdmirx.dtsi:#include "design-fixes.dtsi"
./project-spec/meta-user/recipes-bsp/device-tree/files/vcu_sditx.dtsi:#include "design-fixes.dtsi"
./project-spec/meta-user/recipes-bsp/device-tree/files/system-user.dtsi:#include "design-fixes.dtsi"
...

```

- Start with the __*system-user.dtsi*__ file
```bash
:~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi$ cat ./project-spec/meta-user/recipes-bsp/device-tree/files/system-user.dtsi
#include "system-conf.dtsi"

/* Define configuration */
#define CONFIG_HDMI_RX_MULTI_STREAM
#define CONFIG_TPG
#define CONFIG_CSI
#define CONFIG_HDMI_RX
#define CONFIG_HDMI_TX
#define CONFIG_VCU
#define CONFIG_APM

/* Includes */
#include "design-fixes.dtsi"
```

### Remove the unused device tree entry
- Comment out the __*CONFIG_CSI*__ definition to get rid of the __*sensor_iic_0*__ device
```bash
:~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi$ cat ./project-spec/meta-user/recipes-bsp/device-tree/files/system-user.dtsi
#include "system-conf.dtsi"

/* Define configuration */
#define CONFIG_HDMI_RX_MULTI_STREAM
#define CONFIG_TPG
//#define CONFIG_CSI
#define CONFIG_HDMI_RX
#define CONFIG_HDMI_TX
#define CONFIG_VCU
#define CONFIG_APM

/* Includes */
#include "design-fixes.dtsi"
```

### Repeat this process, locating and removing the remaining device tree entries:

#### sensor_iic_0, mipi_csi2_rx_v_proc_ss_scaler, mipi_csi2_rx_v_proc_ss_csc, mipi_csi2_rx_mipi_csi2_rx_subsystem_0, csiss_in
- Comment out __*CONFIG_CSI*__ in ./project-spec/meta-user/recipes-bsp/device-tree/files/system-user.dtsi
```C
#include "system-conf.dtsi"

/* Define configuration */
#define CONFIG_HDMI_RX_MULTI_STREAM
#define CONFIG_TPG
//#define CONFIG_CSI
#define CONFIG_HDMI_RX
#define CONFIG_HDMI_TX
#define CONFIG_VCU
#define CONFIG_APM

/* Includes */
#include "design-fixes.dtsi"
```

### hdmi_ctrl_iic, mpsoc_ss_hdmi_ctrl_iic, hdmi_input_v_hdmi_rx_ss_0, hdmi_input_v_proc_ss_0, hdmi_output_v_hdmi_tx_ss_0, hdmi_output_v_mix_0, axi_intc_0, vid_phy_controller, xx_mix_master
```C
#include "system-conf.dtsi"

/* Define configuration */
//#define CONFIG_HDMI_RX_MULTI_STREAM
#define CONFIG_TPG
//#define CONFIG_CSI
//#define CONFIG_HDMI_RX
//#define CONFIG_HDMI_TX
#define CONFIG_VCU
#define CONFIG_APM

/* Includes */
#include "design-fixes.dtsi
```

### tpg_input_v_tpg_1, tpg_port0
```C
#include "system-conf.dtsi"

/* Define configuration */
//#define CONFIG_HDMI_RX_MULTI_STREAM
//#define CONFIG_TPG
//#define CONFIG_CSI
//#define CONFIG_HDMI_RX
//#define CONFIG_HDMI_TX
#define CONFIG_VCU
#define CONFIG_APM

/* Includes */
#include "design-fixes.dtsi
```

### Execute the build
```bash
:~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi$ petalinux-build
[INFO] building project
[INFO] sourcing bitbake
INFO: bitbake petalinux-user-image
Loading cache: 100% |###################################################################################################################################################################| Time: 0:00:00
Loaded 3463 entries from dependency cache.
Parsing recipes: 100% |#################################################################################################################################################################| Time: 0:00:04
Parsing of 2572 .bb files complete (2538 cached, 34 parsed). 3464 targets, 137 skipped, 0 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |##############################################################################################################################################################| Time: 0:00:28
Checking sstate mirror object availability: 100% |######################################################################################################################################| Time: 0:00:03
NOTE: Executing SetScene Tasks
NOTE: Executing RunQueue Tasks
WARNING: gstd-1.0-r0 do_configure: QA Issue: gstd: configure was passed unrecognised options: --disable-gtk-doc [unknown-configure-option]
WARNING: petalinux-user-image-1.0-r0 do_rootfs: [log_check] petalinux-user-image: found 2 warning messages in the logfile:
[log_check] warning: %post(sysvinit-inittab-2.88dsf-r10.plnx_zynqmp) scriptlet failed, exit status 1
[log_check] warning: %post(xserver-nodm-init-3.0-r31.plnx_zynqmp) scriptlet failed, exit status 1

NOTE: Tasks Summary: Attempted 7516 tasks of which 5560 didn't need to be rerun and all succeeded.

Summary: There were 2 WARNING messages shown.
INFO: Copying Images from deploy to images
INFO: Creating images/linux directory
NOTE: copy to TFTP-boot directory is not enabled !!
[INFO] successfully built project
```

### Package the FSBL, FPGA design into a BOOT.bin image
```bash
:~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi$ petalinux-package --boot --format BIN --fsbl images/linux/zynqmp_fsbl.elf --u-boot images/linux/u-boot.elf --pmufw images/linux/pmufw.elf --fpga images/linux/system.bit --force
INFO: File in BOOT BIN: "~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/images/linux/zynqmp_fsbl.elf"
INFO: File in BOOT BIN: "~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/images/linux/pmufw.elf"
INFO: File in BOOT BIN: "~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/images/linux/system.bit"
INFO: File in BOOT BIN: "~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/images/linux/bl31.elf"
INFO: File in BOOT BIN: "~/avnet-uzev-ports/temp/vcu_hdmirx_nohdmi/images/linux/u-boot.elf"
INFO: Generating ZynqMP binary package BOOT.BIN...


****** Xilinx Bootgen v2018.3
  **** Build date : Nov 15 2018-19:22:29
    ** Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.

INFO: Binary is ready.

```

# Create a UZ7EV_CC working copy of the ZCU106 reference design module for __*vcu_hdmirx*__
- Use the "NODHMI" modified version as a starting point for design content

## Create a new set of TCL scripts for the UZ7EV_CC
```bash
:~/avnet-uzev-ports/temp$ cd rdf0428-zcu106-vcu-trd-2018-3
:~/avnet-uzev-ports/temp/rdf0428-zcu106-vcu-trd-2018-3$ cd pl
:~/avnet-uzev-ports/temp/rdf0428-zcu106-vcu-trd-2018-3/pl$ mkdir -p scripts_uz7ev
:~/avnet-uzev-ports/temp/rdf0428-zcu106-vcu-trd-2018-3/pl$ cp scripts/vcu_hdmirx_proj.tcl scripts_uz7ev/vcu_proj_uz7ev_cc.tcl 
:~/avnet-uzev-ports/temp/rdf0428-zcu106-vcu-trd-2018-3/pl$ cp scripts/vcu_hdmi_bd.tcl scripts_uz7ev/vcu_bd_uz7ev_cc.tcl
```

## Create a new set of Constraints files for the UZ7EV_CC
```bash
:~/avnet-uzev-ports/temp/rdf0428-zcu106-vcu-trd-2018-3/pl$ mkdir -p constrs_uz7ev
:~/avnet-uzev-ports/temp/rdf0428-zcu106-vcu-trd-2018-3/pl$ cp constrs/vcu_hdmirx.xdc constrs_uz7ev/vcu_uz7ev_cc.xdc
```

## Modify the new PROJ TCL script with new project information (__*vcu_proj_uz7ev_cc.tcl*__)
- Make modifications to this script, bringing over the board definition from the UZ7EV_CC tcl script.
- Also added parameterization to use __*platform*__ and __*design_name*__ variables to standardize variable use

File: ![vcu_proj_uz7ev_cc.tcl]("rdf0428-uz7ev-vcu-trd-2018-3/pl/scripts_uz7ev/vcu_proj_uz7ev_cc.tcl")

```bash
:~/avnet-uzev-ports/temp/rdf0428-zcu106-vcu-trd-2018-3/pl$ cat scripts_uz7ev/vcu_proj_uz7ev_cc.tcl
# global variables
#set ::platform "zcu106"
set ::platform "uz7ev_cc"
#set ::silicon "e"
set ::silicon "i"

# local variables
#set project_dir "vcu_hdmirx"
set project_dir "vcu_${platform}"
set ip_dir "srcs/ip"
#set constrs_dir "constrs"
set constrs_dir "constrs_uz7ev"
#set scripts_dir "scripts"
set scripts_dir "scripts_uz7ev"

# set variable names
#set part "xczu7ev-ffvc1156-2-${::silicon}"
set part "xczu7ev-fbvb900-1-${::silicon}"
#set design_name "vcu_hdmirx"
set design_name "vcu_${platform}"
puts "INFO: Target part selected: '$part'"
...
#set_property board_part xilinx.com:zcu106:part0:2.3 [current_project]
set_property BOARD_PART em.avnet.com:ultrazed_7ev_cc:part0:1.1 [current_project]

# set up IP repo
set_property ip_repo_paths $ip_dir [current_fileset]
update_ip_catalog -rebuild

# set up bd design
create_bd_design $design_name
#source $scripts_dir/vcu_hdmirx_bd.tcl
source $scripts_dir/vcu_bd_${platform}.tcl

# add hdl sources to project
#make_wrapper -files [get_files ./$project_dir/vcu_hdmirx.srcs/sources_1/bd/vcu_hdmirx/vcu_hdmirx.bd] -top
make_wrapper -files [get_files ./$project_dir/$design_name.srcs/sources_1/bd/$design_name/$design_name.bd] -top

#add_files -norecurse ./$project_dir/vcu_hdmirx.srcs/sources_1/bd/vcu_hdmirx/hdl/vcu_hdmirx_wrapper.v
add_files -norecurse ./$project_dir/$design_name.srcs/sources_1/bd/$design_name/hdl/${design_name}_wrapper.v

#set_property top vcu_hdmirx_wrapper [current_fileset]
set_property top ${design_name}_wrapper [current_fileset]

#add_files -fileset constrs_1 -norecurse $constrs_dir/vcu_hdmirx.xdc
add_files -fileset constrs_1 -norecurse $constrs_dir/${design_name}.xdc

update_compile_order -fileset sources_1
set_property strategy Performance_NetDelay_low [get_runs impl_1]       

validate_bd_design
update_compile_order -fileset sources_1
regenerate_bd_layout
save_bd_design
...
```

## Modify the new BD TCL script with new design definition information (__*vcu_bd_uz7ev_cc.tcl*__)
- Note: We will be converting the UZ Petalinux reference design to a hierarchical design to match the VCU TRD
- We will also be maintaining port assignments from the TRD to make it easier to re-use this effort and bring in additional functions from the TRD with mininmal changes
File: ![vcu_bd_uz7ev_cc.tcl]("rdf0428-uz7ev-vcu-trd-2018-3/pl/scripts_uz7ev/vcu_bd_uz7ev_cc.tcl")

### Update the project and board definitions
- Make the following modifications, bringing over the board definition from the UZ7EV_CC tcl script.
- Reuse the design_name variable from the project script

```bash
:~/avnet-uzev-ports/temp/rdf0428-zcu106-vcu-trd-2018-3/pl$ cat scripts_uz7ev/vcu_bd_uz7ev_cc.tcl
...
set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
#   create_project project_1 myproj -part xczu7ev-ffvc1156-2-e
   create_project project_1 myproj -part xczu7ev-fbvb900-1-i
#   set_property BOARD_PART xilinx.com:zcu106:part0:2.3 [current_project]
   set_property BOARD_PART em.avnet.com:ultrazed_7ev_cc:part0:1.1 [current_project]
}

# CHANGE DESIGN NAME HERE
# Design name set in top level project script
#variable design_name
#set design_name vcu_hdmirx
...
```

### Update the IP block check to incorporate the IP from the TRD reference design with hdmi removed
File: ![vcu_hdmirx_nohdmi.reference.tcl]("_artifacts/rdf0428-uz7ev-vcu-trd-2018-3/vcu_hdmirx_nohdmi.reference.tcl")

```bash
:~/avnet-uzev-ports/temp/rdf0428-zcu106-vcu-trd-2018-3/pl$ cat scripts_uz7ev/vcu_proj_uz7ev_cc.tcl
...
##################################################################
# CHECK IPs
##################################################################
set bCheckIPs 1
if { $bCheckIPs == 1 } {
   set list_check_ips "\ 
xilinx.com:ip:clk_wiz:*\
xilinx.com:ip:xlconstant:*\
xilinx.com:ip:xlconcat:*\
xilinx.com:ip:proc_sys_reset:*\
xilinx.com:ip:vcu:*\
xilinx.com:ip:xlslice:*\
xilinx.com:ip:zynq_ultra_ps_e:*\
"
...

```

### Update the IP block check to add the axi_gpio block used by the UZ reference design
File: ![uz_petalinux_v2018.3.reference.tcl]("_artifacts/rdf0428-uz7ev-vcu-trd-2018-3/uz_petalinux_v2018.3.reference.tcl")

```bash
:~/avnet-uzev-ports/temp/rdf0428-zcu106-vcu-trd-2018-3/pl$ cat scripts_uz7ev/vcu_proj_uz7ev_cc.tcl
...
##################################################################
# CHECK IPs
##################################################################
set bCheckIPs 1
if { $bCheckIPs == 1 } {
   set list_check_ips "\ 
xilinx.com:ip:clk_wiz:*\
xilinx.com:ip:xlconstant:*\
xilinx.com:ip:xlconcat:*\
xilinx.com:ip:proc_sys_reset:*\
xilinx.com:ip:vcu:*\
xilinx.com:ip:xlslice:*\
xilinx.com:ip:zynq_ultra_ps_e:*\
xilinx.com:ip:axi_gpio:*\
"
...
```

### Update the definition of the processor subsystem block in __*create_hier_cell_mpsoc_ss*__
- General Notes:
	- MPSOC AXI Bus Interfaces from TRD Design:
		- M_AXI_HPM0_FPD 	- used for (internal) AXI Interconnect Block
		- S_AXI_HP2_FPD 	- used for AXI Interconnect #1 (VCU Encoder Bus)
		- S_AXI_HP3_FPD 	- used for AXI Interconnect #2 (VCU Decoder Bus)
		- S_AXI_HPC0_FPD 	- used for AXI Interconnect #3 (VCU MCU Bus)
	- AXI Interconnect Block Port Use from TRD Design:
		- M04_AXI 			- used for IRQ
		- M08_AXI_0 		- used for VCU Connection
	- AXI Interconnect Block Port Use from UZ7EV Petalinux Design:
		- M00_AXI 			- used for GPIO_0 block (on-board dip switches)
		- M01_AXI 			- used for GPIO_1 block (on-board leds)
		- M02_AXI   		- used for GPIO_2 block (on-board push buttons)
	- AXI Interconnect Block Notes:
		- Only use 9 ports (0 through 8)
		- Watch clock use, based on TRD
		- There is the PL clock from MPSOC, then there is a second externally generated clock for the VCU and other high speed peripheral IP (HDMI, etc. from the TRD design)
	
- Notes for the __*zynq_ultra_ps_e_0*__ instance
	- Use the DDR configuration, PCIe configuration and 

### Add the UZ Petalinux design specific components to the root design in __*create_root_design*__
- General Notes:
	- Input Clock to the Board
		- __*sysclk_uz7ev*__ - 300MHz differential LVDS clock input
		- Note: This clock needs to be defined in the constraints file to specify input pin(s)
	- GPIO Peripherals
		- __*axi_gpio_0*__ for the on-board dip switches
		- __*axi_gpio_1*__ for the on-board leds
		- __*axi_gpio_2*__ for the on-board push-buttons
	- Verify clock and reset connections


## Test the VCU design creation using the newly created TCL scripts
```bash
:~/avnet-uzev-ports$ cd rdf0428-uz7ev-vcu-trd-2018-3
:~/avnet-uzev-ports/rdf0428-zcu106-vcu-trd-2018-3$ export TRD_HOME=$(pwd)
:~/avnet-uzev-ports/df0428-zcu106-vcu-trd-2018-3$ cd pl
:~/avnet-uzev-ports/rdf0428-zcu106-vcu-trd-2018-3/pl$ vivado -source scripts_uz7ev/vcu_proj_uz7ev_cc.tcl

****** Vivado v2018.3 (64-bit)
  **** SW Build 2405991 on Thu Dec  6 23:36:41 MST 2018
  **** IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
    ** Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.

start_gui
```

## Generate the Bitstream
- From the main menu
	- Flow -> Generate Bitstream

- If prompted regarding no implementation results
	- Select 'Yes'
	- Select 'OK'

## Generate the SDK files for BSP work
- From the main menu
	- File -> Export -> Export Hardware

- When prompted
	- Check 'Include bitstream'
	- Select 'OK'

## Exit Vivado


# Build Linux for the UltraZed-EV using the upgraded Vivado Design from the reference BSP __*uz_petalinux*__

## Create a targeted Petalinux Build for the upgraded Vivado Base Design

### Create a new Petalinux project
```bash
:~/avnet-uzev-ports/temp$ petalinux-create -t project -n uz_petalinux_v2018_3 --template zynqMP
INFO: Create project: uz_petalinux_v2018_3
INFO: New project successfully created in ~/avnet-uzev-ports/temp/uz_petalinux_v2018_3
```

### Use the modified Vivado design to configure the Petalinux project
```bash
:~/avnet-uzev-ports/temp$ cd uz_petalinux_v2018_3
:~/avnet-uzev-ports/temp/uz_petalinux_v2018_3$ petalinux-config --get-hw-description=~/avnet-uzev-ports/rdf0428-uz7ev-vcu-trd-2018-3/pl/vcu_uz7ev_cc/vcu_uz7ev_cc.sdk --oldconfig
INFO: Getting hardware description...
INFO: Rename vcu_uz7ev_cc_wrapper.hdf to system.hdf
[INFO] generating Kconfig for project
[INFO] oldconfig project
[INFO] sourcing bitbake
[INFO] generating plnxtool conf
[INFO] generating meta-plnx-generated layer
[INFO] generating machine configuration
[INFO] generating bbappends for project . This may take time ! 
[INFO] generating u-boot configuration files
[INFO] generating kernel configuration files
[INFO] generating kconfig for Rootfs
[INFO] oldconfig rootfs
[INFO] generating petalinux-user-image.bb
```

### Compare the upgraded petalinux configuration to the original uz_petalinux configuration from the v2018.2 BSP release
- Bring over any changes necessary from the v2018.2 build 

- ./meta-user/conf/layer.conf
	- Same

- ./meta-user/conf/petalinuxbsp.conf
	- Same

- ./meta-user/recipes-apps/
	- Copy the custom application recipes from the original uz_petalinux configuration
		- ./meta-user/recipes-apps/blinky
		- ./meta-user/recipes-apps/blinky-init
		- ./meta-user/recipes-apps/user-led-test
		- ./meta-user/recipes-apps/user-switch-test
		- ./meta-user/recipes-apps/mac-eeprom-config
		- ./meta-user/recipes-apps/mac-eeprom-config-init

- ./meta-user/recipes-bsp/device-tree/files/multi-arch/zynqmp-qemu-multiarch-arm.dts
	- Same

- ./meta-user/recipes-bsp/device-tree/files/multi-arch/zynqmp-qemu-multiarch-pmu.dts
	- Same

- ./meta-user/recipes-bsp/device-tree/files/system-user.dtsi
	- Copy this file from the uz7ev_evcc_2018_2/project-spec/meta-user/recipes-bsp/device-tree/files folder
		- system-user.dtsi

- ./meta-user/recipes-bsp/device-tree/files/zynqmp-qemu-arm.dts
	- Same

- ./meta-user/recipes-bsp/device-tree/device-tree.bbappend
	- Same

- ./meta-user/recipes-bsp/u-boot/files/platform-top.h
	- Note: https://forums.xilinx.com/t5/Embedded-Linux/Booting-Error-Reading-Image-but-not-successful/td-p/894893
	- Copy from the uz7ev_evcc_2018_2/project-spec/meta-user/recipes-bsp/u-boot/files/platform-top.h file to create the following:
```bash
:~/avnet-uzev-ports/temp/uz_petalinux_v2018_3/project-spec$$ cat meta-user/recipes-bsp/u-boot/files/platform-top.h
#include <configs/platform-auto.h>

#define CONFIG_NR_DRAM_BANKS 3

#define CONFIG_SYS_BOOTM_LEN 0xF000000
#undef CONFIG_BOOTARGS
#undef CONFIG_SYS_BOOTMAPSZ
#undef CONFIG_PREBOOT

#define DFU_ALT_INFO_RAM \
                "dfu_ram_info=" \
        "setenv dfu_alt_info " \
        "image.ub ram $netstart 0x1e00000\0" \
        "dfu_ram=run dfu_ram_info && dfu 0 ram 0\0" \
        "thor_ram=run dfu_ram_info && thordown 0 ram 0\0"


#define DFU_ALT_INFO_MMC \
        "dfu_mmc_info=" \
        "set dfu_alt_info " \
        "${kernel_image} fat 0 1\\\\;" \
        "dfu_mmc=run dfu_mmc_info && dfu 0 mmc 0\0" \
        "thor_mmc=run dfu_mmc_info && thordown 0 mmc 0\0"

#define DFU_ALT_INFO  \
                DFU_ALT_INFO_RAM \
                DFU_ALT_INFO_MMC


/*Required for uartless designs */
#ifndef CONFIG_BAUDRATE
#define CONFIG_BAUDRATE 115200
#ifdef CONFIG_DEBUG_UART
#undef CONFIG_DEBUG_UART
#endif
#endif

/*Define CONFIG_ZYNQMP_EEPROM here and its necessaries in u-boot menuconfig if you had EEPROM memory. */
#ifdef CONFIG_ZYNQMP_EEPROM
#define CONFIG_SYS_I2C_EEPROM_ADDR_LEN  1
#define CONFIG_CMD_EEPROM
#define CONFIG_ZYNQ_EEPROM_BUS          5
#define CONFIG_ZYNQ_GEM_EEPROM_ADDR     0x54
#define CONFIG_ZYNQ_GEM_I2C_MAC_OFFSET  0x20
#endif

/* Extra U-Boot Env settings */
#define CONFIG_EXTRA_ENV_SETTINGS \
	SERIAL_MULTI \ 
	CONSOLE_ARG \ 
	PSSERIAL0 \ 
	"nc=setenv stdout nc;setenv stdin nc;\0" \ 
	"ethaddr=00:0a:35:00:22:01\0" \
	"importbootenv=echo \"Importing environment from SD ...\"; " \ 
		"env import -t ${loadbootenv_addr} $filesize\0" \ 
	"loadbootenv=load mmc $sdbootdev:$partid ${loadbootenv_addr} ${bootenv}\0" \ 
	"sd_uEnvtxt_existence_test=test -e mmc $sdbootdev:$partid /uEnv.txt\0" \ 
	"uenvboot=" \ 
        "if run sd_uEnvtxt_existence_test; then " \ 
            "run loadbootenv; " \  
            "echo Loaded environment from ${bootenv}; " \ 
            "run importbootenv; " \
        "fi\0" \
	"sdboot=echo boot Petalinux; run uenvboot ; mmcinfo && fatload mmc 1 ${netstart} ${kernel_img} && bootm \0" \ 
	"autoload=no\0" \ 
	"clobstart=0x10000000\0" \ 
	"netstart=0x10000000\0" \ 
	"dtbnetstart=0x11800000\0" \ 
	"loadaddr=0x10000000\0" \ 
	"boot_img=BOOT.BIN\0" \ 
	"load_boot=tftpboot ${clobstart} ${boot_img}\0" \ 
	"update_boot=setenv img boot; setenv psize ${bootsize}; setenv installcmd \"install_boot\"; run load_boot ${installcmd}; setenv img; setenv psize; setenv installcmd\0" \ 
	"install_boot=mmcinfo && fatwrite mmc 0 ${clobstart} ${boot_img} ${filesize}\0" \ 
	"bootenvsize=0x40000\0" \ 
	"bootenvstart=0x1360000\0" \ 
	"eraseenv=sf probe 0 && sf erase ${bootenvstart} ${bootenvsize}\0" \ 
	"jffs2_img=rootfs.jffs2\0" \ 
	"load_jffs2=tftpboot ${clobstart} ${jffs2_img}\0" \ 
	"update_jffs2=setenv img jffs2; setenv psize ${jffs2size}; setenv installcmd \"install_jffs2\"; run load_jffs2 test_img; setenv img; setenv psize; setenv installcmd\0" \ 
	"sd_update_jffs2=echo Updating jffs2 from SD; mmcinfo && fatload mmc 0:1 ${clobstart} ${jffs2_img} && run install_jffs2\0" \ 
	"install_jffs2=sf probe 0 && sf erase ${jffs2start} ${jffs2size} && " \ 
		"sf write ${clobstart} ${jffs2start} ${filesize}\0" \ 
	"kernel_img=image.ub\0" \ 
	"load_kernel=tftpboot ${clobstart} ${kernel_img}\0" \ 
	"update_kernel=setenv img kernel; setenv psize ${kernelsize}; setenv installcmd \"install_kernel\"; run load_kernel ${installcmd}; setenv img; setenv psize; setenv installcmd\0" \ 
	"install_kernel=mmcinfo && fatwrite mmc 1 ${clobstart} ${kernel_img} ${filesize}\0" \ 
	"cp_kernel2ram=mmcinfo && fatload mmc 1 ${netstart} ${kernel_img}\0" \ 
	"dtb_img=system.dtb\0" \ 
	"load_dtb=tftpboot ${clobstart} ${dtb_img}\0" \ 
	"update_dtb=setenv img dtb; setenv psize ${dtbsize}; setenv installcmd \"install_dtb\"; run load_dtb test_img; setenv img; setenv psize; setenv installcmd\0" \ 
	"sd_update_dtb=echo Updating dtb from SD; mmcinfo && fatload mmc 0:1 ${clobstart} ${dtb_img} && run install_dtb\0" \ 
	"fault=echo ${img} image size is greater than allocated place - partition ${img} is NOT UPDATED\0" \ 
	"test_crc=if imi ${clobstart}; then run test_img; else echo ${img} Bad CRC - ${img} is NOT UPDATED; fi\0" \ 
	"test_img=setenv var \"if test ${filesize} -gt ${psize}\\; then run fault\\; else run ${installcmd}\\; fi\"; run var; setenv var\0" \ 
	"netboot=tftpboot ${netstart} ${kernel_img} && bootm\0" \ 
	"default_bootcmd=run cp_kernel2ram && bootm ${netstart}\0" \ 
""
```

- ./meta-user/recipes-bsp/u-boot/u-boot-xlnx_%.bbappend
	- Same

- ./meta-user/recipes-core/images/petalinux-image-full.bbappend
	- Copy from uz7ev_evcc_2018_2/project-spec/meta-user/recipes-core/images/petalinux-image.bbappend to create:
```bash
:~/avnet-uzev-ports/temp/uz_petalinux_v2018_3/project-spec$$ cat meta-user/recipes-core/images/petalinux-image.bbappend
#Note: Mention Each package in individual line
#      cascaded representation with line breaks are not valid in this file.
IMAGE_INSTALL_append = " peekpoke"
IMAGE_INSTALL_append = " gpio-demo"
IMAGE_INSTALL_append = " blinky"
IMAGE_INSTALL_append = " blinky-init"
IMAGE_INSTALL_append = " iperf3"
IMAGE_INSTALL_append = " bonnie++"

IMAGE_INSTALL_append = " user-led-test"
IMAGE_INSTALL_append = " user-switch-test"

IMAGE_INSTALL_append = " mac-eeprom-config"
IMAGE_INSTALL_append = " mac-eeprom-config-init"
```
<---------------- REMOVE ME ------------------------>
<---------------- REMOVE ME ------------------------>
<---------------- REMOVE ME ------------------------>
<---------------- REMOVE ME ------------------------>
- ./project-spec/configs/config (Check the following UBOOT CONFIG)
```bash
	- CONFIG_SUBSYSTEM_UBOOT_CONFIG_TARGET="xilinx_zynqmp_zcu102_rev1_0_defconfig"
```
<---------------- REMOVE ME ------------------------>
<---------------- REMOVE ME ------------------------>
<---------------- REMOVE ME ------------------------>
<---------------- REMOVE ME ------------------------>

- ./project-spec/configs/rootfs.config
	- Configure the rootfs in Petalinux
	- Add the following configurations to the rootfs
```
	- CONFIG_bc=y
		: Filesystem Packages : base : bc : [*] bc

	- CONFIG_i2c-tools=y
		: Filesystem Packages : base : i2c-tools : [*] i2c-tools

	- CONFIG_i2c-tools-misc=y
		: Filesystem Packages : base : i2c-tools : [*] i2c-tools-misc

	- CONFIG_usbutils=y
		: Filesystem Packages : base : usbutils : [*] usbutils

	- CONFIG_ethtool=y
		: Filesystem Packages : console : network : ethtool : [*] ethtool

	- CONFIG_hdparm=y
		: Filesystem Packages : console : utils : hdparm : [*] hdparm

	- CONFIG_coreutils=y
		: Filesystem Packages : misc : coreutils : [*] coreutils

	- CONFIG_libv4l=y
		: Filesystem Packages : misc : v4l-utils : [*] libv4l

	- CONFIG_media-ctl=y
		: Filesystem Packages : misc : v4l-utils : [*] media-ctl

	- CONFIG_yavta=y
		: Filesystem Packages : misc : yavta : [*] yavta

	- CONFIG_libdrm=y
		: Filesystem Packages : x11 : base : libdrm : [*] libdrm

	- CONFIG_libdrm-tests=y
		: Filesystem Packages : x11 : base : libdrm : [*] libdrm-tests

	- CONFIG_libdrm-kms=y
		: Filesystem Packages : x11 : base : libdrm : [*] libdrm-kms

	# In the apps section
	- CONFIG_blinky=y
		: apps : [*] blinky (NEW)

	- CONFIG_blinky-init=y
		: apps : [*] blinky-init (NEW)

	- CONFIG_mac-eeprom-config=y
		: apps : [*] mac-eeprom-config (NEW)

	- CONFIG_mac-eeprom-config-init=y
		: apps : [*] mac-eeprom-config-init (NEW)

	- CONFIG_user-led-test=y
		: apps : [*] user-led-tests (NEW)

	- CONFIG_user-switch-test=y
		: apps : [*] user-switch-tests (NEW)

	- CONFIG_bonniePLUSPLUS=y
		: user packages : [*] bonnie++ (NEW)

	- CONFIG_iperf3=y
		: user packages : [*] iperf3 (NEW)
```

- Launch the rootfs menu configuration and select the above configuration parameters
```bash
:~/avnet-uzev-ports/temp$ cd uz_petalinux_v2018_3
:~/avnet-uzev-ports/temp/uz_petalinux_v2018_3$ petalinux-config -c rootfs
[INFO] sourcing bitbake
[INFO] generating plnxtool conf
[INFO] generating meta-plnx-generated layer
[INFO] generating machine configuration
[INFO] configuring: rootfs
[INFO] generating kconfig for Rootfs
[INFO] menuconfig rootfs

<!-- KCONFIG MENU LAUNCHES HERE -->

configuration written to ~/avnet-uzev-ports/temp/uz_petalinux_v2018_3/project-spec/configs/rootfs_config

*** End of the configuration.
*** Execute 'make' to start the build or try 'make help'.

[INFO] generating petalinux-user-image.bb
[INFO] successfully configured rootfs
```

### Execute the build
```bash
:~/avnet-uzev-ports/temp/uz_petalinux_v2018_3$ petalinux-build
[INFO] building project
[INFO] sourcing bitbake
INFO: bitbake petalinux-user-image
Parsing recipes: 100% |#######################################################################################################################################################################################################| Time: 0:01:15
Parsing of 2575 .bb files complete (0 cached, 2575 parsed). 3467 targets, 137 skipped, 0 masked, 0 errors.
WARNING: No recipes available for:
  ~/avnet-uzev-ports/temp/uz_petalinux_v2018_3/project-spec/meta-user/recipes-apps/blinky-init/petalinux-image.bbappend
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |####################################################################################################################################################################################################| Time: 0:00:10
Checking sstate mirror object availability: 100% |############################################################################################################################################################################| Time: 0:00:12
NOTE: Executing SetScene Tasks
NOTE: Executing RunQueue Tasks
WARNING: petalinux-user-image-1.0-r0 do_rootfs: [log_check] petalinux-user-image: found 1 warning message in the logfile:
[log_check] warning: %post(sysvinit-inittab-2.88dsf-r10.plnx_zynqmp) scriptlet failed, exit status 1

NOTE: Tasks Summary: Attempted 3520 tasks of which 2451 didn't need to be rerun and all succeeded.

Summary: There were 2 WARNING messages shown.
INFO: Copying Images from deploy to images
INFO: Creating images/linux directory
NOTE: Successfully copied built images to tftp dir:  /tftpboot
[INFO] successfully built project
```

### Package the FSBL, FPGA design into a BOOT.bin image
```bash
:~/avnet-uzev-ports/temp/uz_petalinux_v2018_3$ petalinux-package --boot --format BIN --fsbl images/linux/zynqmp_fsbl.elf --u-boot images/linux/u-boot.elf --pmufw images/linux/pmufw.elf --fpga images/linux/system.bit --force
INFO: File in BOOT BIN: "~/avnet-uzev-ports/temp/uz_petalinux_v2018_3/images/linux/zynqmp_fsbl.elf"
INFO: File in BOOT BIN: "~/avnet-uzev-ports/temp/uz_petalinux_v2018_3/images/linux/pmufw.elf"
INFO: File in BOOT BIN: "~/avnet-uzev-ports/temp/uz_petalinux_v2018_3/images/linux/system.bit"
INFO: File in BOOT BIN: "~/avnet-uzev-ports/temp/uz_petalinux_v2018_3/images/linux/bl31.elf"
INFO: File in BOOT BIN: "~/avnet-uzev-ports/temp/uz_petalinux_v2018_3/images/linux/u-boot.elf"
INFO: Generating ZynqMP binary package BOOT.BIN...


****** Xilinx Bootgen v2018.3
  **** Build date : Nov 15 2018-19:22:29
    ** Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.

INFO: Binary is ready.
```

### Test Booting the new images from SD Card (BOOT.BIN, image.ub)
```bash
Xilinx Zynq MP First Stage Boot Loader 
Release 2018.3   Jun 13 2019  -  03:15:44
NOTICE:  ATF running on XCZU7EV/silicon v4/RTL5.1 at 0xfffea000
NOTICE:  BL31: Secure code at 0x0
NOTICE:  BL31: Non secure code at 0x8000000
NOTICE:  BL31: v1.5(release):xilinx-v2018.2-919-g08560c36
NOTICE:  BL31: Built : 03:14:19, Jun 13 2019
PMUFW:  v1.1


U-Boot 2018.01 (Jun 13 2019 - 03:14:28 +0000) Xilinx ZynqMP ZCU102 rev1.0

I2C:   ready
DRAM:  4 GiB
EL Level:       EL2
Chip ID:        zu7ev
MMC:   mmc@ff160000: 0 (eMMC), mmc@ff170000: 1 (SD)
SF: Detected n25q256a with page size 512 Bytes, erase size 128 KiB, total 64 MiB
*** Warning - bad CRC, using default environment

In:    serial@ff000000
Out:   serial@ff000000
Err:   serial@ff000000
Board: Xilinx ZynqMP
Bootmode: LVL_SHFT_SD_MODE1
Net:   ZYNQ GEM: ff0e0000, phyaddr 0, interface rgmii-id
eth0: ethernet@ff0e0000
U-BOOT for uz_petalinux_v2018_3

ethernet@ff0e0000 Waiting for PHY auto negotiation to complete....... done
BOOTP broadcast 1
BOOTP broadcast 2
BOOTP broadcast 3
DHCP client bound to address 192.168.1.109 (812 ms)
Hit any key to stop autoboot:  0 
Device: mmc@ff160000
Manufacturer ID: 13
OEM: 14e
Name: Q2J55 
Tran Speed: 200000000
Rd Block Len: 512
MMC version 5.0
High Capacity: Yes
Capacity: 7.1 GiB
Bus Width: 4-bit
Erase Group Size: 512 KiB
HC WP Group Size: 8 MiB
User Capacity: 7.1 GiB WRREL
Boot Capacity: 16 MiB ENH
RPMB Capacity: 4 MiB ENH
reading image.ub
22115356 bytes read in 1866 ms (11.3 MiB/s)
## Loading kernel from FIT Image at 10000000 ...
   Using 'conf@system-top.dtb' configuration
   Trying 'kernel@1' kernel subimage
     Description:  Linux kernel
     Type:         Kernel Image
     Compression:  gzip compressed
     Data Start:   0x10000104
     Data Size:    7083901 Bytes = 6.8 MiB
     Architecture: AArch64
     OS:           Linux
     Load Address: 0x00080000
     Entry Point:  0x00080000
     Hash algo:    sha1
     Hash value:   d603dc6e0e878300170f842eda40a2d7fe7a617f
   Verifying Hash Integrity ... sha1+ OK
## Loading ramdisk from FIT Image at 10000000 ...
   Using 'conf@system-top.dtb' configuration
   Trying 'ramdisk@1' ramdisk subimage
     Description:  petalinux-user-image
     Type:         RAMDisk Image
     Compression:  gzip compressed
     Data Start:   0x106ca508
     Data Size:    14993796 Bytes = 14.3 MiB
     Architecture: AArch64
     OS:           Linux
     Load Address: unavailable
     Entry Point:  unavailable
     Hash algo:    sha1
     Hash value:   3ec9ac124a47e038ff798ecc44378e77633a3490
   Verifying Hash Integrity ... sha1+ OK
## Loading fdt from FIT Image at 10000000 ...
   Using 'conf@system-top.dtb' configuration
   Trying 'fdt@system-top.dtb' fdt subimage
     Description:  Flattened Device Tree blob
     Type:         Flat Device Tree
     Compression:  uncompressed
     Data Start:   0x106c1988
     Data Size:    35516 Bytes = 34.7 KiB
     Architecture: AArch64
     Hash algo:    sha1
     Hash value:   fc5d8e9b8bd53e4caa6308c0b66e9ab77090b559
   Verifying Hash Integrity ... sha1+ OK
   Booting using the fdt blob at 0x106c1988
   Uncompressing Kernel Image ... OK
   Loading Ramdisk to 071b3000, end 07fff984 ... OK
   Loading Device Tree to 00000000071a7000, end 00000000071b2abb ... OK

Starting kernel ...

[    0.000000] Booting Linux on physical CPU 0x0
[    0.000000] Linux version 4.14.0-xilinx-v2018.3 (oe-user@oe-host) (gcc version 7.3.0 (GCC)) #1 SMP Thu Jun 13 03:18:19 UTC 2019
[    0.000000] Boot CPU: AArch64 Processor [410fd034]
[    0.000000] Machine model: xlnx,zynqmp
...
Starting syslogd/klogd: done
Starting tcf-agent: OK

PetaLinux 2018.3 uz_petalinux_v2018_3 /dev/ttyPS0

uz_petalinux_v2018_3 login: 
```

### Package the current configuration is a BSP file to preserve the current state of development
```bash
:~/avnet-uzev-ports/temp$ petalinux-package --bsp -p ~/avnet-uzev-ports/temp/uz_petalinux_v2018_3 --hwsource ~/avnet-uzev-ports/temp/rdf0428-zcu106-vcu-trd-2018-3/pl/vcu_uz7ev_cc --output vcu_uz7ev_cc_petalinux_2018_3_boot-test.bsp 
~/avnet-uzev-ports/temp/vcu_uz7ev_cc_petalinux_2018_3_boot-test.bsp
INFO: Target BSP "~/avnet-uzev-ports/temp/vcu_uz7ev_cc_petalinux_2018_3_boot-test.bsp" will contain the following projects
INFO: PetaLinux project: ~/avnet-uzev-ports/temp/uz_petalinux_v2018_3
INFO:   Copying ~/avnet-uzev-ports/temp/uz_petalinux_v2018_3/config.project
INFO:   Copying ~/avnet-uzev-ports/temp/uz_petalinux_v2018_3/.petalinux
INFO:   Copying ~/avnet-uzev-ports/temp/uz_petalinux_v2018_3/.gitignore
INFO:   Copying ~/avnet-uzev-ports/temp/uz_petalinux_v2018_3/project-spec
INFO:   Copying ~/avnet-uzev-ports/temp/uz_petalinux_v2018_3/components
INFO:   Copying hardware project ~/avnet-uzev-ports/temp/rdf0428-zcu106-vcu-trd-2018-3/pl/vcu_uz7ev_cc
INFO: Creating BSP
INFO: Generating package vcu_uz7ev_cc_petalinux_2018_3_boot-test.bsp...
INFO: BSP is ready
```

# Build Linux for the UltraZed-EV using the newly package BSP __*vcu_uz7ev_cc_petalinux_2018_3_boot-test.bsp*__

## Create a targeted Petalinux Build for the upgraded Vivado Base Design

### Create a new Petalinux project
```bash
:~/avnet-uzev-ports/temp$ petalinux-create -t project -n uz_petalinux_from_bsp_v2018_3 -s ~/avnet-uzev-ports/temp/vcu_uz7ev_cc_petalinux_2018_3_boot-test.bsp
INFO: Create project: uz_petalinux_from_bsp_v2018_3
INFO: New project successfully created in ~/avnet-uzev-ports/temp/uz_petalinux_from_bsp_v2018_3
```

### Execute the build with the default bsp configuration
```bash
:~/avnet-uzev-ports/temp/uz_petalinux_v2018_3$ cd uz_petalinux_from_bsp_v2018_3
:~/avnet-uzev-ports/temp/uz_petalinux_v2018_3$ petalinux-build
xilinx@xilinx_petalinux_v2018:~/avnet-uzev-ports/temp/uz_petalinux_from_bsp_v2018_3$ petalinux-build
[INFO] building project
[INFO] generating Kconfig for project
[INFO] oldconfig project
[INFO] sourcing bitbake
[INFO] generating plnxtool conf
[INFO] generating meta-plnx-generated layer
[INFO] generating machine configuration
[INFO] generating bbappends for project . This may take time ! 
[INFO] generating u-boot configuration files
[INFO] generating kernel configuration files
[INFO] generating kconfig for Rootfs
[INFO] oldconfig rootfs
[INFO] generating petalinux-user-image.bb
INFO: bitbake petalinux-user-image
Loading cache: 100% |################################################################################| Time: 0:00:00
Loaded 3466 entries from dependency cache.
Parsing recipes: 100% |##############################################################################| Time: 0:00:05
Parsing of 2575 .bb files complete (2534 cached, 41 parsed). 3467 targets, 137 skipped, 0 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |###########################################################################| Time: 0:00:11
Checking sstate mirror object availability: 100% |###################################################| Time: 0:00:10
NOTE: Executing SetScene Tasks
NOTE: Executing RunQueue Tasks
WARNING: petalinux-user-image-1.0-r0 do_rootfs: [log_check] petalinux-user-image: found 1 warning message in the logfile:
[log_check] warning: %post(sysvinit-inittab-2.88dsf-r10.plnx_zynqmp) scriptlet failed, exit status 1

NOTE: Tasks Summary: Attempted 3520 tasks of which 2451 didn't need to be rerun and all succeeded.

Summary: There was 1 WARNING message shown.
INFO: Copying Images from deploy to images
INFO: Creating images/linux directory
NOTE: Successfully copied built images to tftp dir:  /tftpboot
[INFO] successfully built project
```

### Package the FSBL, FPGA design into a BOOT.bin image
```bash
:~/avnet-uzev-ports/temp/uz_petalinux_from_bsp_v2018_3$ petalinux-package --boot --format BIN --fsbl images/linux/zynqmp_fsbl.elf --u-boot images/linux/u-boot.elf --pmufw images/linux/pmufw.elf --fpga images/linux/system.bit --force
INFO: File in BOOT BIN: "~/avnet-uzev-ports/temp/uz_petalinux_from_bsp_v2018_3/images/linux/zynqmp_fsbl.elf"
INFO: File in BOOT BIN: "~/avnet-uzev-ports/temp/uz_petalinux_from_bsp_v2018_3/images/linux/pmufw.elf"
INFO: File in BOOT BIN: "~/avnet-uzev-ports/temp/uz_petalinux_from_bsp_v2018_3/images/linux/system.bit"
INFO: File in BOOT BIN: "~/avnet-uzev-ports/temp/uz_petalinux_from_bsp_v2018_3/images/linux/bl31.elf"
INFO: File in BOOT BIN: "~/avnet-uzev-ports/temp/uz_petalinux_from_bsp_v2018_3/images/linux/u-boot.elf"
INFO: Generating ZynqMP binary package BOOT.BIN...


****** Xilinx Bootgen v2018.3
  **** Build date : Nov 15 2018-19:22:29
    ** Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.

INFO: Binary is ready.
```

### Test Booting the new images from SD Card (BOOT.BIN, image.ub)
```bash
Xilinx Zynq MP First Stage Boot Loader 
Release 2018.3   Jun 13 2019  -  07:22:51
NOTICE:  ATF running on XCZU7EV/silicon v4/RTL5.1 at 0xfffea000
NOTICE:  BL31: Secure code at 0x0
NOTICE:  BL31: Non secure code at 0x8000000
NOTICE:  BL31: v1.5(release):xilinx-v2018.2-919-g08560c36
NOTICE:  BL31: Built : 07:22:51, Jun 13 2019
PMUFW:  v1.1


U-Boot 2018.01 (Jun 13 2019 - 07:21:17 +0000) Xilinx ZynqMP ZCU102 rev1.0

I2C:   ready
DRAM:  4 GiB
EL Level:       EL2
Chip ID:        zu7ev
MMC:   mmc@ff160000: 0 (eMMC), mmc@ff170000: 1 (SD)
SF: Detected n25q256a with page size 512 Bytes, erase size 128 KiB, total 64 MiB
*** Warning - bad CRC, using default environment

In:    serial@ff000000
Out:   serial@ff000000
Err:   serial@ff000000
Board: Xilinx ZynqMP
Bootmode: LVL_SHFT_SD_MODE1
Net:   ZYNQ GEM: ff0e0000, phyaddr 0, interface rgmii-id
eth0: ethernet@ff0e0000
U-BOOT for uz_petalinux_v2018_3

ethernet@ff0e0000 Waiting for PHY auto negotiation to complete....... done
BOOTP broadcast 1
BOOTP broadcast 2
BOOTP broadcast 3
DHCP client bound to address 192.168.1.109 (812 ms)
Hit any key to stop autoboot:  0 
Device: mmc@ff160000
Manufacturer ID: 13
OEM: 14e
Name: Q2J55 
Tran Speed: 200000000
Rd Block Len: 512
MMC version 5.0
High Capacity: Yes
Capacity: 7.1 GiB
Bus Width: 4-bit
Erase Group Size: 512 KiB
HC WP Group Size: 8 MiB
User Capacity: 7.1 GiB WRREL
Boot Capacity: 16 MiB ENH
RPMB Capacity: 4 MiB ENH
reading image.ub
22115356 bytes read in 1866 ms (11.3 MiB/s)
## Loading kernel from FIT Image at 10000000 ...
   Using 'conf@system-top.dtb' configuration
   Trying 'kernel@1' kernel subimage
     Description:  Linux kernel
     Type:         Kernel Image
     Compression:  gzip compressed
     Data Start:   0x10000104
     Data Size:    7083901 Bytes = 6.8 MiB
     Architecture: AArch64
     OS:           Linux
     Load Address: 0x00080000
     Entry Point:  0x00080000
     Hash algo:    sha1
     Hash value:   d603dc6e0e878300170f842eda40a2d7fe7a617f
   Verifying Hash Integrity ... sha1+ OK
## Loading ramdisk from FIT Image at 10000000 ...
   Using 'conf@system-top.dtb' configuration
   Trying 'ramdisk@1' ramdisk subimage
     Description:  petalinux-user-image
     Type:         RAMDisk Image
     Compression:  gzip compressed
     Data Start:   0x106ca508
     Data Size:    14993796 Bytes = 14.3 MiB
     Architecture: AArch64
     OS:           Linux
     Load Address: unavailable
     Entry Point:  unavailable
     Hash algo:    sha1
     Hash value:   3ec9ac124a47e038ff798ecc44378e77633a3490
   Verifying Hash Integrity ... sha1+ OK
## Loading fdt from FIT Image at 10000000 ...
   Using 'conf@system-top.dtb' configuration
   Trying 'fdt@system-top.dtb' fdt subimage
     Description:  Flattened Device Tree blob
     Type:         Flat Device Tree
     Compression:  uncompressed
     Data Start:   0x106c1988
     Data Size:    35516 Bytes = 34.7 KiB
     Architecture: AArch64
     Hash algo:    sha1
     Hash value:   fc5d8e9b8bd53e4caa6308c0b66e9ab77090b559
   Verifying Hash Integrity ... sha1+ OK
   Booting using the fdt blob at 0x106c1988
   Uncompressing Kernel Image ... OK
   Loading Ramdisk to 071b3000, end 07fff984 ... OK
   Loading Device Tree to 00000000071a7000, end 00000000071b2abb ... OK

Starting kernel ...

[    0.000000] Booting Linux on physical CPU 0x0
[    0.000000] Linux version 4.14.0-xilinx-v2018.3 (oe-user@oe-host) (gcc version 7.3.0 (GCC)) #1 SMP Thu Jun 13 03:18:19 UTC 2019
[    0.000000] Boot CPU: AArch64 Processor [410fd034]
[    0.000000] Machine model: xlnx,zynqmp
...
Starting syslogd/klogd: done
Starting tcf-agent: OK

PetaLinux 2018.3 uz_petalinux_v2018_3 /dev/ttyPS0

uz_petalinux_v2018_3 login: 
```

# Create a Linux Build for the UltraZed-EV with VCU Support by bringing in the TRD Reference BSP configuration from the __*vcu_hdmirx_nohdmi*__ build

## Create a targeted Petalinux Build for VCU support

### Create a new Petalinux project
```bash
:~/avnet-uzev-ports/temp$ petalinux-create -t project -n uz_petalinux_v2018_3_vcu_uz7ev_cc -s ~/avnet-uzev-ports/temp/vcu_uz7ev_cc_petalinux_2018_3_boot-test.bsp
INFO: Create project: uz_petalinux_v2018_3_vcu_uz7ev_cc
INFO: New project successfully created in ~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc
```

### Compare the base v2018.3 petalinux to the vcu_hdmirx_nohdmi Petalinux build configuration
- Bring over any changes necessary from the v2018.3 vcu_hdmirx_nohdmi build configuration

- ./meta-user/conf/layer.conf
	- Same

- ./meta-user/conf/petalinuxbsp.conf
	- Same

- ./meta-user/recipes-apps/
	- Copy the custom application recipes from the original vcu_hdmirx_nohdmi configuration
		- ./meta-user/recipes-apps/sds_lib
		- ./meta-user/recipes-apps/trd-init

- ./meta-user/recipes-bsp/device-tree/files/multi-arch/zynqmp-qemu-multiarch-arm.dts
	- Not present in TRD

- ./meta-user/recipes-bsp/device-tree/files/multi-arch/zynqmp-qemu-multiarch-pmu.dts
	- Not present in TRD

- ./meta-user/recipes-bsp/device-tree/files/
	- Copy all of the following device tree includes from the original vcu_hdmirx_nohdmi configuration
		- ./meta-user/recipes-bsp/device-tree/files/
			- apm.dtsi (vcu_apm support)
			- design-fixes.dtsi (configurable for TRD design modules)
			- xlnk.dtsi

	- The following are unused at this time
		- ./meta-user/recipes-bsp/device-tree/files/
			- 0001-vproc_ss-Updated-node-name-for-SDI.patch
			- hdmi-misc.dtsi
			- li-imx274mipi-fmc.dtsi (MIPI CMOS Camera)
			- vcap-hdmi_[2:7].dtsi (Video Subdevice definitions for multi-stream demo)
			- vcu_[\*].dtsi (Example configurations that go in system-user.dtsi for each TRD Design Module)

- ./meta-user/recipes-bsp/device-tree/files/system-conf.dtsi
	- Move the contents of the existing system-user.dtsi into the system-conf.dtsi for the uz_petalnux_v2018_3 configuration
		- system-conf.dtsi should now look like this
```bash
:~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc/project-spec$ cat meta-user/recipes-bsp/device-tree/files/system-conf.dtsi
/ {
	chosen {
		bootargs = "earlycon clk_ignore_unused consoleblank=0 cma=1700M uio_pdrv_genirq.of_id=generic-uio";
		stdout-path = "serial0:115200n8";
	};
};

&gem3 {
	status = "okay";
	local-mac-address = [00 0a 35 00 02 90];
	phy-mode = "rgmii-id";
	phy-handle = <&phy0>;
	phy0: phy@0 {
		reg = <0x0>;
		ti,rx-internal-delay = <0x5>;
		ti,tx-internal-delay = <0x5>;
		ti,fifo-depth = <0x1>;
	};
};

&i2c1 {
	status = "okay";
	clock-frequency = <400000>;

	i2cswitch@70 { /* U7 on UZ3EG SOM, U8 on UZ7EV SOM */
		compatible = "nxp,pca9542";
		#address-cells = <1>;
		#size-cells = <0>;
		reg = <0x70>;
		i2c@0 { /* i2c mw 70 0 1 */
			#address-cells = <1>;
			#size-cells = <0>;
			reg = <0>;
			/* Ethernet MAC ID EEPROM */
			mac_eeprom@51 { /* U5 on UZ3EG IOCC and U7 on the UZ7EV EVCC */
				compatible = "at,24c08";
				reg = <0x51>;
			};
			/* CLOCK2 CONFIG EEPROM */
			clock_eeprom@52 { /* U5 on the UZ7EV EVCC */
				compatible = "at,24c08";
				reg = <0x52>;
			};
		};
	};
};

&qspi {
	#address-cells = <1>;
	#size-cells = <0>;
	status = "okay";
	is-dual = <1>; /* Set for dual-parallel QSPI config */
	num-cs = <2>;
	xlnx,fb-clk = <0x1>;
	flash0: flash@0 {
        /* The Flash described below doesn't match our board ("micron,n25qu256a"), but is needed */
        /* so the Flash MTD partitions are correctly identified in /proc/mtd */
		compatible = "micron,m25p80"; /* 32MB */
		#address-cells = <1>;
		#size-cells = <1>;
		reg = <0x0>;
		spi-tx-bus-width = <1>;
		spi-rx-bus-width = <4>; /* FIXME also DUAL configuration possible */
		spi-max-frequency = <108000000>; /* Set to 108000000 Based on DC1 spec */
	};
};

/* SD0 eMMC, 8-bit wide data bus */
&sdhci0 {
	status = "okay";
	bus-width = <8>;
	max-frequency = <50000000>;
};

/* SD1 with level shifter */
&sdhci1 {
	status = "okay";
	max-frequency = <50000000>;
	no-1-8-v;	/* for 1.0 silicon */
};

/* ULPI SMSC USB3320 */
&usb0 {
	status = "okay";
};

&dwc3_0 {
	status = "okay";
	dr_mode = "host";
	phy-names = "usb3-phy";
    snps,usb3_lpm_capable;
    phys = <&lane2 4 0 2 52000000>;
};
```

- ./meta-user/recipes-bsp/device-tree/files/system-user.dtsi
	- Modify this to match the vcu_hdmirx_nohdmi design
```bash
:~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc/project-spec$ cat meta-user/recipes-bsp/device-tree/files/system-user.dtsi
#include "system-conf.dtsi"

/* Define configuration */
#define CONFIG_VCU
#define CONFIG_APM

/* Includes */
#include "design-fixes.dtsi"
```

- ./meta-user/recipes-bsp/device-tree/files/vcu.dtsi
	- Copy the system-user.dtsi to a backup file called vcu.dtsi (The TRD has backup device tree definitions like this for each configuration)
```bash
:~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc/project-spec$ cp meta-user/recipes-bsp/device-tree/files/system-user.dtsi meta-user/recipes-bsp/device-tree/files/vcu.dtsi
```

- ./meta-user/recipes-bsp/device-tree/files/zynqmp-qemu-arm.dts
	- Not present in TRD

- ./meta-user/recipes-bsp/device-tree/device-tree.bbappend
	- Modify this to incorporate the new device tree includes from the vcu_hdmirx_nohdmi design
```bash
:~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc/project-spec$ cat meta-user/recipes-bsp/device-tree/device-tree.bbappend
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "\
			file://system-conf.dtsi \
		    file://system-user.dtsi \
		    file://apm.dtsi \
		    file://design-fixes.dtsi \
		    file://xlnk.dtsi \
"
```

- ./meta-user/recipes-bsp/u-boot/u-boot-xlnx_%.bbappend
	- Leave as is (don't bring in the file://bsp.cfg file from the vcu_hdmirx_nohdmi build configuration)

- ./meta-user/recipes-bsp/u-boot/files/platform-top.h
	- Leave as is (don't bring over any differences from the vcu_hdmirx_nohdmi build configuration)

- ./meta-user/recipes-core/base-files/*
	- Copy the following core recipe folder from vcu_hdmirx_nohdmi/project-spec/meta-user/recipes-core/ to the new build configuration
		- base-files
```bash
:~/avnet-uzev-ports/temp$ cp -r vcu_hdmirx_nohdmi/project-spec/meta-user/recipes-core/base-files uz_petalinux_v2018_3_vcu_uz7ev_cc/project-spec/meta-user/recipes-core/
```

- ./meta-user/recipes-core/images/petalinux-image-full.bbappend
	- Copy from vcu_hdmirx_nohdmi/project-spec/meta-user/recipes-core/images/petalinux-image.bbappend to create:
```bash
:~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc/project-spec$ cat meta-user/recipes-core/images/petalinux-image-full.bbappend
#Note: Mention Each package in individual line
#      cascaded representation with line breaks are not valid in this file.
IMAGE_INSTALL_append = " peekpoke"
IMAGE_INSTALL_append = " gpio-demo"
IMAGE_INSTALL_append = " blinky"
IMAGE_INSTALL_append = " blinky-init"
IMAGE_INSTALL_append = " iperf3"
IMAGE_INSTALL_append = " bonnie++"

IMAGE_INSTALL_append = " user-led-test"
IMAGE_INSTALL_append = " user-switch-test"

IMAGE_INSTALL_append = " mac-eeprom-config"
IMAGE_INSTALL_append = " mac-eeprom-config-init"

IMAGE_INSTALL_append = " gstreamer-vcu-examples"
IMAGE_INSTALL_append = " packagegroup-petalinux-v4lutils"
IMAGE_INSTALL_append = " packagegroup-petalinux-audio"
IMAGE_INSTALL_append = " trd-init"
IMAGE_INSTALL_append = " gst-shark"
IMAGE_INSTALL_append = " sds_lib"
```

- ./meta-user/recipes-graphics
	- Copy the entire recipes-graphics folder from vcu_hdmirx_nohdmi/project-spec/meta-user/recipes-graphics to the new build configuration
```bash
:~/avnet-uzev-ports/temp$ cp -r vcu_hdmirx_nohdmi/project-spec/meta-user/recipes-graphics uz_petalinux_v2018_3_vcu_uz7ev_cc/project-spec/meta-user/
```

- ./meta-user/recipes-kernel/linux/linux-xlnx_%.bbappend
	- Copy this from vcu_hdmirx_nohdmi/project-spec/meta-user/recipes-kernel/linux to the new build configuration
	- Modify this to only enable the relevant patches
		- Note: 0004-drm_atomic_helper-Suppress-vlank-timeout-warning-mes.patch is the only one useful
		- The other patches apply to HDMI or video mixer IP which is not used in this VCU only design
```bash
:~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc/project-spec$ cat meta-user/recipes-kernel/linux/linux-xlnx_%.bbappend
SRC_URI += "file://user.cfg"

SRC_URI_append = " \
				file://0004-drm_atomic_helper-Supress-vblank-timeout-warning-mes.patch \
"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"
```

- ./meta-user/recipes-kernel/linux/files/0004-drm_atomic_helper-Suppress-vblank-timeout-warning-mes.patch
	- Copy this file from vcu_hdmirx_nohdmi/project-spec/meta-user/recipes-kernel/linux/files to the new build configuration
```bash
:~/avnet-uzev-ports/temp$ cp -r vcu_hdmirx_nohdmi/project-spec/meta-user/recipes-kernel/linux/files/0004-drm_atomic_helper-Suppress-vblank-timeout-warning-mes.patch uz_petalinux_v2018_3_vcu_uz7ev_cc/project-spec/meta-user/recipes-kernel/linux/files
```

- ./meta-user/recipes-kernel/linux/files/user.cfg
	- Copy this file from vcu_hdmirx_nohdmi/project-spec/meta-user/recipes-kernel/linux/files to the new build configuration
	- Modify this to only enable the following configuration items
```bash
:~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc/project-spec$ cat meta-user/recipes-kernel/linux/files/user.cfg
# Configurations for the VCU Only Design
CONFIG_XILINX_APF=y
CONFIG_XILINX_DMA_APF=y
```

- ./project-spec/configs/config
	- Configure the Petalinux Subsystem
	- Update the following configurations
```
	- CONFIG_SUBSYSTEM_HOSTNAME="vcu_uz7ev_cc"
		: Firmware Version Configuration : (vcu_uz7ev_cc) Host Name

	- CONFIG_SUBSYSTEM_PRODUCT="vcu_uz7ev_cc"
		: Firmware Version Configuration : (vcu_uz7ev_cc) Product Name

	- CONFIG_SUBSYSTEM_FW_VERSION="1.00"
		: Firmware Version Configuration : (1.00) Firmware Version

	- CONFIG_YOCTO_ENABLE_DEBUG_TWEAKS=y
		: Yocto Settings : [*] Enable Debug Tweaks
```
- Launch the petalinux configuration and select the above configuration parameters
```bash
:~/avnet-uzev-ports/temp$ cd uz_petalinux_v2018_3
:~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc$ petalinux-config
[INFO] generating Kconfig for project
[INFO] menuconfig project

<!-- KCONFIG MENU LAUNCHES HERE -->

*** End of the configuration.
*** Execute 'make' to start the build or try 'make help'.

[INFO] sourcing bitbake
[INFO] generating plnxtool conf
[INFO] generating meta-plnx-generated layer
[INFO] generating machine configuration
[INFO] generating bbappends for project . This may take time ! 
[INFO] generating u-boot configuration files
[INFO] generating kernel configuration files
[INFO] generating kconfig for Rootfs
[INFO] oldconfig rootfs
[INFO] generating petalinux-user-image.bb
[INFO] successfully configured project
```

- ./project-spec/configs/rootfs.config
	- Configure the rootfs in Petalinux
	- Add the following configurations to the rootfs
```
	- CONFIG_gdb=y
		: Filesystem Packages : misc : gdb : [*] gdb

	- CONFIG_gdbserver=y
		: Filesystem Packages : misc : gdb : [*] gdbserver

	- CONFIG_packagegroup-core-x11=y
		: Filesystem Packages : misc : packagegroup-core-x11 : [*] packagegroup-core-x11

	- CONFIG_packagegroup-core-x11-dev=y
		: Filesystem Packages : misc : packagegroup-core-x11 : [*] packagegroup-core-x11-dev

	- CONFIG_v4l-utils=y
		: Filesystem Packages : misc : v4l-utils : [*] v4l-utils

	- CONFIG_packagegroup-petalinux-audio=y
		: Petalinux Package Groups : packagegroup-petalinux-audio : [*] packagegroup-petalinux-audio

	- CONFIG_packagegroup-petalinux-gstreamer=y
		: Petalinux Package Groups : packagegroup-petalinux-gstreamer : [*] packagegroup-petalinux-gstreamer

	- CONFIG_packagegroup-petalinux-matchbox=y
		: Petalinux Package Groups : packagegroup-petalinux-matchbox : [*] packagegroup-petalinux-matchbox		

	- CONFIG_packagegroup-petalinux-qt=y
		: Petalinux Package Groups : packagegroup-petalinux-qt : [*] packagegroup-petalinux-qt		

	- CONFIG_inherit-populate-sdk-qt5=y
		: Petalinux Package Groups : packagegroup-petalinux-qt : [*] populate_sdk_qt5	

	- CONFIG_packagegroup-petalinux-v4lutils=y
		: Petalinux Package Groups : packagegroup-petalinux-v4lutils : [*] packagegroup-petalinux-v4lutils		

	- CONFIG_gst-shark=y
		: user packages : [*] gst-shark (NEW)

	- CONFIG_gstreamer-vcu-examples=y
		: user packages : [*] gstreamer-vcu-examples (NEW)

	- CONFIG_sds_lib=y
		: user packages : [*] sds_lib (NEW)

```

- Launch the rootfs menu configuration and select the above configuration parameters
```bash
:~/avnet-uzev-ports/temp$ cd uz_petalinux_v2018_3
:~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc$ petalinux-config -c rootfs
[INFO] sourcing bitbake
[INFO] generating plnxtool conf
[INFO] generating meta-plnx-generated layer
[INFO] generating machine configuration
[INFO] configuring: rootfs
[INFO] generating kconfig for Rootfs
[INFO] menuconfig rootfs

<!-- KCONFIG MENU LAUNCHES HERE -->

configuration written to ~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc/project-spec/configs/rootfs_config

*** End of the configuration.
*** Execute 'make' to start the build or try 'make help'.

[INFO] generating petalinux-user-image.bb
[INFO] successfully configured rootfs
```

### Execute the build
```bash
:~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc$ petalinux-build
[INFO] building project
[INFO] sourcing bitbake
INFO: bitbake petalinux-user-image
Loading cache: 100% |#############################################################################################################| Time: 0:00:00
Loaded 3468 entries from dependency cache.
Parsing recipes: 100% |###########################################################################################################| Time: 0:00:04
Parsing of 2577 .bb files complete (2542 cached, 35 parsed). 3469 targets, 137 skipped, 0 masked, 0 errors.
NOTE: Resolving any missing task queue dependencies
Initialising tasks: 100% |########################################################################################################| Time: 0:00:28
Checking sstate mirror object availability: 100% |################################################################################| Time: 0:00:13
NOTE: Executing SetScene Tasks
NOTE: Executing RunQueue Tasks
WARNING: petalinux-user-image-1.0-r0 do_rootfs: [log_check] petalinux-user-image: found 2 warning messages in the logfile:
[log_check] warning: %post(xserver-nodm-init-3.0-r31.plnx_zynqmp) scriptlet failed, exit status 1
[log_check] warning: %post(sysvinit-inittab-2.88dsf-r10.plnx_zynqmp) scriptlet failed, exit status 1

NOTE: Tasks Summary: Attempted 7726 tasks of which 6524 didn't need to be rerun and all succeeded.

Summary: There was 1 WARNING message shown.
INFO: Copying Images from deploy to images
INFO: Creating images/linux directory
NOTE: Successfully copied built images to tftp dir:  /tftpboot
[INFO] successfully built project
```

### De-compile the resulting device tree and verify that the VCU is instantiated
```bash
:~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc$ dtc -I dtb -O dts -o images/linux/system_decompiled.dts images/linux/system.dtb
:~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc$ cat images/linux/system_decompiled.dts
...
		vcu@a0100000 {
			#address-cells = <0x2>;
			#clock-cells = <0x1>;
			#size-cells = <0x2>;
			clock-names = "pll_ref", "aclk", "vcu_core_enc", "vcu_core_dec", "vcu_mcu_enc", "vcu_mcu_dec";
			clocks = <0x36 0x3 0x47 0x37 0x1 0x37 0x2 0x37 0x3 0x37 0x4>;
			compatible = "xlnx,vcu-1.2", "xlnx,vcu";
			interrupt-names = "vcu_host_interrupt";
			interrupt-parent = <0x4>;
			interrupts = <0x0 0x60 0x4>;
			ranges;
			reg = <0x0 0xa0140000 0x0 0x1000 0x0 0xa0141000 0x0 0x1000>;
			reg-names = "vcu_slcr", "logicore";
			reset-gpios = <0x38 0x77 0x0>;
			linux,phandle = <0x37>;
			phandle = <0x37>;
...
```

### Package the FSBL, FPGA design into a BOOT.bin image
```bash
:~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc$ petalinux-package --boot --format BIN --fsbl images/linux/zynqmp_fsbl.elf --u-boot images/linux/u-boot.elf --pmufw images/linux/pmufw.elf --fpga images/linux/system.bit --force
INFO: File in BOOT BIN: "~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc/images/linux/zynqmp_fsbl.elf"
INFO: File in BOOT BIN: "~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc/images/linux/pmufw.elf"
INFO: File in BOOT BIN: "~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc/images/linux/system.bit"
INFO: File in BOOT BIN: "~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc/images/linux/bl31.elf"
INFO: File in BOOT BIN: "~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc/images/linux/u-boot.elf"
INFO: Generating ZynqMP binary package BOOT.BIN...


****** Xilinx Bootgen v2018.3
  **** Build date : Nov 15 2018-19:22:29
    ** Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.

INFO: Binary is ready.

```

### Test Booting the new images from SD Card (BOOT.BIN, image.ub)
```bash
Xilinx Zynq MP First Stage Boot Loader 
Release 2018.3   Jun 14 2019  -  11:17:21
NOTICE:  ATF running on XCZU7EV/silicon v4/RTL5.1 at 0xfffea000
NOTICE:  BL31: Secure code at 0x0
NOTICE:  BL31: Non secure code at 0x8000000
NOTICE:  BL31: v1.5(release):xilinx-v2018.2-919-g08560c36
NOTICE:  BL31: Built : 15:47:58, Jun 14 2019
PMUFW:  v1.1


U-Boot 2018.01 (Jun 14 2019 - 15:48:31 +0000) Xilinx ZynqMP ZCU102 rev1.0

I2C:   ready
DRAM:  4 GiB
EL Level:       EL2
Chip ID:        zu7ev
MMC:   mmc@ff160000: 0 (eMMC), mmc@ff170000: 1 (SD)
SF: Detected n25q256a with page size 512 Bytes, erase size 128 KiB, total 64 MiB
*** Warning - bad CRC, using default environment

In:    serial@ff000000
Out:   serial@ff000000
Err:   serial@ff000000
Board: Xilinx ZynqMP
Bootmode: LVL_SHFT_SD_MODE1
Net:   ZYNQ GEM: ff0e0000, phyaddr 0, interface rgmii-id
eth0: ethernet@ff0e0000
Hit any key to stop autoboot:  0 
Device: mmc@ff160000
Manufacturer ID: 13
OEM: 14e
Name: Q2J55 
Tran Speed: 200000000
Rd Block Len: 512
MMC version 5.0
High Capacity: Yes
Capacity: 7.1 GiB
Bus Width: 4-bit
Erase Group Size: 512 KiB
HC WP Group Size: 8 MiB
User Capacity: 7.1 GiB WRREL
Boot Capacity: 16 MiB ENH
RPMB Capacity: 4 MiB ENH
reading image.ub
153073164 bytes read in 12836 ms (11.4 MiB/s)
## Loading kernel from FIT Image at 10000000 ...
   Using 'conf@system-top.dtb' configuration
   Trying 'kernel@1' kernel subimage
     Description:  Linux kernel
     Type:         Kernel Image
     Compression:  gzip compressed
     Data Start:   0x10000104
     Data Size:    7098813 Bytes = 6.8 MiB
     Architecture: AArch64
     OS:           Linux
     Load Address: 0x00080000
     Entry Point:  0x00080000
     Hash algo:    sha1
     Hash value:   c82f27224472f6dce2c906f88f71e098c0b958e4
   Verifying Hash Integrity ... sha1+ OK
## Loading ramdisk from FIT Image at 10000000 ...
   Using 'conf@system-top.dtb' configuration
   Trying 'ramdisk@1' ramdisk subimage
     Description:  petalinux-user-image
     Type:         RAMDisk Image
     Compression:  gzip compressed
     Data Start:   0x106cdfb0
     Data Size:    145936586 Bytes = 139.2 MiB
     Architecture: AArch64
     OS:           Linux
     Load Address: unavailable
     Entry Point:  unavailable
     Hash algo:    sha1
     Hash value:   4b3b54e78424af84e334f9bf2350ee6d89f22c41
   Verifying Hash Integrity ... sha1+ OK
## Loading fdt from FIT Image at 10000000 ...
   Using 'conf@system-top.dtb' configuration
   Trying 'fdt@system-top.dtb' fdt subimage
     Description:  Flattened Device Tree blob
     Type:         Flat Device Tree
     Compression:  uncompressed
     Data Start:   0x106c53c8
     Data Size:    35618 Bytes = 34.8 KiB
     Architecture: AArch64
     Hash algo:    sha1
     Hash value:   8394df87b5a5749c822574eb4571a0e46bcc536a
   Verifying Hash Integrity ... sha1+ OK
   Booting using the fdt blob at 0x106c53c8
   Uncompressing Kernel Image ... OK
   Loading Ramdisk to 7526b000, end 7dd980ca ... OK
   Loading Device Tree to 000000007525f000, end 000000007526ab21 ... OK

Starting kernel ...

[    0.000000] Booting Linux on physical CPU 0x0
[    0.000000] Linux version 4.14.0-xilinx-v2018.3 (oe-user@oe-host) (gcc version 7.3.0 (GCC)) #1 SMP Fri Jun 14 11:18:57 UTC 2019
[    0.000000] Boot CPU: AArch64 Processor [410fd034]
[    0.000000] Machine model: xlnx,zynqmp
[    0.000000] earlycon: cdns0 at MMIO 0x00000000ff000000 (options '115200n8')
[    0.000000] bootconsole [cdns0] enabled
[    0.000000] efi: Getting EFI parameters from FDT:
[    0.000000] efi: UEFI not found.
[    0.000000] cma: Reserved 1700 MiB at 0x000000000ac00000
[    0.000000] psci: probing for conduit method from DT.
[    0.000000] psci: PSCIv1.1 detected in firmware.
[    0.000000] psci: Using standard PSCI v0.2 function IDs
[    0.000000] psci: MIGRATE_INFO_TYPE not supported.
[    0.000000] random: fast init done
[    0.000000] percpu: Embedded 21 pages/cpu @ffffffc87ff72000 s46488 r8192 d31336 u86016
[    0.000000] Detected VIPT I-cache on CPU0
[    0.000000] CPU features: enabling workaround for ARM erratum 845719
[    0.000000] Built 1 zonelists, mobility grouping on.  Total pages: 1033987
[    0.000000] Kernel command line: earlycon clk_ignore_unused consoleblank=0 cma=1700M uio_pdrv_genirq.of_id=generic-uio
[    0.000000] PID hash table entries: 4096 (order: 3, 32768 bytes)
[    0.000000] Dentry cache hash table entries: 524288 (order: 10, 4194304 bytes)
[    0.000000] Inode-cache hash table entries: 262144 (order: 9, 2097152 bytes)
[    0.000000] software IO TLB [mem 0x06c00000-0x0ac00000] (64MB) mapped at [ffffffc006c00000-ffffffc00abfffff]
[    0.000000] Memory: 2162884K/4193280K available (10172K kernel code, 664K rwdata, 3228K rodata, 512K init, 2362K bss, 289596K reserved, 17)
[    0.000000] Virtual kernel memory layout:
[    0.000000]     modules : 0xffffff8000000000 - 0xffffff8008000000   (   128 MB)
[    0.000000]     vmalloc : 0xffffff8008000000 - 0xffffffbebfff0000   (   250 GB)
[    0.000000]       .text : 0xffffff8008080000 - 0xffffff8008a70000   ( 10176 KB)
[    0.000000]     .rodata : 0xffffff8008a70000 - 0xffffff8008da0000   (  3264 KB)
[    0.000000]       .init : 0xffffff8008da0000 - 0xffffff8008e20000   (   512 KB)
[    0.000000]       .data : 0xffffff8008e20000 - 0xffffff8008ec6200   (   665 KB)
[    0.000000]        .bss : 0xffffff8008ec6200 - 0xffffff8009114d30   (  2363 KB)
[    0.000000]     fixed   : 0xffffffbefe7fd000 - 0xffffffbefec00000   (  4108 KB)
[    0.000000]     PCI I/O : 0xffffffbefee00000 - 0xffffffbeffe00000   (    16 MB)
[    0.000000]     vmemmap : 0xffffffbf00000000 - 0xffffffc000000000   (     4 GB maximum)
[    0.000000]               0xffffffbf00000000 - 0xffffffbf1dc00000   (   476 MB actual)
[    0.000000]     memory  : 0xffffffc000000000 - 0xffffffc880000000   ( 34816 MB)
[    0.000000] Hierarchical RCU implementation.
[    0.000000]  RCU event tracing is enabled.
[    0.000000]  RCU restricting CPUs from NR_CPUS=8 to nr_cpu_ids=4.
[    0.000000] RCU: Adjusting geometry for rcu_fanout_leaf=16, nr_cpu_ids=4
[    0.000000] NR_IRQS: 64, nr_irqs: 64, preallocated irqs: 0
[    0.000000] GIC: Adjusting CPU interface base to 0x00000000f902f000
[    0.000000] GIC: Using split EOI/Deactivate mode
[    0.000000] arch_timer: cp15 timer(s) running at 99.99MHz (phys).
[    0.000000] clocksource: arch_sys_counter: mask: 0xffffffffffffff max_cycles: 0x171015c90f, max_idle_ns: 440795203080 ns
[    0.000003] sched_clock: 56 bits at 99MHz, resolution 10ns, wraps every 4398046511101ns
[    0.008292] Console: colour dummy device 80x25
[    0.012377] console [tty0] enabled
[    0.015743] bootconsole [cdns0] disabled
[    0.000000] Booting Linux on physical CPU 0x0
[    0.000000] Linux version 4.14.0-xilinx-v2018.3 (oe-user@oe-host) (gcc version 7.3.0 (GCC)) #1 SMP Fri Jun 14 11:18:57 UTC 2019
[    0.000000] Boot CPU: AArch64 Processor [410fd034]
[    0.000000] Machine model: xlnx,zynqmp
[    0.000000] earlycon: cdns0 at MMIO 0x00000000ff000000 (options '115200n8')
[    0.000000] bootconsole [cdns0] enabled
[    0.000000] efi: Getting EFI parameters from FDT:
[    0.000000] efi: UEFI not found.
[    0.000000] cma: Reserved 1700 MiB at 0x000000000ac00000
[    0.000000] psci: probing for conduit method from DT.
[    0.000000] psci: PSCIv1.1 detected in firmware.
[    0.000000] psci: Using standard PSCI v0.2 function IDs
[    0.000000] psci: MIGRATE_INFO_TYPE not supported.
[    0.000000] random: fast init done
[    0.000000] percpu: Embedded 21 pages/cpu @ffffffc87ff72000 s46488 r8192 d31336 u86016
[    0.000000] Detected VIPT I-cache on CPU0
[    0.000000] CPU features: enabling workaround for ARM erratum 845719
[    0.000000] Built 1 zonelists, mobility grouping on.  Total pages: 1033987
[    0.000000] Kernel command line: earlycon clk_ignore_unused consoleblank=0 cma=1700M uio_pdrv_genirq.of_id=generic-uio
[    0.000000] PID hash table entries: 4096 (order: 3, 32768 bytes)
[    0.000000] Dentry cache hash table entries: 524288 (order: 10, 4194304 bytes)
[    0.000000] Inode-cache hash table entries: 262144 (order: 9, 2097152 bytes)
[    0.000000] software IO TLB [mem 0x06c00000-0x0ac00000] (64MB) mapped at [ffffffc006c00000-ffffffc00abfffff]
[    0.000000] Memory: 2162884K/4193280K available (10172K kernel code, 664K rwdata, 3228K rodata, 512K init, 2362K bss, 289596K reserved, 17)
[    0.000000] Virtual kernel memory layout:
[    0.000000]     modules : 0xffffff8000000000 - 0xffffff8008000000   (   128 MB)
[    0.000000]     vmalloc : 0xffffff8008000000 - 0xffffffbebfff0000   (   250 GB)
[    0.000000]       .text : 0xffffff8008080000 - 0xffffff8008a70000   ( 10176 KB)
[    0.000000]     .rodata : 0xffffff8008a70000 - 0xffffff8008da0000   (  3264 KB)
[    0.000000]       .init : 0xffffff8008da0000 - 0xffffff8008e20000   (   512 KB)
[    0.000000]       .data : 0xffffff8008e20000 - 0xffffff8008ec6200   (   665 KB)
[    0.000000]        .bss : 0xffffff8008ec6200 - 0xffffff8009114d30   (  2363 KB)
[    0.000000]     fixed   : 0xffffffbefe7fd000 - 0xffffffbefec00000   (  4108 KB)
[    0.000000]     PCI I/O : 0xffffffbefee00000 - 0xffffffbeffe00000   (    16 MB)
[    0.000000]     vmemmap : 0xffffffbf00000000 - 0xffffffc000000000   (     4 GB maximum)
[    0.000000]               0xffffffbf00000000 - 0xffffffbf1dc00000   (   476 MB actual)
[    0.000000]     memory  : 0xffffffc000000000 - 0xffffffc880000000   ( 34816 MB)
[    0.000000] Hierarchical RCU implementation.
[    0.000000]  RCU event tracing is enabled.
[    0.000000]  RCU restricting CPUs from NR_CPUS=8 to nr_cpu_ids=4.
[    0.000000] RCU: Adjusting geometry for rcu_fanout_leaf=16, nr_cpu_ids=4
[    0.000000] NR_IRQS: 64, nr_irqs: 64, preallocated irqs: 0
[    0.000000] GIC: Adjusting CPU interface base to 0x00000000f902f000
[    0.000000] GIC: Using split EOI/Deactivate mode
[    0.000000] arch_timer: cp15 timer(s) running at 99.99MHz (phys).
[    0.000000] clocksource: arch_sys_counter: mask: 0xffffffffffffff max_cycles: 0x171015c90f, max_idle_ns: 440795203080 ns
[    0.000003] sched_clock: 56 bits at 99MHz, resolution 10ns, wraps every 4398046511101ns
[    0.008292] Console: colour dummy device 80x25
[    0.012377] console [tty0] enabled
[    0.015743] bootconsole [cdns0] disabled
[    0.019652] Calibrating delay loop (skipped), value calculated using timer frequency.. 199.99 BogoMIPS (lpj=399996)
[    0.019666] pid_max: default: 32768 minimum: 301
[    0.019775] Mount-cache hash table entries: 8192 (order: 4, 65536 bytes)
[    0.019793] Mountpoint-cache hash table entries: 8192 (order: 4, 65536 bytes)
[    0.020414] ASID allocator initialised with 65536 entries
[    0.020470] Hierarchical SRCU implementation.
[    0.020792] EFI services will not be available.
[    0.020817] zynqmp_plat_init Platform Management API v1.1
[    0.020826] zynqmp_plat_init Trustzone version v1.0
[    0.020936] smp: Bringing up secondary CPUs ...
[    0.021195] Detected VIPT I-cache on CPU1
[    0.021223] CPU1: Booted secondary processor [410fd034]
[    0.021506] Detected VIPT I-cache on CPU2
[    0.021525] CPU2: Booted secondary processor [410fd034]
[    0.021797] Detected VIPT I-cache on CPU3
[    0.021816] CPU3: Booted secondary processor [410fd034]
[    0.021861] smp: Brought up 1 node, 4 CPUs
[    0.021896] SMP: Total of 4 processors activated.
[    0.021904] CPU features: detected feature: 32-bit EL0 Support
[    0.021916] CPU: All CPU(s) started at EL2
[    0.021932] alternatives: patching kernel code
[    0.022651] devtmpfs: initialized
[    0.026556] clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 7645041785100000 ns
[    0.026580] futex hash table entries: 1024 (order: 5, 131072 bytes)
[    0.064541] xor: measuring software checksum speed
[    0.103840]    8regs     :  2111.000 MB/sec
[    0.143873]    8regs_prefetch:  1882.000 MB/sec
[    0.183903]    32regs    :  2594.000 MB/sec
[    0.223935]    32regs_prefetch:  2180.000 MB/sec
[    0.223944] xor: using function: 32regs (2594.000 MB/sec)
[    0.224018] pinctrl core: initialized pinctrl subsystem
[    0.224549] NET: Registered protocol family 16
[    0.225378] cpuidle: using governor menu
[    0.225836] vdso: 2 pages (1 code @ ffffff8008a76000, 1 data @ ffffff8008e24000)
[    0.225853] hw-breakpoint: found 6 breakpoint and 4 watchpoint registers.
[    0.226353] DMA: preallocated 256 KiB pool for atomic allocations
[    0.249022] reset_zynqmp reset-controller: Xilinx zynqmp reset driver probed
[    0.249610] ARM CCI_400_r1 PMU driver probed
[    0.261766] HugeTLB registered 2.00 MiB page size, pre-allocated 0 pages
[    0.328151] raid6: int64x1  gen()   368 MB/s
[    0.396111] raid6: int64x1  xor()   408 MB/s
[    0.464197] raid6: int64x2  gen()   630 MB/s
[    0.532231] raid6: int64x2  xor()   552 MB/s
[    0.600292] raid6: int64x4  gen()   956 MB/s
[    0.668358] raid6: int64x4  xor()   680 MB/s
[    0.736375] raid6: int64x8  gen()   898 MB/s
[    0.804464] raid6: int64x8  xor()   682 MB/s
[    0.872528] raid6: neonx1   gen()   666 MB/s
[    0.940531] raid6: neonx1   xor()   781 MB/s
[    1.008604] raid6: neonx2   gen()  1071 MB/s
[    1.076631] raid6: neonx2   xor()  1102 MB/s
[    1.144689] raid6: neonx4   gen()  1377 MB/s
[    1.212755] raid6: neonx4   xor()  1318 MB/s
[    1.280806] raid6: neonx8   gen()  1511 MB/s
[    1.348858] raid6: neonx8   xor()  1398 MB/s
[    1.348866] raid6: using algorithm neonx8 gen() 1511 MB/s
[    1.348874] raid6: .... xor() 1398 MB/s, rmw enabled
[    1.348882] raid6: using neon recovery algorithm
[    1.349488] XGpio: /amba_pl@0/gpio@a0000000: registered, base is 504
[    1.349816] XGpio: /amba_pl@0/gpio@a0001000: registered, base is 496
[    1.350054] XGpio: /amba_pl@0/gpio@a0002000: registered, base is 493
[    1.350883] SCSI subsystem initialized
[    1.351061] usbcore: registered new interface driver usbfs
[    1.351100] usbcore: registered new interface driver hub
[    1.351141] usbcore: registered new device driver usb
[    1.351208] media: Linux media interface: v0.10
[    1.351236] Linux video capture interface: v2.00
[    1.351277] pps_core: LinuxPPS API ver. 1 registered
[    1.351286] pps_core: Software ver. 5.3.6 - Copyright 2005-2007 Rodolfo Giometti <giometti@linux.it>
[    1.351308] PTP clock support registered
[    1.351339] EDAC MC: Ver: 3.0.0
[    1.351482] PLL: shutdown
[    1.351503] zynqmp_pll_disable() clock disable failed for iopll_int, ret = -13
[    1.352270] zynqmp-ipi ff9905c0.mailbox: Probed ZynqMP IPI Mailbox driver.
[    1.352456] FPGA manager framework
[    1.352573] fpga-region fpga-full: FPGA Region probed
[    1.352677] Advanced Linux Sound Architecture Driver Initialized.
[    1.352928] Bluetooth: Core ver 2.22
[    1.352959] NET: Registered protocol family 31
[    1.352968] Bluetooth: HCI device and connection manager initialized
[    1.352981] Bluetooth: HCI socket layer initialized
[    1.352992] Bluetooth: L2CAP socket layer initialized
[    1.353011] Bluetooth: SCO socket layer initialized
[    1.353690] clocksource: Switched to clocksource arch_sys_counter
[    1.353769] VFS: Disk quotas dquot_6.6.0
[    1.353819] VFS: Dquot-cache hash table entries: 512 (order 0, 4096 bytes)
[    1.358281] NET: Registered protocol family 2
[    1.358595] TCP established hash table entries: 32768 (order: 6, 262144 bytes)
[    1.358823] TCP bind hash table entries: 32768 (order: 7, 524288 bytes)
[    1.359279] TCP: Hash tables configured (established 32768 bind 32768)
[    1.359350] UDP hash table entries: 2048 (order: 4, 65536 bytes)
[    1.359433] UDP-Lite hash table entries: 2048 (order: 4, 65536 bytes)
[    1.359590] NET: Registered protocol family 1
[    1.359806] RPC: Registered named UNIX socket transport module.
[    1.359816] RPC: Registered udp transport module.
[    1.359824] RPC: Registered tcp transport module.
[    1.359832] RPC: Registered tcp NFSv4.1 backchannel transport module.
[    1.359934] Trying to unpack rootfs image as initramfs...
[    7.744189] Freeing initrd memory: 142516K
[    7.744609] hw perfevents: no interrupt-affinity property for /pmu, guessing.
[    7.744783] hw perfevents: enabled with armv8_pmuv3 PMU driver, 7 counters available
[    7.745523] audit: initializing netlink subsys (disabled)
[    7.745616] audit: type=2000 audit(7.732:1): state=initialized audit_enabled=0 res=1
[    7.745970] workingset: timestamp_bits=62 max_order=20 bucket_order=0
[    7.746682] NFS: Registering the id_resolver key type
[    7.746705] Key type id_resolver registered
[    7.746713] Key type id_legacy registered
[    7.746726] nfs4filelayout_init: NFSv4 File Layout Driver Registering...
[    7.746748] jffs2: version 2.2. (NAND) �© 2001-2006 Red Hat, Inc.
[    7.775491] Block layer SCSI generic (bsg) driver version 0.4 loaded (major 247)
[    7.775512] io scheduler noop registered
[    7.775520] io scheduler deadline registered
[    7.775543] io scheduler cfq registered (default)
[    7.775552] io scheduler mq-deadline registered
[    7.775560] io scheduler kyber registered
[    7.776212] nwl-pcie fd0e0000.pcie: Link is DOWN
[    7.776260] OF: PCI: host bridge /amba/pcie@fd0e0000 ranges:
[    7.776282] OF: PCI:   MEM 0xe0000000..0xefffffff -> 0xe0000000
[    7.776296] OF: PCI:   MEM 0x600000000..0x7ffffffff -> 0x600000000
[    7.776431] nwl-pcie fd0e0000.pcie: PCI host bridge to bus 0000:00
[    7.776446] pci_bus 0000:00: root bus resource [bus 00-ff]
[    7.776458] pci_bus 0000:00: root bus resource [mem 0xe0000000-0xefffffff]
[    7.776469] pci_bus 0000:00: root bus resource [mem 0x600000000-0x7ffffffff pref]
[    7.776746] pci 0000:00:00.0: PCI bridge to [bus 01-0c]
[    7.777665] xilinx-dpdma fd4c0000.dma: Xilinx DPDMA engine is probed
[    7.778049] xilinx-zynqmp-dma fd500000.dma: ZynqMP DMA driver Probe success
[    7.778207] xilinx-zynqmp-dma fd510000.dma: ZynqMP DMA driver Probe success
[    7.778363] xilinx-zynqmp-dma fd520000.dma: ZynqMP DMA driver Probe success
[    7.778517] xilinx-zynqmp-dma fd530000.dma: ZynqMP DMA driver Probe success
[    7.778672] xilinx-zynqmp-dma fd540000.dma: ZynqMP DMA driver Probe success
[    7.778828] xilinx-zynqmp-dma fd550000.dma: ZynqMP DMA driver Probe success
[    7.778992] xilinx-zynqmp-dma fd560000.dma: ZynqMP DMA driver Probe success
[    7.779148] xilinx-zynqmp-dma fd570000.dma: ZynqMP DMA driver Probe success
[    7.779359] xilinx-zynqmp-dma ffa80000.dma: ZynqMP DMA driver Probe success
[    7.779511] xilinx-zynqmp-dma ffa90000.dma: ZynqMP DMA driver Probe success
[    7.779669] xilinx-zynqmp-dma ffaa0000.dma: ZynqMP DMA driver Probe success
[    7.779824] xilinx-zynqmp-dma ffab0000.dma: ZynqMP DMA driver Probe success
[    7.779977] xilinx-zynqmp-dma ffac0000.dma: ZynqMP DMA driver Probe success
[    7.780140] xilinx-zynqmp-dma ffad0000.dma: ZynqMP DMA driver Probe success
[    7.780297] xilinx-zynqmp-dma ffae0000.dma: ZynqMP DMA driver Probe success
[    7.780457] xilinx-zynqmp-dma ffaf0000.dma: ZynqMP DMA driver Probe success
[    7.807043] Serial: 8250/16550 driver, 4 ports, IRQ sharing disabled
[    7.810554] cacheinfo: Unable to detect cache hierarchy for CPU 0
[    7.814807] brd: module loaded
[    7.818288] loop: module loaded
[    7.819254] ahci-ceva fd0c0000.ahci: AHCI 0001.0301 32 slots 2 ports 6 Gbps 0x3 impl platform mode
[    7.819273] ahci-ceva fd0c0000.ahci: flags: 64bit ncq sntf pm clo only pmp fbs pio slum part ccc sds apst 
[    7.820137] scsi host0: ahci-ceva
[    7.820346] scsi host1: ahci-ceva
[    7.820459] ata1: SATA max UDMA/133 mmio [mem 0xfd0c0000-0xfd0c1fff] port 0x100 irq 43
[    7.820472] ata2: SATA max UDMA/133 mmio [mem 0xfd0c0000-0xfd0c1fff] port 0x180 irq 43
[    7.820639] mtdoops: mtd device (mtddev=name/number) must be supplied
[    7.821470] m25p80 spi0.0: found n25q256a, expected m25p80
[    7.821723] m25p80 spi0.0: n25q256a (65536 Kbytes)
[    7.823079] libphy: Fixed MDIO Bus: probed
[    7.824153] tun: Universal TUN/TAP device driver, 1.6
[    7.824288] CAN device driver interface
[    7.824919] macb ff0e0000.ethernet: Not enabling partial store and forward
[    7.825357] libphy: MACB_mii_bus: probed
[    7.897767] macb ff0e0000.ethernet eth0: Cadence GEM rev 0x50070106 at 0xff0e0000 irq 30 (00:0a:35:00:22:01)
[    7.897792] Marvell 88E1510 ff0e0000.ethernet-ffffffff:00: attached PHY driver [Marvell 88E1510] (mii_bus:phy_addr=ff0e0000.ethernet-fffff)
[    7.898499] usbcore: registered new interface driver asix
[    7.898550] usbcore: registered new interface driver ax88179_178a
[    7.898580] usbcore: registered new interface driver cdc_ether
[    7.898610] usbcore: registered new interface driver net1080
[    7.898639] usbcore: registered new interface driver cdc_subset
[    7.898669] usbcore: registered new interface driver zaurus
[    7.898710] usbcore: registered new interface driver cdc_ncm
[    7.899250] xilinx-axipmon ffa00000.perf-monitor: Probed Xilinx APM
[    7.899643] ehci_hcd: USB 2.0 'Enhanced' Host Controller (EHCI) Driver
[    7.899654] ehci-pci: EHCI PCI platform driver
[    7.899955] usbcore: registered new interface driver uas
[    7.899993] usbcore: registered new interface driver usb-storage
[    7.900842] rtc_zynqmp ffa60000.rtc: rtc core: registered ffa60000.rtc as rtc0
[    7.900901] i2c /dev entries driver
[    7.901488] cdns-i2c ff030000.i2c: 400 kHz mmio ff030000 irq 32
[    7.959139] i2c i2c-0: Added multiplexed i2c bus 1
[    7.959403] i2c i2c-0: Added multiplexed i2c bus 2
[    7.959416] pca954x 0-0070: registered 2 multiplexed busses for I2C mux pca9542
[    7.959519] IR NEC protocol handler initialized
[    7.959528] IR RC5(x/sz) protocol handler initialized
[    7.959536] IR RC6 protocol handler initialized
[    7.959543] IR JVC protocol handler initialized
[    7.959551] IR Sony protocol handler initialized
[    7.959558] IR SANYO protocol handler initialized
[    7.959565] IR Sharp protocol handler initialized
[    7.959573] IR MCE Keyboard/mouse protocol handler initialized
[    7.959581] IR XMP protocol handler initialized
[    7.961110] usbcore: registered new interface driver uvcvideo
[    7.961121] USB Video Class driver (1.1.1)
[    7.961616] cdns-wdt fd4d0000.watchdog: Xilinx Watchdog Timer at ffffff80091c5000 with timeout 60s
[    7.961801] cdns-wdt ff150000.watchdog: Xilinx Watchdog Timer at ffffff80091cd000 with timeout 10s
[    7.962007] Bluetooth: HCI UART driver ver 2.3
[    7.962018] Bluetooth: HCI UART protocol H4 registered
[    7.962026] Bluetooth: HCI UART protocol BCSP registered
[    7.962053] Bluetooth: HCI UART protocol LL registered
[    7.962062] Bluetooth: HCI UART protocol ATH3K registered
[    7.962070] Bluetooth: HCI UART protocol Three-wire (H5) registered
[    7.962115] Bluetooth: HCI UART protocol Intel registered
[    7.962124] Bluetooth: HCI UART protocol QCA registered
[    7.962163] usbcore: registered new interface driver bcm203x
[    7.962198] usbcore: registered new interface driver bpa10x
[    7.962231] usbcore: registered new interface driver bfusb
[    7.962265] usbcore: registered new interface driver btusb
[    7.962274] Bluetooth: Generic Bluetooth SDIO driver ver 0.1
[    7.962323] usbcore: registered new interface driver ath3k
[    7.962575] EDAC MC: ECC not enabled
[    7.962743] EDAC DEVICE0: Giving out device to module zynqmp-ocm-edac controller zynqmp_ocm: DEV ff960000.memory-controller (INTERRUPT)
[    7.963215] cpufreq: cpufreq_online: CPU0: Running at unlisted freq: 1099999 KHz
[    7.963272] cpufreq: cpufreq_online: CPU0: Unlisted initial frequency changed to: 1199999 KHz
[    7.963662] sdhci: Secure Digital Host Controller Interface driver
[    7.963671] sdhci: Copyright(c) Pierre Ossman
[    7.963679] sdhci-pltfm: SDHCI platform and OF driver helper
[    8.009699] mmc0: SDHCI controller on ff160000.mmc [ff160000.mmc] using ADMA 64-bit
[    8.009748] PLL: shutdown
[    8.009816] PLL: enable
[    8.057695] mmc1: SDHCI controller on ff170000.mmc [ff170000.mmc] using ADMA 64-bit
[    8.063649] ledtrig-cpu: registered to indicate activity on CPUs
[    8.063804] usbcore: registered new interface driver usbhid
[    8.063813] usbhid: USB HID core driver
[    8.066012] fpga_manager fpga0: Xilinx ZynqMP FPGA Manager registered
[    8.066394] usbcore: registered new interface driver snd-usb-audio
[    8.067200] pktgen: Packet Generator for packet performance testing. Version: 2.75
[    8.070675] Netfilter messages via NETLINK v0.30.
[    8.070813] ip_tables: (C) 2000-2006 Netfilter Core Team
[    8.070986] Initializing XFRM netlink socket
[    8.071056] NET: Registered protocol family 10
[    8.071621] Segment Routing with IPv6
[    8.071676] ip6_tables: (C) 2000-2006 Netfilter Core Team
[    8.071852] sit: IPv6, IPv4 and MPLS over IPv4 tunneling driver
[    8.072245] NET: Registered protocol family 17
[    8.072311] NET: Registered protocol family 15
[    8.072330] bridge: filtering via arp/ip/ip6tables is no longer available by default. Update your scripts to load br_netfilter if you need.
[    8.072346] Ebtables v2.0 registered
[    8.074225] can: controller area network core (rev 20170425 abi 9)
[    8.074262] NET: Registered protocol family 29
[    8.074272] can: raw protocol (rev 20170425)
[    8.074280] can: broadcast manager protocol (rev 20170425 t)
[    8.074292] can: netlink gateway (rev 20170425) max_hops=1
[    8.074351] Bluetooth: RFCOMM TTY layer initialized
[    8.074364] Bluetooth: RFCOMM socket layer initialized
[    8.074380] Bluetooth: RFCOMM ver 1.11
[    8.074395] Bluetooth: BNEP (Ethernet Emulation) ver 1.3
[    8.074404] Bluetooth: BNEP filters: protocol multicast
[    8.074414] Bluetooth: BNEP socket layer initialized
[    8.074423] Bluetooth: HIDP (Human Interface Emulation) ver 1.2
[    8.074441] Bluetooth: HIDP socket layer initialized
[    8.074518] mmc0: new HS200 MMC card at address 0001
[    8.074578] 9pnet: Installing 9P2000 support
[    8.074598] Key type dns_resolver registered
[    8.075006] registered taskstats version 1
[    8.075332] Btrfs loaded, crc32c=crc32c-generic
[    8.079849] ff000000.serial: ttyPS0 at MMIO 0xff000000 (irq = 46, base_baud = 6249999) is a xuartps
[    8.080298] mmcblk0: mmc0:0001 Q2J55L 7.09 GiB 
[    8.080535] mmcblk0boot0: mmc0:0001 Q2J55L partition 1 16.0 MiB
[    8.080619] mmcblk0boot1: mmc0:0001 Q2J55L partition 2 16.0 MiB
[    8.080688] mmcblk0rpmb: mmc0:0001 Q2J55L partition 3 4.00 MiB
[    8.095777]  mmcblk0: p1
[    8.129920] ata1: SATA link down (SStatus 0 SControl 330)
[    8.135882] ata2: SATA link down (SStatus 0 SControl 330)
[    8.159273] mmc1: new SDHC card at address e624
[    8.159438] mmcblk1: mmc1:e624 SU08G 7.40 GiB (ro)
[    8.163316]  mmcblk1: p1
[    9.846839] console [ttyPS0] enabled
[    9.850871] ff010000.serial: ttyPS1 at MMIO 0xff010000 (irq = 47, base_baud = 6249999) is a xuartps
[    9.861112] xilinx-psgtr fd400000.zynqmp_phy: Lane:3 type:8 protocol:4 pll_locked:yes
[    9.869297] PLL: shutdown
[    9.873840] zynqmp_clk_divider_set_rate() set divider failed for pl1_ref_div1, ret = -13
[    9.882389] xilinx-dp-snd-codec fd4a0000.zynqmp-display:zynqmp_dp_snd_codec0: Xilinx DisplayPort Sound Codec probed
[    9.893082] xilinx-dp-snd-pcm zynqmp_dp_snd_pcm0: Xilinx DisplayPort Sound PCM probed
[    9.901146] xilinx-dp-snd-pcm zynqmp_dp_snd_pcm1: Xilinx DisplayPort Sound PCM probed
[    9.909531] xilinx-dp-snd-card fd4a0000.zynqmp-display:zynqmp_dp_snd_card: xilinx-dp-snd-codec-dai <-> xilinx-dp-snd-codec-dai mapping ok
[    9.921972] xilinx-dp-snd-card fd4a0000.zynqmp-display:zynqmp_dp_snd_card: xilinx-dp-snd-codec-dai <-> xilinx-dp-snd-codec-dai mapping ok
[    9.934696] xilinx-dp-snd-card fd4a0000.zynqmp-display:zynqmp_dp_snd_card: Xilinx DisplayPort Sound Card probed
[    9.944882] OF: graph: no port node found in /amba/zynqmp-display@fd4a0000
[    9.951872] [drm] Supports vblank timestamp caching Rev 2 (21.10.2013).
[    9.958490] [drm] No driver support for vblank timestamp query.
[    9.964473] xlnx-drm xlnx-drm.0: bound fd4a0000.zynqmp-display (ops 0xffffff8008b27868)
[   10.003201] [drm] Cannot find any crtc or sizes
[   10.007918] [drm] Initialized xlnx 1.0.0 20130509 for fd4a0000.zynqmp-display on minor 0
[   10.016031] zynqmp-display fd4a0000.zynqmp-display: ZynqMP DisplayPort Subsystem driver probed
[   10.025527] xilinx-psgtr fd400000.zynqmp_phy: Lane:2 type:0 protocol:3 pll_locked:yes
[   10.035550] xhci-hcd xhci-hcd.0.auto: xHCI Host Controller
[   10.041049] xhci-hcd xhci-hcd.0.auto: new USB bus registered, assigned bus number 1
[   10.048921] xhci-hcd xhci-hcd.0.auto: hcc params 0x0238f625 hci version 0x100 quirks 0x22010810
[   10.057644] xhci-hcd xhci-hcd.0.auto: irq 73, io mem 0xfe200000
[   10.063673] usb usb1: New USB device found, idVendor=1d6b, idProduct=0002
[   10.070466] usb usb1: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[   10.077695] usb usb1: Product: xHCI Host Controller
[   10.082568] usb usb1: Manufacturer: Linux 4.14.0-xilinx-v2018.3 xhci-hcd
[   10.089269] usb usb1: SerialNumber: xhci-hcd.0.auto
[   10.094418] hub 1-0:1.0: USB hub found
[   10.098190] hub 1-0:1.0: 1 port detected
[   10.102296] xhci-hcd xhci-hcd.0.auto: xHCI Host Controller
[   10.107792] xhci-hcd xhci-hcd.0.auto: new USB bus registered, assigned bus number 2
[   10.115573] usb usb2: New USB device found, idVendor=1d6b, idProduct=0003
[   10.122363] usb usb2: New USB device strings: Mfr=3, Product=2, SerialNumber=1
[   10.129582] usb usb2: Product: xHCI Host Controller
[   10.134457] usb usb2: Manufacturer: Linux 4.14.0-xilinx-v2018.3 xhci-hcd
[   10.141158] usb usb2: SerialNumber: xhci-hcd.0.auto
[   10.146279] hub 2-0:1.0: USB hub found
[   10.150050] hub 2-0:1.0: 1 port detected
[   10.154936] rtc_zynqmp ffa60000.rtc: setting system clock to 1970-01-01 00:00:19 UTC (19)
[   10.163120] of_cfs_init
[   10.165573] of_cfs_init: OK
[   10.168471] clk: Not disabling unused clocks
[   10.172973] ALSA device list:
[   10.175937]   #0: DisplayPort monitor
[   10.179999] Freeing unused kernel memory: 512K
INIT: version 2.88 booting
[   10.273444] FAT-fs (mmcblk0p1): Volume was not properly unmounted. Some data may be corrupt. Please run fsck.
Starting udev
[   10.314622] udevd[1802]: starting version 3.2.2
[   10.325002] udevd[1803]: starting eudev-3.2.2
[   10.387025] mali: loading out-of-tree module taints kernel.
[   10.633659] xilinx-vcu xilinx-vcu: failed to set logicoreIP refclk rate -22
[   10.640775] VCU PLL: enable
[   10.644883] xilinx-vcu xilinx-vcu: xvcu_probe: Probed successfully
[   10.669400] al5e a0100000.al5e: l2 prefetch size:10526720 (bits), l2 color bitdepth:8
[   10.689799] al5d a0120000.al5d: l2 prefetch size:10526720 (bits), l2 color bitdepth:8
Fri Jun 14 15:56:00 UTC 2019
Starting internet superserver: inetd.
 
*********************************************************************
***                                                               ***
***   Avnet UltraZed Out Of Box PetaLinux Build V1.2              ***
***   The PS LED is mapped to ZynqMP MIO26                        ***
***                                                               ***
*********************************************************************
 
Configuring packages on first boot....
 (This may take several minutes. Please do not power off the machine.)
Running postinst /etc/rpm-postinsts/100-xserver-nodm-init...
Running postinst /etc/rpm-postinsts/101-sysvinit-inittab...
update-rc.d: /etc/init.d/run-postinsts exists during rc.d purge (continuing)
INIT: Entering runlevel: 5
 
*********************************************************************
***                                                               ***
***   Avnet Out Of Box PetaLinux Build V1.2                       ***
***   MAC address init config for eth0                            ***
***                                                               ***
*********************************************************************
 
/home/root/mac_eeprom_config.sh --bus 2 --slave 0x51 --file /etc/network/interfaces --interface eth0
 
Reading MAC ID from target EEPROM at 0x51 on bus i2c-2
Retrieved MAC ID 54:10:ec:67:bd:f9 from target EEPROM
Networking interfaces configuration file  detected
programming EEPROM MAC ID to eth0
Configuring network[   11.566752] pps pps0: new PPS source ptp0
 interfaces... [   11.572480] macb ff0e0000.ethernet: gem-ptp-timer ptp clock registered.
[   11.580413] IPv6: ADDRCONF(NETDEV_UP): eth0: link is not ready
udhcpc (v1.24.1) started
Sending discover...
Sending discover...
[   14.693799] macb ff0e0000.ethernet eth0: link up (1000/Full)
[   14.699476] IPv6: ADDRCONF(NETDEV_CHANGE): eth0: link becomes ready
Sending discover...
Sending select for 192.168.1.131...
Lease of 192.168.1.131 obtained, lease time 86400
/etc/udhcpc.d/50default: Adding DNS 192.168.1.254
done.
Starting system message bus: dbus.
Starting Dropbear SSH server: Generating key, this may take a while...
Public key portion is:
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCAa0WAmt4eeq65zbnY4q2Et1Jb6T7/UCEVD9iw8kDNb3KbR3z2sHCRyWEz2DmbiyUdQBLnitxejaJGQRLIpBRa0w3l39GTVUQOAicdrc
Fingerprint: md5 8d:86:77:3f:f2:da:47:1e:ba:b3:5e:9c:fb:b8:6d:d3
dropbear.
Starting syslogd/klogd: done
Starting tcf-agent: OK

Last login: Fri Jun 14 15:56:11 UTC 2019 on tty1
root@vcu_uz7ev_cc:~# 
```

### Clean the Vivado VCU design before generating the final BSP
```bash
:~/avnet-uzev-ports$ cd rdf0428-uz7ev-vcu-trd-2018-3
:~/avnet-uzev-ports/rdf0428-zcu106-vcu-trd-2018-3$ export TRD_HOME=$(pwd)
:~/avnet-uzev-ports/df0428-zcu106-vcu-trd-2018-3$ cd pl/vcu_uz7ev_cc
:~/avnet-uzev-ports/rdf0428-zcu106-vcu-trd-2018-3/pl/vcu_uz7ev_cc$ vivado vcu_uz7ev_cc.xpr

****** Vivado v2018.3 (64-bit)
  **** SW Build 2405991 on Thu Dec  6 23:36:41 MST 2018
  **** IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
    ** Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.

start_gui
```

### Clean the project using the __*reset_project*__ TCL command
- From the main menu
	- Window -> Tcl Console

- In the TCL Console
	- Type 'reset_project'

### Exit Vivado

### Manually delete the journal and log files from the project folder
```bash
:~/avnet-uzev-ports/rdf0428-zcu106-vcu-trd-2018-3/p/vcu_uz7ev_cc$ rm *.jou *.log
```

### Package the current configuration is a BSP file to preserve the current state of development
```bash
:~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc$ petalinux-package --bsp -p ~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc --hwsource ~/avnet-uzev-ports/rdf0428-uz7ev-vcu-trd-2018-3/pl/vcu_uz7ev_cc/ --output vcu_uz7ev_cc_petalinux_v2018_3.bsp
~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc/vcu_uz7ev_cc_petalinux_v2018_3.bsp
INFO: Target BSP "~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc/vcu_uz7ev_cc_petalinux_v2018_3.bsp" will contain the following projects
INFO: PetaLinux project: ~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc
INFO:   Copying ~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc/config.project
INFO:   Copying ~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc/.petalinux
INFO:   Copying ~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc/.gitignore
INFO:   Copying ~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc/project-spec
INFO:   Copying ~/avnet-uzev-ports/temp/uz_petalinux_v2018_3_vcu_uz7ev_cc/components
INFO:   Copying hardware project ~/avnet-uzev-ports/rdf0428-uz7ev-vcu-trd-2018-3/pl/vcu_uz7ev_cc
INFO: Creating BSP
INFO: Generating package vcu_uz7ev_cc_petalinux_v2018_3.bsp...
INFO: BSP is ready
```

#### Summary of BSP Contents
- Without a clean Vivado Project
	- vcu_uz7ev_cc_petalinux_v2018.3.bsp: 109M

- With a clean Vivado Project
	- vcu_uz7ev_cc_petalinux_v2018.3-clean.bsp: 8.5M

# Extra Debug Info

### (FAILED BOOT) CONFIG_SYS_BOOTMAPSZ is defined in the U-boot configuration, resulting in a reset loop because of rootfs size
	- Note: https://forums.xilinx.com/t5/Embedded-Linux/Booting-Error-Reading-Image-but-not-successful/td-p/894893
```bash
U-Boot 2018.01 (Jun 14 2019 - 11:15:45 +0000) Xilinx ZynqMP ZCU102 rev1.0

I2C:   ready
DRAM:  4 GiB
EL Level:       EL2
Chip ID:        zu7ev
MMC:   mmc@ff160000: 0 (eMMC), mmc@ff170000: 1 (SD)
SF: Detected n25q256a with page size 512 Bytes, erase size 128 KiB, total 64 MiB
*** Warning - bad CRC, using default environment

In:    serial@ff000000
Out:   serial@ff000000
Err:   serial@ff000000
Board: Xilinx ZynqMP
Bootmode: LVL_SHFT_SD_MODE1
Net:   ZYNQ GEM: ff0e0000, phyaddr 0, interface rgmii-id
eth0: ethernet@ff0e0000
U-BOOT for vcu_uz7ev_cc

ethernet@ff0e0000 Waiting for PHY auto negotiation to complete....... done
BOOTP broadcast 1
BOOTP broadcast 2
BOOTP broadcast 3
DHCP client bound to address 192.168.1.109 (813 ms)
Hit any key to stop autoboot:  0 
Device: mmc@ff160000
Manufacturer ID: 13
OEM: 14e
Name: Q2J55 
Tran Speed: 200000000
Rd Block Len: 512
MMC version 5.0
High Capacity: Yes
Capacity: 7.1 GiB
Bus Width: 4-bit
Erase Group Size: 512 KiB
HC WP Group Size: 8 MiB
User Capacity: 7.1 GiB WRREL
Boot Capacity: 16 MiB ENH
RPMB Capacity: 4 MiB ENH
reading image.ub
153074012 bytes read in 12829 ms (11.4 MiB/s)
## Loading kernel from FIT Image at 10000000 ...
   Using 'conf@system-top.dtb' configuration
   Trying 'kernel@1' kernel subimage
     Description:  Linux kernel
     Type:         Kernel Image
     Compression:  gzip compressed
     Data Start:   0x10000104
     Data Size:    7098813 Bytes = 6.8 MiB
     Architecture: AArch64
     OS:           Linux
     Load Address: 0x00080000
     Entry Point:  0x00080000
     Hash algo:    sha1
     Hash value:   28d9e06ce8977096dc8d5d420fed4c1297f563c9
   Verifying Hash Integrity ... sha1+ OK
## Loading ramdisk from FIT Image at 10000000 ...
   Using 'conf@system-top.dtb' configuration
   Trying 'ramdisk@1' ramdisk subimage
     Description:  petalinux-user-image
     Type:         RAMDisk Image
     Compression:  gzip compressed
     Data Start:   0x106cdfb0
     Data Size:    145937434 Bytes = 139.2 MiB
     Architecture: AArch64
     OS:           Linux
     Load Address: unavailable
     Entry Point:  unavailable
     Hash algo:    sha1
     Hash value:   57e95388618a54c461d8f5f2f4545910c5b01f3d
   Verifying Hash Integrity ... sha1+ OK
## Loading fdt from FIT Image at 10000000 ...
   Using 'conf@system-top.dtb' configuration
   Trying 'fdt@system-top.dtb' fdt subimage
     Description:  Flattened Device Tree blob
     Type:         Flat Device Tree
     Compression:  uncompressed
     Data Start:   0x106c53c8
     Data Size:    35618 Bytes = 34.8 KiB
     Architecture: AArch64
     Hash algo:    sha1
     Hash value:   8394df87b5a5749c822574eb4571a0e46bcc536a
   Verifying Hash Integrity ... sha1+ OK
   Booting using the fdt blob at 0x106c53c8
   Uncompressing Kernel Image ... OK
   Loading Ramdisk to ffffffffff4d2000, end 07fff41a ... "Synchronous Abort" handler, esr 0x96000044
ELR:     807d7cc
LR:      802043c
x0 : 0000000000000055 x1 : 00000000106cdfb0
x2 : 0000000008b2d41a x3 : 0000000000b2dfff
x4 : 00000000106cdfb0 x5 : 0000000000000061
x6 : 000000007fe621a8 x7 : ffffffffff4d2000
x8 : 000000007dd99470 x9 : 0000000000000008
x10: 00000000ffffffd8 x11: 000000007ff00000
x12: 000000007fe9d8b0 x13: 000000007dd985f0
x14: ffffffffff4d2000 x15: 0000000000000008
x16: 0000000000000002 x17: 0000000007fff41a
x18: 000000007dd99de8 x19: 0000000008b2d41a
x20: 0000000008000000 x21: 00000000106cdfb0
x22: 000000007fe9d810 x23: 000000007fe9d7e8
x24: 000000007fe9d7e0 x25: 0000000010000104
x26: 0000000000000001 x27: 0000000000000000
x28: 0000000000080000 x29: 000000007dd995b0

Resetting CPU ...

```
- Reset Loop ... hmmm....
