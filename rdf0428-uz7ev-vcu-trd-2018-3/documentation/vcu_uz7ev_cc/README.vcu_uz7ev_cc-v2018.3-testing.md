[//]: # (Readme.vcu_uz7ev_cc-2018.3-testing.md)
 
# Testing the v2018.3 vcu_uz7ev_cc design on the UltraZED-EV with Carrier Card
- Pre-built images located here:
	[images/vcu_uz7ev_cc](../../images/vcu_uz7ev_cc/)

- Images built using the BSP located here:
	[vcu_uz7ev_cc_petalinux_v2018_3.bsp](../../bsps/vcu_uz7ev_cc/vcu_uz7ev_cc_petalinux_v2018_3.bsp)

## Working with a DisplayPort Display
- [Explore the DisplayPort Interface Configuration](../testing/displayport/README.uz7ev_cc-DisplayPort-Interface-Configuration.txt)

- [Configure Alpha Blending on the Displayport Interface](../testing/displayport/README.uz7ev_cc-DisplayPort-Interface-Configuration-Alpha-Blending.txt)

- [Test the DisplayPort Display using the gstreamer __*videotestsrc*__ plugin](../testing/displayport/README.uz7ev_cc-DisplayPort-GStreamer-videotestsrc_example.txt)

## Working with the VCU Decoder (with DisplayPort Display Output)
- [Test 1080p30 video decoding and preview on a DisplayPort Display](../testing/displayport/README.uz7ev_cc-DisplayPort-GStreamer-VCU-H.264-1080p30-Decode_example.txt)

## Working with a USB Webcam (with DisplayPort Display Output)
- [Explore the USB Webcam Interface/Mode Configuration](../testing/usbwebcam/README.uz7ev_cc-USBWebcam-Interface-Configuration.txt)

- [Test the USB Webcam with Local Preview on the DisplayPort Display using the gstreamer __*v4l2src*__ plugin](../testing/usbwebcam/README.uz7ev_cc-USBWebcam-Local-Preview-on-DisplayPort-Display.txt)

## Working with the VCU Encoder, a USB Webcam, the DisplayPort Display and an RTP Network Stream
- [Test the VCU Encoding of 720p30 video from a USB Webcam, with Local DisplayPort Display preview and Stream the encoded video over RTP/UDP to a network device](../testing/rtpstreaming/README.uz7ev_cc-RTPStreaming-USBWebcam-DisplayPort-Preview-H.264-Encoded-Streaming-Video.txt)

- [SDP File for VLC](../testing/rtpstreaming/README.uz7ev_cc-RTPStreaming-USBWebcam-DisplayPort-Preview-H.264-Encoded-Streaming-Video.SDP)
