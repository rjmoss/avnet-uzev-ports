# Release Notes: v2018.3 VCU Design for the UZ7EV with Carrier Card
- Initial Release 6/21/2019

## Release Information and Resources
- Base Petalinux v2018.3 Release, including:
	- LEDs, DIP Switches, Pushbuttons
	- DisplayPort, USB Host/Device Support, Console UART
	- VCU Support (H.264, H.265, Encoder/Decoder)
	- GStreamer / V4L2 framework and tools

### Petalinux BSP Files
- [v2018.3 Targeted Petalinux BSP Release](rdf0428-uz7ev-vcu-trd-2018-3/bsps/vcu_uz7ev_cc)

### Pre-built Images (SD-Card)
- [v2018.3 vcu_uz7ev_cc Pre-built Images for SD-Card](rdf0428-uz7ev-vcu-trd-2018-3/images/vcu_uz7ev_cc)

### Testing/Getting Started Instructions
- [Instructions used to validate the operation of the vcu_uz7ev_cc design](rdf0428-uz7ev-vcu-trd-2018-3/documentation/vcu_uz7ev_cc/README.vcu_uz7ev_cc-v2018.3-testing.md)

### Porting Process Documentation
- [The process followed for creating this BSP release](rdf0428-uz7ev-vcu-trd-2018-3/documentation/vcu_uz7ev_cc/README.vcu_uz7ev_cc-v2018.3-port.md)

## Tools used in generating this release
### Base Host OS Environments
- Ubuntu 16.04.3
- Ubuntu 18.04.2

### Xilinx Tools
- Xilinx Petalinux v2018.3 + Ubuntu 16.04.3
- Xilinx Vivado v2018.3 + Ubuntu 18.04.2

## Xilinx Downloads Used
- [Xilinx Petalinux v2018.3 Full Installer](https://www.xilinx.com/member/forms/download/xef.html?filename=petalinux-v2018.3-final-installer.run)
- [Xilinx Vivado v2018.3 Web Installer](https://www.xilinx.com/member/forms/download/xef-vivado.html?filename=Xilinx_Vivado_SDK_Web_2018.3_1207_2324_Lin64.bin)
- [Xilinx v2018.3 ZCU106 VCU TRD Release](https://www.xilinx.com/member/forms/download/design-license-xef.html?filename=rdf0428-zcu106-vcu-trd-2018-3.zip)

## Avnet Downloads Used
- [v2018.2 UltraZED-EV Carrier Card Reference Design](http://zedboard.org/sites/default/files/design/uz7ev_evcc_2018_2.zip)
- [Avnet Board Definition Files for Vivado](https://github.com/Avnet/bdf.git)
- [Instructions for using Avnet Board Definition Files](http://zedboard.org/sites/default/files/documentations/Installing-Board-Definition-Files_v1_0.pdf)
