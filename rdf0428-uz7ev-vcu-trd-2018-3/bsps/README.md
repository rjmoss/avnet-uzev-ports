[//]: # (Readme.md)

# Avet UltraZed-EV VCU TRD BSPs
- This repository contains bsp releases for ports of the Xilinx ZCU106 v2018.3 VCU TRD design.
- Each release contains two files
```
	- *_uz7ev_cc_petalinux_v2018_3.bsp
		- Includes Full Vivado Project and all build/design artifacts
	- *_uz7ev_cc_petainux_v2018_3-clean.bsp
		- Includes Base Vivado Project with no build/design artifacts
```

## BSP Release Descriptions (core features)

### vcu_uz7ev_cc
- Base Petalinux v2018.3 Release, including:
	- LEDs, DIP Switches, Pushbuttons
	- DisplayPort, USB Host/Device Support, Console UART
	- VCU Support (H.264, H.265, Encoder/Decoder)
	- GStreamer / V4L2 framework and tools
