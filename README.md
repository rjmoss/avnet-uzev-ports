[//]: # (Readme.md)

# Avet UltraZed-EV Support
- This repository contains documentation, tools and examples for porting Xilinx releases to Avnet's UltraZed-EV

## Repository Organization


### General Organization
```
./_artifacts/<port-folder-name>
	- Subfolder's for each porting effort contain design artifacts used during key stages of the porting effort.
	- These artifacts include intermediate design files, tcl scripts, block diagrams, design documents and screenshots capturing important reference information during the porting effort.

./<port-folder-name>
	- Top level porting folders represent a Xilinx reference release, usually a TRD or other example design.
	- These folders can contain multiple 'sub-designs' and are generally structured around the Xilinx reference release.


./downloads/
	- A skeleton folder for storing downloaded files used while generating bsps and documentation in this repository.

./temp/
	- A skeleton working folder for building and storing working Vivado designs and Petalinux builds during the development of specific ports.
```

### Ports Included in this repository
- See individual README files for more information on what's included in each specific port.
```
./rdf0428-uz7ev-vcu-trd-2018-3/
	- A port of the ZCU106 v2018.3 VCU TRD to the UltraZED-EV + Carrier Card
```
